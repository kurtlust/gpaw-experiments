# TODOs for the next version of the GPAW benchmark

  * Re-introduce the scripts subdirectory with run scripts for PRACE systems,
    refer to them in the examples section of the README.md in the "Running the
    benchmark" section.
  * Re-introduce build examples for PRACE systems. Refer to them in the build section
    of the README.md.



### Examples

Example [job scripts](scripts/) (`scripts/job-*.sh`) are provided for
different PRACE systems that may offer a helpful starting point.

*TODO: Update the examples as testing on other systems goes on.*


