# Additional development notes

*Author: Kurt Lust, kurt.lust@uantwerpen.be*

These notes are in addition to the regular notes in the README.md file
and provide more information on the development process, such as versions
that we have looked at during the development of the GPAW benchmark.

## Version numbering

The GPAW version numbering changed in 2019. Version 1.5.3 is the
last version with the old numbering. In 2019 the development team switched 
to a version numbering scheme based on year, month and patchlevel, e.g.,
19.8.1 for the second version released in August 2019.


## GPAW for accelerators

There have been various developments for GPGPUs and MICs in the past
using either CUDA or pyMIC/libxstream. Many of those branches see no
development anymore and the information on the
[GPU page of the GPAW Wiki](https://wiki.fysik.dtu.dk/gpaw/devel/projects/gpu.html)
is also outdated. 

* Outdated branches in the [GitLab repository](https://gitlab.com/gpaw/gpaw)
  include the branch [rpa-gpu-expt](https://gitlab.com/gpaw/gpaw/tree/rpa-gpu-expt)
  and [cuda](https://gitlab.com/gpaw/gpaw/tree/cuda).
* The current CUDA development version is available in a
  [separate GitLab for CUDA development, cuda branch](https://gitlab.com/mlouhivu/gpaw/tree/cuda).
  This version should correspond to the Aalto version mentioned on the GPAW Wiki.
  As of early 2020, that version seems to be derived from the 1.5.2 CPU version
  (at least, I could find a commit that claims to merge the 1.5.2 code).


## Compatible versions of GPAW and its dependencies

The main source of information on version numbers is the file ``doc/install.rst``
included in the distribution.

Other sources of information:

* Python versions: [On PyPi](https://pypi.org/project/gpaw/) or towards the bottom 
  of the file ``setup.py``
* ASE version: 
     * [In the release notes](https://wiki.fysik.dtu.dk/gpaw/releasenotes.html):
       preferred version, though other versions may also work.
     * Line ``install_requires`` towards the bottom of the file ``setup.py`` in
       the most recent versions may give a different restriction from the release notes.

<table cellpadding="3">
<tr><th>GPAW</th>   <th>Python</th>       <th>ASE</th>                 <th>NumPy</th>    <th>SciPy</th>     <th>LibXC</td></tr>
<tr><td>1.5.2</td>  <td>2.7, 3.4-3.7</td> <td>3.17.0</td>              <td>&geq;1.9</td> <td>&geq;0.14</td> <td>&geq;2.0.1</td></tr>
<tr><td>19.8.1</td> <td>3.4-3.7</td>      <td>3.18.0, &geq;3.18.0</td> <td>&geq;1.9</td> <td>&geq;0.14</td> <td>3.x or 4.x</td></tr>
<tr><td>20.1.0</td> <td>3.5-3.8</td>      <td>3.19.0, &geq;3.18.0</td> <td>&geq;1.9</td> <td>&geq;0.14</td> <td>3.x or 4.x</td></tr>
</table>

## Sources of installation instructions

Official generic [installation instructions](https://wiki.fysik.dtu.dk/gpaw/install.html)
and
[platform specific examples](https://wiki.fysik.dtu.dk/gpaw/platforms/platforms.html)
are provided in the [GPAW wiki](https://wiki.fysik.dtu.dk/gpaw/). 

The UEABS repository contains additional instructions:
* [general instructions](README.md) - Under development
* [GPGPUs](build/build-cuda.md) - To check
* [Xeon Phis (KNC)](build/build-xeon-phi.md) - Outdated, should be removed 
  from the benchmark release.

Example [build scripts](build/examples/) are also available for some PRACE
systems. Currently these are still the old scripts from the 2.1 release and
before.

## Old information on benchmarks, to be ported to the post-2.1 release

#### Case S: Carbon nanotube

A ground state calculation for a carbon nanotube in vacuum. By default uses a
6-6-10 nanotube with 240 atoms (freely adjustable) and serial LAPACK with an
option to use ScaLAPACK. Expected to scale up to 10 nodes and/or 100 MPI
tasks.

Input file: [benchmark/carbon-nanotube/input.py](benchmark/carbon-nanotube/input.py)

#### Case M: Copper filament

A ground state calculation for a copper filament in vacuum. By default uses a
2x2x3 FCC lattice with 71 atoms (freely adjustable) and ScaLAPACK for
parallelisation. Expected to scale up to 100 nodes and/or 1000 MPI tasks.

Input file: [benchmark/carbon-nanotube/input.py](benchmark/copper-filament/input.py)

#### Case L: Silicon cluster

A ground state calculation for a silicon cluster in vacuum. By default the
cluster has a radius of 15Å (freely adjustable) and consists of 702 atoms,
and ScaLAPACK is used for parallelisation. Expected to scale up to 1000 nodes
and/or 10000 MPI tasks.

Input file: [benchmark/carbon-nanotube/input.py](benchmark/silicon-cluster/input.py)


### Running the benchmarks

No special command line options or environment variables are needed to run the
benchmarks on most systems. One can simply say e.g.
```
srun gpaw-python input.py
```

#### Special case: KNC

For KNCs (Xeon Phi Knights Corner), one needs to use a wrapper script to set
correct affinities for pyMIC (see
[scripts/affinity-wrapper.sh](scripts/affinity-wrapper.sh) for an example)
and to set two environment variables for GPAW:
```shell
GPAW_OFFLOAD=1  # (to turn on offloading)
GPAW_PPN=<no. of MPI tasks per node>
```

For example, in a SLURM system, this could be:
```shell
GPAW_PPN=12 GPAW_OFFLOAD=1 mpirun -np 256 -bootstrap slurm \
  ./affinity-wrapper.sh 12 gpaw-python input.py
```

#### Examples

Example [job scripts](scripts/) (`scripts/job-*.sh`) are provided for
different PRACE systems that may offer a helpful starting point.

