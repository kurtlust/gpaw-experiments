# GPAW

## Summary Version

* CPU-only version of the code: 2020.1.0
* GPU-version of the code: No official release. TODO: Refer to a commit?

TODO: Fix version. Or is this the version of this document?

## Purpose of Benchmark

* Benchmarking of density-functional theory methods
* Benchmarking mixed Python/C code
* Benchmarking of floating point and interconnect performance/balance
* Benchmark CUDA performance

## Characteristics of Benchmark

[GPAW](https://wiki.fysik.dtu.dk/gpaw/) is a density-functional theory (DFT)
program for ab initio electronic structure calculations using the projector
augmented wave method. It uses a uniform real-space grid representation of the
electronic wavefunctions that allows for excellent computational scalability
and systematic converge properties.

GPAW is written mostly in Python, but includes also computational kernels
written in C as well as leveraging external libraries such as NumPy, BLAS and
ScaLAPACK. Parallelisation is based on message-passing using MPI with no
support for multithreading. 

There is a CUDA development version that however cannot be found in the main
GPAW repository as it is developed in parallel by a different team. It is lagging
in development compared to the main version. As of early 2020, it appears to be
derived from GPAW 1.5.2.

* [Main GPAW web site](https://wiki.fysik.dtu.dk/gpaw/)
* [GPAW development on GitLab](https://gitlab.com/gpaw/gpaw)
* [Separate GitLab for CUDA development, cuda branch](https://gitlab.com/mlouhivu/gpaw/tree/cuda).



## Mechanics of Building Benchmark

### Dependencies

* GPAW is Python code (3.5 or newer) but it also contains some C code for some performance-critical 
  parts and to interface to a number of libraries on which it depends (for GPAW 19.8.1):
    * BLAS
    * LAPACK
    * MPI
    * BLACS and ScaLAPACK
    * FFTW or Intel MKL for FFT
    * [LibXC](https://www.tddft.org/programs/libxc/) 3.X or 4.X. LibXC is a library 
      of exchange-correlation functions for density-functional theory
* GPAW depends on a number of standard Python packages. As of GPAW 19.8.1, these are:
    * NumPY 1.9 or later 
    * SciPy 0.14 or later
* GPAW relies on ASE, a Python package from the same group
    * Check the release notes of GPAW as the releases of ASE and GPAW should match. 
      E.g., at the time of writing, version 19.8.1 is the most up-to-date release of GPAW with 3.18.0 the matching ASE version.
    * ASE has some optional dependencies that are not needed for the benchmarking: 
      Matplotlib (2.0.0 or newer), tkinter and Flask.
* TODO: GPAW also has [ELPA](https://elpa.mpcdf.mpg.de/) as an optional dependency.
    * TODO: Is this library needed for the benchmarks?
* Additional requirements for the GPU version:
    * NVIDIA CUDA toolkit
    * [PyCUDA](https://pypi.org/project/pycuda/) Python module

Detailed information on versions of dependencies for each version of GPAW can be found
in the file ``doc/install.rst`` of the GPAW distribution.


### Getting the software

Optimized BLAS, LAPACK, BLACS and ScaLAPACK libraries, MPI and FFTW are considered 
standard on a HPC system, so their installation is not covered in these notes.

We also assume the user has a working Python installation with a properly optimized 
version of NumPy and SciPy to start with.

#### LibXC (dependency of GPAW)

* The most recent version of LibXC can be [downloaded from its website](https://www.tddft.org/programs/libxc/download/).
* [LibXC GitLab repository](https://gitlab.com/libxc/libxc)
    * Download via a web browser: 
        * [Select "Tags"](https://gitlab.com/libxc/libxc/-/tags)
        * At the far right you see a download symbol for each version where you can chose between downloading a zip, a tar, a tar.gz or a tar.bz2 file. This 
          corresponds to links of the form `https://gitlab.com/libxc/libxc/-/archive/<version>/libxc-<version>.zip`
    * Clone the GitLab repository at a given tag, e.g., for version 4.3.4: `git clone -b 4.3.4 https://gitlab.com/libxc/libxc.git`
  
#### ASE (dependency of GPAW)

* ASE is avaialble on PyPI ([package ase](https://pypi.org/project/ase/))
    * ASE is available both as source and as a wheel
* [ASE GitLab repository](https://gitlab.com/ase/ase)
    * Download via a web browser: 
        * [Select "Tags"](https://gitlab.com/ase/ase/-/tags)
        * At the far right you see a download symbol for each version where you can chose between downloading a zip, a tar, a tar.gz or a tar.bz2 file. This 
          corresponds to links of the form `https://gitlab.com/ase/ase/-/archive/<version>/ase-<version>.zip`
    * Clone the GitLab repository at a given tag, e.g., for version 3.18.1: `git clone -b 3.18.1 https://gitlab.com/ase/ase.git`

#### GPAW

GPAW is freely available under the GPL license. 

* GPAW is available on PyPI ([package gpaw](https://pypi.org/project/gpaw/))
    * GPAW is only available as a tar.gz file, not as precompiled wheels.
* [GPAW CPU-version GitLab repository](https://gitlab.com/gpaw/gpaw)
    * Download via a web browser: 
        * [Select "Tags"](https://gitlab.com/gpaw/gpaw/-/tags)
        * At the far right you see a download symbol for each version where you can 
          chose between downloading a zip, a tar, a tar.gz or a tar.bz2 file. This 
          corresponds to links of the form `https://gitlab.com/gpaw/gpaw/-/archive/<version>/gpaw-<version>.zip`
    * Clone the GitLab repository at a given tag, e.g., for version 19.8.1: `git clone -b 19.8.1 https://gitlab.com/gpaw/gpaw.git`.
* [GPAW GPU-version GitLab repository](https://gitlab.com/mlouhivu/gpaw)
    * Download via a web browser: 
        * Select the [cuda branch](https://gitlab.com/mlouhivu/gpaw/tree/cuda)
        * Next to the "Web IDE" button there is a button that allows you to download 
          a compressed archive in several formats. This corresponds to links of the 
          form [`https://gitlab.com/mlouhivu/gpaw/-/archive/cuda/gpaw-cuda.zip`](https://gitlab.com/mlouhivu/gpaw/-/archive/cuda/gpaw-cuda.zip).
    * Clone the cuda branch to get the most recent CUDA development version: `git clone -b cuda https://gitlab.com/mlouhivu/gpaw.git`

## Installing the software

TODO. The text below is old text.

There are example GPAW installation instructions for several clusters in [the "Platforms 
and architectures" section of the documentation](https://wiki.fysik.dtu.dk/gpaw/platforms/platforms.html#) 
though that list is getting a bit outdated.



## Mechanics of Running Benchmark

### Benchmark cases

#### Case S: Carbon nanotube

* A ground state calculation for a carbon nanotube in vacuum. 
* By default uses a 6-6-10 nanotube with 240 atoms (freely adjustable) 
* Serial LAPACK with an option to use ScaLAPACK. 
* Expected to scale up to 100 MPI tasks and/or 10 nodes.

Input file: [benchmark/carbon-nanotube/input.py](benchmark/carbon-nanotube/input.py)

#### Case M: Copper filament

* A ground state calculation for a copper filament in vacuum. 
* By default uses a 2x2x3 FCC lattice with 71 atoms (freely adjustable) 
* ScaLAPACK for parallelisation. 
* Expected to scale up to 1000 MPI tasks and/or 100 nodes.

Input file: [benchmark/carbon-nanotube/input.py](benchmark/copper-filament/input.py)

#### Case L: Silicon cluster

* A ground state calculation for a silicon cluster in vacuum. 
* By default the cluster has a radius of 15Å (freely adjustable) and consists of 702 atoms
* ScaLAPACK is used for parallelisation. 
* Expected to scale up to 10000 MPI tasks and/or 1000 nodes.

Input file: [benchmark/carbon-nanotube/input.py](benchmark/silicon-cluster/input.py)


### Running the benchmarks

No special command line options or environment variables are needed to run the
benchmarks on most systems. One can simply say e.g.
```
srun gpaw-python input.py
```




## Verification of Results





