# Installing GPAW

## Dependencies

* GPAW is Python code (3.5 or newer) but it also contains some C code for some performance-critical 
  parts and to interface to a number of libraries on which it depends (for GPAW 20.1.0):
    * BLAS
    * LAPACK
    * MPI
    * BLACS and ScaLAPACK
    * FFTW or Intel MKL for FFT
    * [LibXC](https://www.tddft.org/programs/libxc/) 3.X or 4.X. LibXC is a library 
      of exchange-correlation functions for density-functional theory
* GPAW depends on a number of standard Python packages. As of GPAW 20.1.0, these are:
    * NumPY 1.9 or later 
    * SciPy 0.14 or later
* GPAW relies on ASE, a Python package from the same group
    * Check the release notes of GPAW as the releases of ASE and GPAW should match. 
      E.g., at the time of writing, version 20.1.0 is the most up-to-date release of GPAW with 3.19.0 the matching ASE version.
    * ASE has some optional dependencies that are not needed for the benchmarking: Matplotlib (2.0.0 or newer), tkinter and Flask.
* GPAW also has [ELPA](https://elpa.mpcdf.mpg.de/) as an optional dependency.
    * TODO: Is this library needed for the benchmarks?
* Additional requirements for the GPU version:
    * NVIDIA CUDA toolkit
    * [PyCUDA](https://pypi.org/project/pycuda/) Python module

### Determining version numbers of dependencies

The main source of information on version numbers is the file ``doc/install.rst``
included in the distribution.

Other sources of information:

* Python versions: [On PyPi](https://pypi.org/project/gpaw/) or towards the bottom 
  of the file ``setup.py``
* ASE version: 
     * [In the release notes](https://wiki.fysik.dtu.dk/gpaw/releasenotes.html):
       preferred version, though other versions may also work.
     * Line ``install_requires`` towards the bottom of the file ``setup.py`` in
       the most recent versions may give a different restriction from the release notes.

<table cellpadding="3">
<tr><th>GPAW</th>   <th>Python</th>       <th>ASE</th>                 <th>NumPy</th>    <th>SciPy</th>     <th>LibXC</td></tr>
<tr><td>1.5.2</td>  <td>2.7, 3.4-3.7</td> <td>3.17.0</td>              <td>&geq;1.9</td> <td>&geq;0.14</td> <td>&geq;2.0.1</td></tr>
<tr><td>19.8.1</td> <td>3.4-3.7</td>      <td>3.18.0, &geq;3.18.0</td> <td>&geq;1.9</td> <td>&geq;0.14</td> <td>3.x or 4.x</td></tr>
<tr><td>20.1.0</td> <td>3.5-3.8</td>      <td>3.19.0, &geq;3.18.0</td> <td>&geq;1.9</td> <td>&geq;0.14</td> <td>3.x or 4.x</td></tr>
</table>

## Getting the software

Optimized BLAS, LAPACK, BLACS and ScaLAPACK libraries, MPI and FFTW are considered 
standard on a HPC system, so their installation is not covered in these notes.

We also assume the user has a working Python installation with a properly optimized 
version of NumPy and SciPy to start with.

### LibXC (dependency of GPAW)

* The most recent version of LibXC can be [downloaded from its website](https://www.tddft.org/programs/libxc/download/).
* [LibXC GitLab repository](https://gitlab.com/libxc/libxc)
    * Download via a web browser: 
        * [Select "Tags"](https://gitlab.com/libxc/libxc/-/tags)
        * At the far right you see a download symbol for each version where you can chose between downloading a zip, a tar, a tar.gz or a tar.bz2 file. This 
          corresponds to links of the form `https://gitlab.com/libxc/libxc/-/archive/<version>/libxc-<version>.zip`
    * Clone the GitLab repository at a given tag, e.g., for version 4.3.4: `git clone -b 4.3.4 https://gitlab.com/libxc/libxc.git`
  
### ASE (dependency of GPAW)

* ASE is avaialble on PyPI ([package ase](https://pypi.org/project/ase/))
    * ASE is available both as source and as a wheel
* [ASE GitLab repository](https://gitlab.com/ase/ase)
    * Download via a web browser: 
        * [Select "Tags"](https://gitlab.com/ase/ase/-/tags)
        * At the far right you see a download symbol for each version where you can chose between downloading a zip, a tar, a tar.gz or a tar.bz2 file. This 
          corresponds to links of the form `https://gitlab.com/ase/ase/-/archive/<version>/ase-<version>.zip`
    * Clone the GitLab repository at a given tag, e.g., for version 3.18.1: `git clone -b 3.18.1 https://gitlab.com/ase/ase.git`

### GPAW

* GPAW is available on PyPI ([package gpaw](https://pypi.org/project/gpaw/))
    * GPAW is only available as a tar.gz file, not as precompiled wheels.
* [GPAW CPU-version GitLab repository](https://gitlab.com/gpaw/gpaw)
    * Download via a web browser: 
        * [Select "Tags"](https://gitlab.com/gpaw/gpaw/-/tags)
        * At the far right you see a download symbol for each version where you can 
          chose between downloading a zip, a tar, a tar.gz or a tar.bz2 file. This 
          corresponds to links of the form `https://gitlab.com/gpaw/gpaw/-/archive/<version>/gpaw-<version>.zip`
    * Clone the GitLab repository at a given tag, e.g., for version 19.8.1: `git clone -b 19.8.1 https://gitlab.com/gpaw/gpaw.git`.
* [GPAW GPU-version GitLab repository](https://gitlab.com/mlouhivu/gpaw)
    * Download via a web browser: 
        * Select the [cuda branch](https://gitlab.com/mlouhivu/gpaw/tree/cuda)
        * Next to the "Web IDE" button there is a button that allows you to download 
          a compressed archive in several formats. This corresponds to links of the 
          form [`https://gitlab.com/mlouhivu/gpaw/-/archive/cuda/gpaw-cuda.zip`](https://gitlab.com/mlouhivu/gpaw/-/archive/cuda/gpaw-cuda.zip).
    * Clone the cuda branch to get the most recent CUDA development version: `git clone -b cuda https://gitlab.com/mlouhivu/gpaw.git`

## Installing the software

There are example GPAW installation instructions for several clusters in [the "Platforms 
and architectures" section of the documentation](https://wiki.fysik.dtu.dk/gpaw/platforms/platforms.html#) 
though that list is getting a bit outdated.




