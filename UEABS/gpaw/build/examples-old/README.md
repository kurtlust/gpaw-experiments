# Example build scripts

## davide @ Cineca

Build instructions for GPU and CPU on the DAVIDE OpenPower GPU cluster @ Cineca.

These instructions haven't been tested recently. They are still for a version
based on distutils rather then setuptools that was dropped towards the end of
the development of this version of the benchmark suite.


## juwels @ Jülich Supercomputing Centre

Build instructions for the CPU nodes of JUWELS.

These instructions haven't been tested recently. They are still for a version
based on distutils rather then setuptools that was dropped towards the end of
the development of this version of the benchmark suite.


## piz-daint @ CSCS

Build instructions for the Piz Daint (GPU cluster).

These instructions haven't been tested recently. They are still for a version
based on distutils rather then setuptools that was dropped towards the end of
the development of this version of the benchmark suite.
