### Python installation script for JUWELS
###   uses --prefix to set a custom installation directory

# version numbers (modify if needed)
python_version=2.7.13

# installation directory (modify!)
tgt=$HOME/lib/python-2019-01

# setup build environment
module load CUDA/9.2.88
module load Intel/2019.0.117-GCC-7.3.0
module load IntelMPI/2018.4.274
module load imkl/2019.0.117
export CC=icc
export CFLAGS='-O2 -xAVX2 -axCORE-AVX512'
export CXX=icpc
export FC=ifort
export F90=$FC
export FFLAGS=$CFLAGS

# python
git clone https://github.com/python/cpython.git python-$python_version
cd python-$python_version
git checkout v$python_version
./configure --prefix=$tgt --enable-shared --disable-ipv6 --enable-unicode=ucs4 2>&1 | tee loki-conf
make 2>&1 | tee loki-make
make install 2>&1 | tee loki-inst
cd ..
sed -e "s|<BASE>|$tgt|g" setup/load-python.sh > $tgt/load.sh

# install pip
source $tgt/load.sh
python -m ensurepip
pip install --upgrade pip

# fix permissions
chmod -R g+rwX $tgt
chmod -R o+rX $tgt
