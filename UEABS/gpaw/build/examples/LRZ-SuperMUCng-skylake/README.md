# Build issues at LRZ

## Compilers used for testing

The following compilers, available on the system at the time of testing and installed
with Spack, were used:

  * Intel C/C++/Fortran version 19.1.2.254 (as provided by the intel/2020.0.2 module)
  * Intel MPI version 2019.8.254
  * Intel MKL 2020.1.217

These are components from the Intel compilers version 2020 update 2.

## Issues

  * It is impossible to download files from this system, so all sources have to be
    put in place manually before starting the compile. The scripts will generate the
    necessary URLs.

  * A general issues: At some patch releases of Python 3.8 and 3.9 changes were made
    to certain components of the code that installs additional packages. This causes
    failures in the installation process of NumPy and SciPy. So it is better to stick
    to the versions used in these example scripts (Python 3.8.7 and 3.9.4).

  * GPAW tried to compile a file that includes an MPI header with the regular C compiler.
    The solution was to set the regular C compiler to the MPI wrapper compiler script.

    That issue does not occur on all systems since some systems have the MPI header
    files included in ``CFILES`` so that the regular C compile also finds those header
    files.

  * We failed to get the combination Python 3.9.4 + NumPy 1.19.5 + SciPy 1.5.4 to compile.
    It failed during the installation of SciPy, even though this combination worked on
    several other systems.

  * The system uses TCL modules 4.6 at the time of testing. We failed to get the right
    compiler (which were not the default on the system) to load in the module file
    unless we first manually unloaded intel-mkl, intel-mpi and intel.

    There are no examples on how to do this in the existing module files of the system
    which may be due to the fact that spack with rpath is being used.

    It might have been a better solution to simply unload spack and use the other
    compiler modules that are not spack-managed instead rather than a variant of the
    default environment as presented by the system.
