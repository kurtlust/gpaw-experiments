# Example build scripts

## BSC-MareNostrum4-skylake

MareNostrum 4 is a Tier-0 cluster installed at the Barcelona Supercomputing
Center. GPAW was installed on the general-purpose block of the cluster,
consisting of 3,456 nodes with dual 24-core Intel Xeon Platinum 8160
"Skylake" CPUs and 96 GB of RAM in most nodes. The interconnect is
Omni-Path.

Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).

[Go to the directory](BSC-MareNostrum4-skylake)


## HLRS-Hawk-rome

Hawk is a Tier-0 cluster hosted by HLRS in Germany. The cluster consists of
5,632 compute nodes with dual 64-core AMD EPYC 7742 "Rome" CPUs and 256 GB of
memory. The interconnect is HDR200 InfiniBand in a 9-dimensional hypercube topology.

Most software on the cluster is installed through [Spack](https://spack.io/).
Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).

[Go to the directory](HLRS-Hawk-rome)


## JSC-JUWELS-skylake

JUWELS is a Tier-0 system hosted by the Jülich Supercomputing Centre. GPAW was only
installed on the so-called "Cluster Module" consisting of 2,271 compute nodes
with dual 24-core Intel Xeon Platinum 8168 "Skylake" CPUs and 96 GB RAM per node.
The interconnect is EDR InfiniBand.

Most software on the cluster is installed through [EasyBuild](https://easybuild.io/).
Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).

[Go to the directory](JSC-JUWELS-skylake)


## LRZ-SuperMUCng-skylake

SuperMUC-NG is a Tier-0 system hosted by LRZ in Germany. The cluster has 6,480
compute nodes with dual 24-core Intel Xeon Platinum 8174 CPUs. Most nodes have 96 GB
of RAM. The interconnect is Omni-Path. The cluster runs a SUSE Linux variant.

Most software on the cluster is installed through [Spack](https://spack.io/).
Environment modules are provided through the TCL-based [Environment Modules
package](https://modules.readthedocs.io/).

[Go to the directory](LRZ-SuperMUCng-skylake)


## TGCC-Irene-rome

Joliot-Curie/Irene is a Tier-0 machine hosted at the TGCC in France. GPAW was installed
on the AMD Rome partition. This partition has 2,292 dual 64-core AMD EPYC 7H12 "Rome"
CPUs with 256 GB RAM each. The interconnect is HDR100 InfiniBand.

Environment modules are provided through the TCL-based [Environment Modules
package](https://modules.readthedocs.io/).

[Go to the directory](TGCC-Irene-rome)


## CalcUA-vaughan-rome

Vaughan is a cluster installed at the University of Antwerp
as one of the VSC (Vlaams Supercomputer Centrum) Tier-2 clusters.
The cluster has 144 nodes with dual 32-core AMD EPYC 7452 "Rome" CPUs and 256 GB of RAM memory
per node. Nodes are interconnected through an InfiniBand HDR100 network. The system
does not contain accelerators. The cluster uses CentOS 8 as the
operating system.

Most software on the cluster is installed through [EasyBuild](https://easybuild.io/).
Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).

[Go to the directory](CalcUA-vaughan-rome)


## CalcUA-leibniz-broadwell

Leibniz is a cluster installed at the University of Antwerp
as one of the VSC (Vlaams Supercomputer Centrum) Tier-2 clusters.
The cluster has 153 regular CPU nodes with dual Intel Xeon E5-2680v4
"broadwell" CPUs with 128 or 256 GB of RAM memory per node.
Nodes are interconnected through an InfiniBand EDR network.
The system has two nodes with dual NVIDIA P100 GPUs and a node
with dual NEC Aurora TSUBASA first generation vector boards.
During the testing period the cluster used CentOS 7 as the
operating system.

Most software on the cluster is installed through [EasyBuild](https://easybuild.io/).
Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).

[Go to the directory](CalcUA-leibniz-broadwell)


## piz-daint @ CSCS

Build instructions for the Piz Daint (GPU cluster).

These instructions haven't been tested recently. They are still for a version
based on distutils rather than setuptools that was dropped towards the end of
the development of this version of the benchmark suite. They are here because
it is the only still relevant set of instructions for a GPU-based machine.

[Go to the directory](piz-daint)

