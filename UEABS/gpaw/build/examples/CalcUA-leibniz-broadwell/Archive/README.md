# Example build scripts for CalcUA-leibniz-broadwell

Cluster characteristics:
  * CPU architecture: dual-socket 14-core Intel Xeon E5-2680v4 "broadwell" CPUs
  * Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).
  * Package manager: Most software on the cluster is installed through [
    EasyBuild](https://easybuilders.github.io/). 

Unless otherwise mentioned below, the following combinations of versions of GPAW, ASE, Python, NumPy 
and SciPy were used:

| GPAW    | ASE     | Python | NumPy  | SciPy |
|:--------|:--------|:-------|:-------|:------|
| 1.5.2   | 3.17.0  | 3.7.9  | 1.18.5 | 1.5.4 |
| 19.8.1  | 3.18.2  | 3.7.9  | 1.18.5 | 1.5.4 |
| 20.1.0  | 3.19.3  | 3.8.6  | 1.18.5 | 1.5.4 |
| 20.10.0 | 3.20.1  | 3.9.0  | 1.19.4 | 1.5.4 |

