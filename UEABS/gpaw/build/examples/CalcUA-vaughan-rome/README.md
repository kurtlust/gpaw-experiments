# Example build scripts for CalcUA-vaughan-rome

Cluster characteristics:
  * CPU architecture: dual-socket 32-core AMD EPYC 7452 "Rome" CPUs
  * Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).
  * Package manager: Most software on the cluster is installed through [
    EasyBuild](https://easybuilders.github.io/).

Unless otherwise mentioned below, the following combinations of versions of GPAW, ASE, Python, NumPy
and SciPy were used:

| GPAW    | ASE     | Python | NumPy  | SciPy |
|:--------|:--------|:-------|:-------|:------|
| 20.1.0  | 3.19.3  | 3.8.7  | 1.18.5 | 1.5.4 |
| 20.10.0 | 3.20.1  | 3.9.1  | 1.19.5 | 1.5.4 |
| 21.1.0  | 3.21.1  | 3.9.1  | 1.19.5 | 1.5.4 |


The following example build scripts do a complete build, including a build of Python
and a minimal set of dependencies that is needed to build a large enough part of the
Python standard library to build GPAW and run the benchmarks: zlib,
ncurses, readline, libffi, SQLite (used by one of the GPAW tests but likely
not needed for the benchmarks), FFTW, libxc, Python and the Python packages
NumPy, SciPy, ase and GPAW. The included libraries are not sufficient for a
full Python installation with all standard library packages or a full ASE/GPAW
installation with all optional packages but should be enough to run the benchmarks.
All software is build with the Intel 2020 compilers.
  * `build_20.1.0_Python38_FFTW_icc`
  * `build_20.10.0_Python39_FFTW_icc`
  * `build_21.1.0_Python39_FFTW_icc`

The following build example build scripts install GPAW on top of the Intel Python
distribution. They rely as much as possible on modules already in the Intel Python
distribution or software that should be installed in the OS to run Intel Python.
The configuration accesses the FFT library available through
NumPy rather then linking directly to a FFT library.
The example script does use a module that provides up-to-date build
tools (buildtools/2020a) but on recent OSes it can be omitted.
Rather than loading the Intel and IntelPython3 modules one could as well
run the scripts provided by Intel to initialize the environment variables
for the compilers and Python distribution, and provided the necessary build
tools are also available in the system OS, this build could be done without
relying on any EasyBuild or Spack-generated modules.
  * `build_20.1.0_IntelPython3_icc.sh`
  * `build_20.10.0_IntelPython3_icc.sh`
  * `build_21.1.0_IntelPython3_icc.sh`


The following example build scripts depend on other modules (Python and FFTW) that
were installed on the cluster through EasyBuild beyond the compiler modules and a
module providing some up-to-date basic build tools:
  * `build_20.1.0_Python39icc_icc.sh`
  * `build_20.10.0_Python39icc_icc.sh`
  * `build_21.1.0_Python39icc_icc.sh`

