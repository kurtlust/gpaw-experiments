#!/usr/bin/bash
#
# Builds those modules that depend on EasyBuild modules. To better
# reproduce the build process, we use a separate set of EasyBuild
# recipes not included in the EasyBuild distribution.
#

echo -e "Running on $(hostname)\n"

function build() {

    echo -e "\n################################################################################\n" \
            "\nBuilding $1\n\n"
    ./$1

}

#
# Builds that depend on a Python and FFTW module provided outside
# the script.
#
build build_20.1.0_Python38icc_FFTW_icc.sh
build build_20.10.0_Python39icc_FFTW_icc.sh
build build_21.1.0_Python39icc_FFTW_icc.sh

#
# Build that depends on a FFTW module and Python dependencies provided
# outside the script but does build its own Python interpreter.
#
