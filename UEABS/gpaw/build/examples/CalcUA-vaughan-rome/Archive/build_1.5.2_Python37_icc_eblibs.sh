#!/bin/bash
#
# Installation script for GPAW 1.5.2:
# * We compile our own Python as this is the best guarantee to not have to
#   struggle with compatibility problems between various compilers used for
#   various components
# * As we have noted incompatibilities between GPAW 1.5.2 and the latest
#   NumPy and SciPy at the time of development (NumPy 1.19.2, SciPy 1.5.3),
#   we stuck to NumPy 1.18.5 and SciPy 1.4.1.
# * Using the matching version of ase, 3.17.0
# * Compiling with the Intel compilers
#
# The FFT library is discovered at runtime. With the settings used in this script
# this should be MKL FFT, but it is possible to change this at runtime to either 
# MKL, FFTW or the built-in NumPy FFT routines, see the installation instructions
# (link below).
#
# The original installation instructions for GPAW can be found at
# https://gitlab.com/gpaw/gpaw/-/blob/1.5.2/doc/install.rst
#

packageID='1.5.2-Python37-icc-eblibs'
packageName='GPAW-UEABS'

echo -e "\n### Building GPAW-manual/$packageID from $0\n\n"

#
# NOTE: For easy maintanance we load many settings from the UEABS module.
# Of course these variables can be set by hand also.
# Note that the UEABS module also completes the MODULEPATH set below.
#

# The next three variables are only used to load the right UEABS module
# and to give example values for variable settings in comments.
install_root=$VSC_SCRATCH/UEABS
systemID=CalcUA-vaughan-rome
UEABS_version='2.2'

module purge
MODULEPATH=$install_root/$systemID/Modules
module load UEABS/$UEABS_version

#
# The following UEABS_ variables are needed:
#
# Directory to put the downloaded sources of the packages.
# UEABS_DOWNLOADS=$install_root/Downloads
# Directory where packages should be installed.
# UEABS_PACKAGES=$install_root/$systemID/Packages
# Directory where modules are installed
# UEABS_MODULES=$install_root/$systemID/Modules
# Directory containing the UEABS repository (to find the patch file)
# UEABS_REPOSITORY=...

install_dir=$UEABS_PACKAGES/$packageName/$packageID
modules_dir=$UEABS_MODULES/$packageName
build_dir="/dev/shm/$USER/$packageName/$packageID"
patch_dir=$UEABS_REPOSITORY/gpaw/build/patches

# Software versions
python_version='3.7.9'
zlib_version='1.2.11'
ncurses_version='6.2'
readline_version='8.0'
sqlite_version='3.33.0'
libffi_version='3.3'
fftw_version='3.3.8'
libxc_version='4.3.4'
wheel_version='0.35.1'
cython_version='0.29.21'

numpy_version='1.18.5'
scipy_version='1.4.1'
ase_version='3.17.0'
GPAW_version='1.5.2'

GPAWsetups_version='0.9.20000' # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html

# Compiler settings
compiler_module='intel/2020a'
opt_level='-O2'
proc_opt_flags='-march=core-avx2 -mtune=core-avx2'
fp_opt_flags='-ftz -fp-speculation=safe -fp-model source'

py_maj_min='3.7'

################################################################################
#
# Prepare the system
#

#
# Load modules
# - Compiler module: This is a system-provide module
# - buildtools module: Contains a number of updated build tools already available
#   on most systems. We use it to get more consistent builds across systems, but
#   the module is not currently included in the UEABS repository. In most cases,
#   it will not be needed at all except for more exactly reproducing our test
#   results.
# - An EasyBuild-generated module containing a suitable FFTW implementation.
# - An EasyBuild-generated module zlib
# - An EasyBuild-generated module providing libffi
# - An EasyBuild-generated module providing ncurses
# - An EasyBuild-generated module providing libreadline
# - An EasyBuild-generated module providing SQLite
# The last five are minimal dependencies for Python. SQLite might not be needed
# though. Some previous versions of gpaw contained a test that used the Python
# SQLite interface and didn't work if Python was compiled without support for
# SQLite, but it is not clear if that matters in the benchmarks.
# In some cases we used modules build specifically for this project, but this was
# to determine the minimum set of libraries etc. that are needed. Modules provided
# on the system would have done the job as well.
#
module load $compiler_module
module load buildtools/$UEABS_version
module load FFTW/$fftw_version-intel-2020a-Cfg5-UEABS-GPAW
module load zlib/$zlib_version-intel-2020a-Cfg5-UEABS-GPAW
module load libffi/$libffi_version-intel-2020a-Cfg5-UEABS-GPAW
module load ncurses/$ncurses_version-intel-2020a-Cfg5-UEABS-GPAW
module load libreadline/$readline_version-intel-2020a-Cfg5-UEABS-GPAW
module load SQLite/$sqlite_version-intel-2020a-Cfg5-UEABS-GPAW

#
# Create the directories and make sure they are clean if that matters
#
/usr/bin/mkdir -p $UEABS_DOWNLOADS

/usr/bin/mkdir -p $install_dir
/usr/bin/rm -rf   $install_dir
/usr/bin/mkdir -p $install_dir

/usr/bin/mkdir -p $modules_dir

/usr/bin/mkdir -p $build_dir
/usr/bin/rm -rf   $build_dir
/usr/bin/mkdir -p $build_dir


################################################################################
#
# Download components
#

echo -e "\nDownloading files...\n"

cd $UEABS_DOWNLOADS

# Python: https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tar.xz
python_file="Python-$python_version.tar.xz"
python_url="https://www.python.org/ftp/python/$python_version"
[[ -f $python_file ]] || wget "$python_url/$python_file"

# https://gitlab.com/libxc/libxc/-/archive/4.3.4/libxc-4.3.4.tar.bz2
libxc_file="libxc-$libxc_version.tar.bz2"
libxc_url="https://gitlab.com/libxc/libxc/-/archive/$libxc_version"
[[ -f $libxc_file ]] || wget "$libxc_url/$libxc_file"

# Downloading wheel so that we can gather all sources for reproducibility.
wheel_file="wheel-$wheel_version.tar.gz"
wheel_url="https://pypi.python.org/packages/source/w/wheel"
[[ -f $wheel_file ]] || wget "$wheel_url/$wheel_file"

# Downloading Cython so that we can gather all sources for reproducibility.
cython_file="Cython-$cython_version.tar.gz"
cython_url="https://pypi.python.org/packages/source/c/cython"
[[ -f $cython_file ]] || wget "$cython_url/$cython_file"

# NumPy needs customizations, so we need to download and unpack the sources
numpy_file="numpy-$numpy_version.zip"
numpy_url="https://pypi.python.org/packages/source/n/numpy"
[[ -f $numpy_file ]] || wget "$numpy_url/$numpy_file"

# SciPy
scipy_file="scipy-$scipy_version.tar.gz"
scipy_url="https://pypi.python.org/packages/source/s/scipy"
[[ -f $scipy_file ]] || wget "$scipy_url/$scipy_file"

# Downloading ase so that we can gather all sources for reproducibility
ase_file="ase-$ase_version.tar.gz"
ase_url="https://pypi.python.org/packages/source/a/ase"
[[ -f $ase_file ]] || wget "$ase_url/$ase_file"

# GPAW needs customization, so we need to download and unpack the sources.
# https://files.pythonhosted.org/packages/49/a1/cf54c399f5489cfdda1e8da02cae8bfb4b39d7cb7a895ce86608fcd0e1c9/gpaw-1.5.2.tar.gz
GPAW_file="gpaw-$GPAW_version.tar.gz"
GPAW_url="https://pypi.python.org/packages/source/g/gpaw"
[[ -f $GPAW_file ]] || wget "$GPAW_url/$GPAW_file"

# Download GPAW-setup, a number of setup files for GPAW.
# https://wiki.fysik.dtu.dk/gpaw-files/gpaw-setups-0.9.20000.tar.gz
GPAWsetups_file="gpaw-setups-$GPAWsetups_version.tar.gz"
GPAWsetups_url="https://wiki.fysik.dtu.dk/gpaw-files"
[[ -f $GPAWsetups_file ]] || wget "$GPAWsetups_url/$GPAWsetups_file"


################################################################################
#
# Install libxc
#

echo -e "\nInstalling libxc...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$libxc_file

cd libxc-$libxc_version

# Configure
autoreconf -i

export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
./configure --prefix="$install_dir" \
            --disable-static --enable-shared --disable-fortran
# Build
make -j $parallel

# Install
make -j install

# Add bin, lib and include to the PATH variables
PATH=$install_dir/bin:$PATH
LIBRARY_PATH=$install_dir/lib:$LIBRARY_PATH
LD_LIBRARY_PATH=$install_dir/lib:$LD_LIBRARY_PATH
CPATH=$install_dir/include:$CPATH

# Clean-up
unset CC
unset CFLAGS


################################################################################
#
# Install Python
#

echo -e "\nInstalling Python...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$python_file

cd Python-$python_version

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC -diag-disable=1678,10148,111,188"
export CXX=icpc
export FC=ifort
export F90=$FC
export FFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC"
export LD=icc
export LDFLAGS="-L$EBROOTLIBFFI/lib -L$EBROOTSQLITE/lib -L$EBROOTLIBREADLINE/lib -L$EBROOTNCURSES/lib"
export CPPFLAGS="-I$EBROOTLIBFFI/include -I$EBROOTSQLITE/include -I$EBROOTLIBREADLINE/include -I$EBROOTNCURSES/include"
./configure --prefix="$install_dir" \
            --build=x86_64-pc-linux-gnu  --host=x86_64-pc-linux-gnu \
            --enable-shared --disable-ipv6 \
            --enable-optimizations \
            --with-ensurepip=upgrade
#             --with-icc \

# Build
make -j $parallel

# Install
make install
cd $install_dir/bin
ln -s python$py_maj_min python

# No need to adapt the PATH etc., this has been done already when installing libxc.

# Clean-up
unset CC
unset CFLAGS
unset CXX
unset FC
unset F90
unset FFLAGS
unset LD
unset LDFLAGS
unset CPPFLAGS


################################################################################
#
# Initialising for installing Python packages
#

echo -e "\nInitialising for installing Python packages...\n"

/usr/bin/mkdir -p "$install_dir/lib/python$py_maj_min/site-packages"
cd $install_dir
/usr/bin/ln -s lib lib64
PYTHONPATH="$install_dir/lib/python$py_maj_min/site-packages"


################################################################################
#
# Install wheel
#

echo -e "\nInstalling Cython...\n"

cd $build_dir
pip$py_maj_min install --prefix=$install_dir --no-deps $UEABS_DOWNLOADS/$wheel_file


################################################################################
#
# Optional: Install pytest and its dependencies to test NumPy and SciPy with
# import numpy
# numpy.test()
# import scipy
# scipy.text()
# We don't care about version numbers here as it is not important for the
# reproducibility of the benchmarks.
#

echo -e "\nInstalling pytest...\n"

cd $build_dir
pip$py_maj_min install --prefix=$install_dir pytest


################################################################################
#
# Install Cython
#

echo -e "\nInstalling Cython...\n"

cd $buikd_dir
pip$py_maj_min install --prefix=$install_dir --no-deps $UEABS_DOWNLOADS/$cython_file


################################################################################
#
# Install NumPy
#

echo -e "\nInstalling NumPy...\n"

cd $build_dir

# Uncompress
unzip $UEABS_DOWNLOADS/$numpy_file

cd numpy-$numpy_version

cat >site.cfg <<EOF
[DEFAULT]
library_dirs = $EBROOTFFTW/lib:$MKLROOT/lib/intel64_lin
include_dirs = $EBROOTFFTW/include:$MKLROOT/include
search_static_first=True
[mkl]
lapack_libs = mkl_rt
mkl_libs = mkl_rt
[fftw]
libraries = fftw3
EOF

export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC -diag-disable=1678,10148,111,188,3438,2650,3175,1890"
export CXX=icpc
export FC=ifort
export F90=$FC
export FFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export LD=icc

# Build NumPy.
# Note: Try python setup.py build --help-compiler and python setup.py build --help-fcompiler to list the
# choice of compilers. We did experience though that if the Intel compilers are selected explicitly,
# the wrong linker was used leading to problems there.
# Parallel build seems dangereous? We got some random failures.
python$py_maj_min setup.py build

# Install NumPy
python$py_maj_min setup.py install --prefix $install_dir

# Brief test: Should not be run in the NumPy build directory...
cd $build_dir
python$py_maj_min -c "import numpy"


################################################################################
#
# Install SciPy
#

echo -e "\nInstalling SciPy...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$scipy_file

cd scipy-$scipy_version

export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC -diag-disable=1678,10148,111,188,3438"
export CXX=icpc
export FC=ifort
export F90=$FC
# FFLAGS is used in the fitpack Makefile
export FFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
# FOPT is used in the odrpack Makefile
export FOPT="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export LD=icc

# Build scipy
# SciPy contains Fortran code so a parallel build may be dangerous.
python$py_maj_min setup.py build 

# Install scipy
python$py_maj_min setup.py install --prefix $install_dir

# Brief test: Should not be run in the SciPy build directory...
cd $build_dir
python$py_maj_min -c "import scipy"


################################################################################
#
# Install ase
#

echo -e "\nInstalling ase...\n"

pip$py_maj_min install --prefix=$install_dir --no-deps $UEABS_DOWNLOADS/$ase_file
#pip install --prefix=$install_dir --no-deps ase==$ase_version

# Brief test
cd $build_dir
python$py_maj_min -c "import ase"


################################################################################
#
# Install GPAW-setups
#

echo -e "\nInstalling gpaw-setups...\n"

mkdir -p $install_dir/share/gpaw-setups
cd $install_dir/share/gpaw-setups
tar -xf $UEABS_DOWNLOADS/$GPAWsetups_file --strip-components=1


################################################################################
#
# Install GPAW
#

echo -e "\nInstalling GPAW...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$GPAW_file

cd gpaw-$GPAW_version

# Make the customize.py script
mv customize.py customize.py.orig
cat >customize.py <<EOF
print( 'GPAW build INFO: Starting execution of the customization script' )
print( 'GPAW build INFO: Variables at the start of the customization script' )
print( 'GPAW build INFO: libraries =               ', libraries )
print( 'GPAW build INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW build INFO: library_dirs =            ', library_dirs )
print( 'GPAW build INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW build INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW build INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW build INFO: include_dirs =            ', include_dirs )
print( 'GPAW build INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW build INFO: compiler =                ', compiler )
print( 'GPAW build INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW build INFO: mpilinker =               ', mpilinker )
print( 'GPAW build INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW build INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW build INFO: define_macros =           ', define_macros )
print( 'GPAW build INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW build INFO: undef_macros =            ', undef_macros )
print( 'GPAW build INFO: scalapack =               ', scalapack )
print( 'GPAW build INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW build INFO: elpa =                    ', elpa )

# Reset the lists of libraries as often the wrong BLAS library is picked up.
libraries = []
mpi_libraries = []

# LibXC. Re-add the library (removed by resetting libraries).
# There is no need to add the library directory as we do set library_dirs to the
# content of LIBRARY_PATH further down.
# There should be no need to add the include directory as it is in CPATH which is
# set when we install gpaw. 
#include_dirs.append('$install_dir/include')
libraries.append('xc')

# libvdwxc
libvdwxc = False

# ELPA
elpa = False

# ScaLAPACK
scalapack = True
mpi_libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']
mpi_define_macros += [('GPAW_NO_UNDERSCORE_CBLACS', '1')]
mpi_define_macros += [('GPAW_NO_UNDERSCORE_CSCALAPACK', '1')]

# Add EasyBuild LAPACK/BLAS libs
# This should also enable MKL FFTW according to the documentation of GPAW 1.5.2
libraries += ['mkl_intel_lp64', 'mkl_sequential', 'mkl_core']

# Add other EasyBuild library directories.
library_dirs = os.environ['LIBRARY_PATH'].split(':')

# Set the compilers
compiler =    os.environ['CC']
mpicompiler = os.environ['MPICC']
mpilinker =   os.environ['MPICC']

# We need extra_compile_args to have the right compiler options when re-compiling
# files for gpaw-python. It does imply double compiler options for the other
# compiles though.
extra_compile_args = os.environ['CFLAGS'].split(' ')

print( 'GPAW build INFO: Variables at the end of the customization script' )
print( 'GPAW build INFO: libraries =               ', libraries )
print( 'GPAW build INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW build INFO: library_dirs =            ', library_dirs )
print( 'GPAW build INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW build INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW build INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW build INFO: include_dirs =            ', include_dirs )
print( 'GPAW build INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW build INFO: compiler =                ', compiler )
print( 'GPAW build INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW build INFO: mpilinker =               ', mpilinker )
print( 'GPAW build INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW build INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW build INFO: define_macros =           ', define_macros )
print( 'GPAW build INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW build INFO: undef_macros =            ', undef_macros )
print( 'GPAW build INFO: scalapack =               ', scalapack )
print( 'GPAW build INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW build INFO: elpa =                    ', elpa )
print( 'GPAW build INFO: Ending execution of the customization script' )
EOF

# Now install gpaw
export CC=icc
export MPICC=mpiicc
export CFLAGS="-std=c99 $opt_level $proc_opt_flags $fp_opt_flags -qno-openmp-simd"
python$py_maj_min setup.py build -j 32

# Install GPAW
#python$py_maj_min setup.py install --prefix="$install_dir"
pip$py_maj_min install --prefix $install_dir --no-deps  --ignore-installed  --no-build-isolation .

# Brief test
cd $build_dir
python$py_maj_min -c "import gpaw"


################################################################################
#
# Finish the install
#

echo -e "\nCleaning up and making the LUA-module GPAW-manual/$packageID...\n"

# Go to a different directory before cleaning up the build directory
cd $modules_dir
/bin/rm -rf $build_dir

# Create a module file
cat >$packageID.lua <<EOF
help([==[

Description
===========

GPAW 1.5.2 for the UEABS benchmark.

Configuration:
  * Parallel GPAW $GPAW_version with ase $ase_version
  * FFT library selected at runtime. The default with the path
    as set through this module should be MKL but it can be changed
    by setting GPAW_FFTWSO as indicated in the install instructions at
    https://gitlab.com/gpaw/gpaw/-/blob/$GPAW_version/doc/install.rst.
    FFTW is installed in the library directory of this module, so it
    is possible to select FFTW as the FFT library.
  * Hand-compiled Python using EasyBuild dependencies

Detailed configuration:
  * Compiler module: $compiler_module, used for all packages
  * EasyBuild modules
      * zlib $zlib_version
      * libffi $libffi_version
      * ncurses $ncurses_version
      * readline $readline_version
      * SQLite $sqlite_version
      * FFTW $fftw_version
  * libxc $libxc_version
  * Python $python_version
  * Important Python packages: numpy-$numpy_version, scipy-$scipy_version, ase-$ase_version, gpaw-$GPAW_version
  * Additional Python packages: wheel-$wheel_version, cython-$cython_version
  * GPAW setups $GPAWsetups_version


More information
================
 - Homepage: http://wiki.fysik.dtu.dk/gpaw
 - Documentation:
    - GPAW web-based documentation: https://wiki.fysik.dtu.dk/gpaw/
    - Version information at https://gitlab.com/gpaw/gpaw/-/blob/$GPAW_version/doc/
    - ASE web-based documentation: https://wiki.fysik.dtu.dk/ase/
]==])

whatis([==[Description: GPAW $GPAW_version with ase $ase_version: UEABS benchmark configuration.]==])

conflict("GPAW")
conflict("GPAW-manual")

-- if not ( isloaded("calcua/2020a") ) then
--     load("calcua/2020a")
-- end

if not ( isloaded("$compiler_module") ) then
    load("$compiler_module")
end

if not ( isloaded("zlib/$zlib_version-intel-2020a-Cfg5-UEABS-GPAW") ) then
    load("zlib/$zlib_version-intel-2020a-Cfg5-UEABS-GPAW")
end

if not ( isloaded("libffi/$libffi_version-intel-2020a-Cfg5-UEABS-GPAW") ) then
    load("libffi/$libffi_version-intel-2020a-Cfg5-UEABS-GPAW")
end

if not ( isloaded("ncurses/$ncurses_version-intel-2020a-Cfg5-UEABS-GPAW") ) then
    load("ncurses/$ncurses_version-intel-2020a-Cfg5-UEABS-GPAW")
end

if not ( isloaded("libreadline/$readline_version-intel-2020a-Cfg5-UEABS-GPAW") ) then
    load("libreadline/$readline_version-intel-2020a-Cfg5-UEABS-GPAW")
end

if not ( isloaded("SQLite/$sqlite_version-intel-2020a-Cfg5-UEABS-GPAW") ) then
    load("SQLite/$sqlite_version-intel-2020a-Cfg5-UEABS-GPAW")
end

if not ( isloaded("FFTW/$fftw_version-intel-2020a-Cfg5-UEABS-GPAW") ) then
    load("FFTW/$fftw_version-intel-2020a-Cfg5-UEABS-GPAW")
end

prepend_path("PATH",            "$install_dir/bin")
prepend_path("LD_LIBRARY_PATH", "$install_dir/lib")
prepend_path("LIBRARY_PATH",    "$install_dir/lib")
prepend_path("PYTHONPATH",      "$install_dir/lib/python$py_maj_min/site-packages")

setenv("GPAW_SETUP_PATH", "$install_dir/share/gpaw-setups")
EOF
