#!/usr/bin/bash
#
# Builds only modules that do not depend on EasyBuild modules beyond the
# compiler toolchains and a minimal module with recent build tools.
#

echo -e "Running on $(hostname)\n"

# Relies on a minimal module path with modules that are symlinked to the system
# ones or the minimal buildtools module created for the UEABS GPAW benchmark.

./build_1.5.2_Python37_icc.sh
./build_19.8.1_Python37_FFTW_icc.sh

# The following depends only on modules on the system provided by the
# 2020a toolchain and uses the system MODULEPATH.
# It does use UEABS to determine its settings though.
./build_1.5.2_IntelPython3_icc.sh
./build_19.8.1_IntelPython3_icc.sh
