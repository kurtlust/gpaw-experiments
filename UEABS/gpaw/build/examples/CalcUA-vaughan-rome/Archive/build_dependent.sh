#!/usr/bin/bash
#
# Builds those modules that depend on EasyBuild modules. To better
# reproduce the build process, we use a separate set of EasyBuild
# recipes not included in the EasyBuild distribution.
#

echo -e "Running on $(hostname)\n"

#
# Builds that depend on a Python and FFTW module provided outside
# the script.
#
./build_1.5.2_Python37icc_icc.sh

#
# Build that depends on a FFTW module and Python dependencies provided
# outside the script but does build its own Python interpreter.
#
./build_1.5.2_Python37_icc_eblibs.sh
