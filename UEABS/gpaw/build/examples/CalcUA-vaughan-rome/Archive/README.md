# Example build scripts for CalcUA-vaughan-rome

Cluster characteristics:
  * CPU architecture: dual-socket 32-core AMD EPYC 7452 "Rome" CPUs
  * Environment modules are provided through [Lmod](https://lmod.readthedocs.io/en/latest/).
  * Package manager: Most software on the cluster is installed through [
    EasyBuild](https://easybuilders.github.io/). 

Unless otherwise mentioned below, the following combinations of versions of GPAW, ASE, Python, NumPy 
and SciPy were used:

| GPAW    | ASE     | Python | NumPy  | SciPy |
|:--------|:--------|:-------|:-------|:------|
| 1.5.2   | 3.17.0  | 3.7.9  | 1.18.5 | 1.5.4 |
| 19.8.1  | 3.18.2  | 3.7.9  | 1.18.5 | 1.5.4 |
| 20.1.0  | 3.19.3  | 3.8.6  | 1.18.5 | 1.5.4 |
| 20.10.0 | 3.20.1  | 3.9.0  | 1.19.4 | 1.5.4 |


The following example build scripts do a complete build, including a build of Python 
and a minimal set of dependencies that is needed to build a large enough part of the
Python standard library to build GPAW and run the benchmarks:
  * `build_1.5.2_Python37_icc.sh`: GPAW 1.5.2, building (almost) everything from sources: zlib, 
    ncurses, readline, libffi, SQLite (used by one of the GPAW tests but likely
    not needed for the benchmarks), FFTW, libxc, Python and the Python packages
    NumPy, SciPy, ase and GPAW. The included libraries are not sufficient for a
    full Python 3.7 installation with all standard library packages or a full ASE/GPAW
    installation with all optional packages but should be enough to run the benchmarks.
    All software is build with the Intel 2020 compilers. 
    The FFT-solution is selected at runtime by GPAW 1.5.2.
  * `build_19.8.1_Python37_FFTW_icc`: Similar to `build_1.5.2_Python37_icc.sh`, but 
    now installing GAW 19.8.1.
    GPAW uses the FFTW library.
  * `build_20.1.0_Python38_FFTW_icc`: Similar to `build_19.8.1_Python37_FFTW_icc` but
    installing GPAW 20.1.0 with matching dependencies.
  * `build_20.10.0_Python39_FFTW_icc`: Similar to `build_19.8.1_Python37_FFTW_icc` and
    `build_20.1.0_Python38_FFTW_icc`, but installing GPAW 20.10.0 with matching
    dependencies

The following build example build scripts install GPAW on top of the Intel Python
distribution. They rely as much as possible on modules already in the Intel Python
distribution or software that should be installed in the OS to run Intel Python:
  * `build_1.5.2_IntelPython3_icc.sh`: Installs GPAW 1.5.2 with the matching ase
    version using the Intel Python distribution from the
    2020 compilers (3.7-based). All libraries are taken from that distribution or the OS,
    including the NumPy and SicPy packages. Only ase and GPAW are installed from
    sources. The example script does use a module that provides up-to-date build
    tools (buildtools/2020a) but on recent OSes it can be omitted.
    Rather than loading the Intel and IntelPython3 modules one could as well
    run the scripts provided by Intel to initialize the environment variables
    for the compilers and Python distribution, and provided the necessary build
    tools are also available in the system OS, this build could be done without
    relying on any EasyBuild or Spack-generated modules.
    
    **Note:** A patch is needed, bringing in some modifications that were made in
    version 19.8.1, to run with the Intel Python3 2020 distribution. Without the
    patch segmentation violations occured in `c/xc/tpss.c` and the related file
    `c/xc/revtpss.c`.
  * `build_19.8.1_IntelPython3_icc.sh`: Installs GPAW 19.8.1 with the matching ase
    version using the Intel Python distribution from the
    2020 compilers (3.7-based). All libraries are taken from that distribution or the OS,
    including the NumPy and SicPy packages. Only ase and GPAW are installed from
    sources. The configuration accesses the FFT library available through
    NumPy rather then linking directly to a FFT library.
    The example script does use a module that provides up-to-date build
    tools (buildtools/2020a) but on recent OSes it can be omitted.
    Rather than loading the Intel and IntelPython3 modules one could as well
    run the scripts provided by Intel to initialize the environment variables
    for the compilers and Python distribution, and provided the necessary build
    tools are also available in the system OS, this build could be done without
    relying on any EasyBuild or Spack-generated modules.
  * `build_20.1.0_IntelPython3_icc.sh`: Installs GPAW 20.1.0 with the matching
    ase version. Otherwise equivalent to `build_19.8.1_IntelPython3_icc.sh`.
  * `build_20.10.0_IntelPython3_icc.sh`: Installs GPAW 20.1.0 with the matching
    ase version. Otherwise equivalent to `build_19.8.1_IntelPython3_icc.sh` and
    `build_20.1.0_IntelPython3_icc.sh`.
    

The following example build scripts depend on other modules that were installed
on the cluster through EasyBuild beyond the compiler modules and a module providing
some up-to-date basic build tools:
  * `build_1.5.2_Python37icc_icc.sh`: GPAW 1.5.2 using an EasyBuild-compiled Python 3.7 
    compiled with the Intel 2020 compilers. It adds NumPy, SciPy, ase and GPAW
    to it.
  * `build_1.5.2_Python37_icc_eblibs.sh`: GPAW 1.5.2 using libraries that are installed on the
    system or through EasyBuild, but installs Python from sources. It also installs
    libxc, used by GPAW, and the Python packages NumPy, SciPy, ase and GPAW.
  * `build_20.10.0_Python39icc_icc.sh`: Like `build_1.5.2_Python37icc_icc.sh`,
    but building GPAW 20.10.0 with FFTW as the FFT library (as this is fixed
    during the build in this version).
