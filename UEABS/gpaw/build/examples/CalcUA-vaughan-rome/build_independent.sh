#!/usr/bin/bash
#
# Builds only modules that do not depend on EasyBuild modules beyond the
# compiler toolchains and a minimal module with recent build tools.
#

echo -e "Running on $(hostname)\n"

function build() {

    echo -e "\n################################################################################\n" \
            "\nBuilding $1\n\n"
    ./$1

}

# Relies on a minimal module path with modules that are symlinked to the system
# ones or the minimal buildtools module created for the UEABS GPAW benchmark.

build build_20.1.0_Python38_FFTW_icc.sh
build build_20.10.0_Python39_FFTW_icc.sh
build build_21.1.0_Python39_FFTW_icc.sh

# The following depends only on modules on the system provided by the
# 2020a toolchain and uses the system MODULEPATH.
# It does use UEABS to determine its settings though.
build build_20.1.0_IntelPython3_icc.sh
build build_20.10.0_IntelPython3_icc.sh
build build_21.1.0_IntelPython3_icc.sh
