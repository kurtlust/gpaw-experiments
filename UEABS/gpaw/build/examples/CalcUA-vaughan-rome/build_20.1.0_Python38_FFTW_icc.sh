#!/bin/bash
#
# Installation script for GPAW 20.1.0:
# * We compile our own Python as this is the best guarantee to not have to
#   struggle with compatibility problems between various compilers used for
#   various components
# * Using the matching version of ase, 3.19.3
# * Compiling with the Intel compilers
#
# The FFT library is discovered at runtime. With the settings used in this script
# this should be MKL FFT, but it is possible to change this at runtime to either
# MKL, FFTW or the built-in NumPy FFT routines, see the installation instructions
# (link below).
#
# The original installation instructions for GPAW can be found at
# https://gitlab.com/gpaw/gpaw/-/blob/20.1.0/doc/install.rst
#

packageID='20.1.0-Python38-FFTW-icc'
packageName='GPAW-UEABS'

echo -e "\n### Building $packageName/$packageID from $0\n\n"

#
# NOTE: For easy maintanance we load many settings from the UEABS module.
# Of course these variables can be set by hand also.
# Note that the UEABS module also completes the MODULEPATH set below.
#

# The next three variables are only used to load the right UEABS module
# and to give example values for variable settings in comments.
install_root=$VSC_SCRATCH/UEABS
systemID=CalcUA-vaughan-rome
UEABS_version='2.2'

module purge
MODULEPATH=$install_root/$systemID/Modules
module load UEABS/$UEABS_version

#
# The following UEABS_ variables are needed:
#
# Directory to put the downloaded sources of the packages.
# UEABS_DOWNLOADS=$install_root/Downloads
# Directory where packages should be installed.
# UEABS_PACKAGES=$install_root/$systemID/Packages
# Directory where modules are installed
# UEABS_MODULES=$install_root/$systemID/Modules

install_dir=$UEABS_PACKAGES/$packageName/$packageID
modules_dir=$UEABS_MODULES/$packageName
build_dir="/dev/shm/$USER/$packageName/$packageID"

# Software versions
python_version='3.8.7'
zlib_version='1.2.11'
ncurses_version='6.2'
readline_version='8.0'
sqlite_version='3.33.0'
sqlite_download='3330000'
libffi_version='3.3'
fftw_version='3.3.8'
libxc_version='4.3.4'
wheel_version='0.36.2'
cython_version='0.29.21'

numpy_version='1.18.5'
scipy_version='1.4.1'
ase_version='3.19.3'
GPAW_version='20.1.0'

GPAWsetups_version='0.9.20000' # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html

compiler_module='intel/2020a'
opt_level='-O2'
proc_opt_flags='-march=core-avx2 -mtune=core-avx2'
fp_opt_flags='-ftz -fp-speculation=safe -fp-model source'
parallel=32

py_maj_min='3.8'

################################################################################
#
# Prepare the system
#

#
# Load modules
# - Compiler module: This is a system-provide module
# - buildtools module: Contains a number of updated build tools already available
#   on most systems. We use it to get more consistent builds across systems, but
#   the module is not currently included in the UEABS repository. In most cases,
#   it will not be needed at all except for more exactly reproducing our test
#   results.
#
module load $compiler_module
module load buildtools/$UEABS_version

#
# Create the directories and make sure they are clean if that matters
#
/usr/bin/mkdir -p $UEABS_DOWNLOADS

/usr/bin/mkdir -p $install_dir
/usr/bin/rm -rf   $install_dir
/usr/bin/mkdir -p $install_dir

/usr/bin/mkdir -p $modules_dir

/usr/bin/mkdir -p $build_dir
/usr/bin/rm -rf   $build_dir
/usr/bin/mkdir -p $build_dir


################################################################################
#
# Download components
#

echo -e "\nDownloading files...\n"

cd $UEABS_DOWNLOADS

# zlib: https://www.zlib.net/zlib-1.2.11.tar.gz
zlib_file="zlib-$zlib_version.tar.gz"
zlib_url="https://www.zlib.net"
[[ -f $zlib_file ]] || wget "$zlib_url/$zlib_file"

# ncurses: https://ftp.gnu.org/pub/gnu/ncurses/ncurses-6.2.tar.gz
ncurses_file="ncurses-$ncurses_version.tar.gz"
ncurses_url="https://ftp.gnu.org/pub/gnu/ncurses"
[[ -f $ncurses_file ]] || wget "$ncurses_url/$ncurses_file"

# readline: https://ftp.gnu.org/pub/gnu/readline/readline-8.0.tar.gz
readline_file="readline-$readline_version.tar.gz"
readline_url="https://ftp.gnu.org/pub/gnu/readline"
[[ -f $readline_file ]] || wget "$readline_url/$readline_file"

# sqlite: https://www.sqlite.org/2020/sqlite-autoconf-3330000.tar.gz
sqlite_file="sqlite-autoconf-$sqlite_download.tar.gz"
sqlite_url="https://www.sqlite.org/2020"
[[ -f $sqlite_file ]] || wget "$sqlite_url/$sqlite_file"

# libffi: https://github.com/libffi/libffi/releases/download/v3.3/libffi-3.3.tar.gz
libffi_file="libffi-$libffi_version.tar.gz"
libffi_url="https://github.com/libffi/libffi/releases/download/v$libffi_version"
[[ -f $libffi_file ]] || wget "$libffi_url/$libffi_file"

# FFTW: http://www.fftw.org/fftw-3.3.8.tar.gz
fftw_file="fftw-$fftw_version.tar.gz"
fftw_url="http://www.fftw.org"
[[ -f $fftw_file ]] || wget "$fftw_url/$fftw_file"

# https://gitlab.com/libxc/libxc/-/archive/4.3.4/libxc-4.3.4.tar.bz2
libxc_file="libxc-$libxc_version.tar.bz2"
libxc_url="https://gitlab.com/libxc/libxc/-/archive/$libxc_version"
[[ -f $libxc_file ]] || wget "$libxc_url/$libxc_file"

# Python: https://www.python.org/ftp/python/3.7.9/Python-3.7.9.tar.xz
python_file="Python-$python_version.tar.xz"
python_url="https://www.python.org/ftp/python/$python_version"
[[ -f $python_file ]] || wget "$python_url/$python_file"

# Downloading wheel so that we can gather all sources for reproducibility.
wheel_file="wheel-$wheel_version.tar.gz"
wheel_url="https://pypi.python.org/packages/source/w/wheel"
[[ -f $wheel_file ]] || wget "$wheel_url/$wheel_file"

# Downloading Cython so that we can gather all sources for reproducibility.
cython_file="Cython-$cython_version.tar.gz"
cython_url="https://pypi.python.org/packages/source/c/cython"
[[ -f $cython_file ]] || wget "$cython_url/$cython_file"

# NumPy needs customizations, so we need to download and unpack the sources
numpy_file="numpy-$numpy_version.zip"
numpy_url="https://pypi.python.org/packages/source/n/numpy"
[[ -f $numpy_file ]] || wget "$numpy_url/$numpy_file"

# SciPy
scipy_file="scipy-$scipy_version.tar.gz"
scipy_url="https://pypi.python.org/packages/source/s/scipy"
[[ -f $scipy_file ]] || wget "$scipy_url/$scipy_file"

# Downloading ase so that we can gather all sources for reproducibility
ase_file="ase-$ase_version.tar.gz"
ase_url="https://pypi.python.org/packages/source/a/ase"
[[ -f $ase_file ]] || wget "$ase_url/$ase_file"

# GPAW needs customization, so we need to download and unpack the sources.
GPAW_file="gpaw-$GPAW_version.tar.gz"
GPAW_url="https://pypi.python.org/packages/source/g/gpaw"
[[ -f $GPAW_file ]] || wget "$GPAW_url/$GPAW_file"

# Download GPAW-setup, a number of setup files for GPAW.
# https://wiki.fysik.dtu.dk/gpaw-files/gpaw-setups-0.9.20000.tar.gz
GPAWsetups_file="gpaw-setups-$GPAWsetups_version.tar.gz"
GPAWsetups_url="https://wiki.fysik.dtu.dk/gpaw-files"
[[ -f $GPAWsetups_file ]] || wget "$GPAWsetups_url/$GPAWsetups_file"


################################################################################
#
# Install ncurses
#
# We mirror the two-step EasyBuild install process. This may be overkill, but
# we know it works.
#

echo -e "\nInstalling ncurses...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$ncurses_file

cd ncurses-$ncurses_version

# Configure step 1
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export CXX=icpc
export CXXFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
./configure --prefix="$install_dir" \
            --with-shared --enable-overwrite

# Build step 1
make -j $parallel

# Install step 1
make install

# Add bin, lib and include to the PATH variables
PATH=$install_dir/bin:$PATH
LIBRARY_PATH=$install_dir/lib:$LIBRARY_PATH
LD_LIBRARY_PATH=$install_dir/lib:$LD_LIBRARY_PATH
CPATH=$install_dir/include:$CPATH

# Configure step 2
make distclean
./configure --prefix="$install_dir" \
            --with-shared --enable-overwrite \
            --enable-ext-colors --enable-widec \
            --includedir=$install_dir/include/ncursesw/

# Build step 2
make -j $parallel

# Install step 2
make install

# Clean-up
unset CC
unset CFLAGS
unset CXX
unset CXXFLAGS


################################################################################
#
# Install readline
#

echo -e "\nInstalling readline...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$readline_file

cd readline-$readline_version

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export CPPFLAGS="-I$install_dir/include"
export LDFLAGS="-L$install_dir/lib -lncurses"
./configure --prefix="$install_dir"

# Build
make -j $parallel

# Install
make install

# Clean-up
unset CC
unset CFLAGS
unset CPPFLAGS
unset LDFLAGS


################################################################################
#
# Install zlib
#

echo -e "\nInstalling zlib...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$zlib_file

cd zlib-$zlib_version

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
./configure --prefix="$install_dir"

# Build
make -j $parallel

# Install
make install

# Clean-up
unset CC
unset CFLAGS


################################################################################
#
# Install libffi
#

echo -e "\nInstalling libffi...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$libffi_file

cd libffi-$libffi_version

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
./configure --prefix="$install_dir" \
            --disable-multi-os-directory

# Build
make -j $parallel

# Install
make install

# Clean-up
unset CC
unset CFLAGS


################################################################################
#
# Install SQLite
#

echo -e "\nInstalling SQLite...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$sqlite_file

cd sqlite-autoconf-$sqlite_download

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC -DSQLITE_DISABLE_INTRINSIC"
export CPPFLAGS="-I$install_dir/include"
./configure --prefix="$install_dir"

# Build
make -j $parallel

# Install
make install

# Clean-up
unset CC
unset CFLAGS
unset CPPFLAGS


################################################################################
#
# Install FFTW
#

echo -e "\nInstalling FFTW...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$fftw_file

# Patch the sources to compile with icc
cat >fftw.patch <<EOF
avoid using -no-gcc when compiling FFTW with Intel compilers,
since that fails on CentOS 8
see https://github.com/easybuilders/easybuild-easyconfigs/issues/10932
and https://github.com/FFTW/fftw3/issues/184
--- fftw-3.3.8/configure.orig
+++ fftw-3.3.8/configure
@@ -14861,6 +14861,9 @@
    intel) # Stop icc from defining __GNUC__, except on MacOS where this fails
         case "\${host_os}" in
             *darwin*) ;; # icc -no-gcc fails to compile some system headers
+            # using -no-gcc with recent Intel compilers fails on CentOS 8,
+            # and was only needed as a workaround for old Intel compilers anyway
+            *linux*) ;;
             *)
 	        { \$as_echo "\$as_me:\${as_lineno-\$LINENO}: checking whether C compiler accepts -no-gcc" >&5
 $as_echo_n "checking whether C compiler accepts -no-gcc... " >&6; }

EOF

cd fftw-$fftw_version
patch -p1 <../fftw.patch

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export MPICC=mpiicc
export F77=ifort
export F77FLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
./configure --prefix="$install_dir" \
            --disable-sse --disable-sse2 --disable-avx --enable-avx2 --disable-avx512 --disable-avx-128-fma --disable-kcvi \
            --enable-fma \
            --disable-openmp --disable-threads \
            --enable-mpi \
            -disable-static --enable-shared --disable-fortran

# Build
make -j $parallel

# Install
make install

# Clean-up
unset CC
unset CFLAGS
unset MPICC
unset F77
unset F77FLAGS


################################################################################
#
# Install libxc
#

echo -e "\nInstalling libxc...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$libxc_file

cd libxc-$libxc_version

# Configure
autoreconf -i

export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
./configure --prefix="$install_dir" \
            --disable-static --enable-shared --disable-fortran
# Build
make -j $parallel

# Install
make -j install

# Clean-up
unset CC
unset CFLAGS


################################################################################
#
# Install Python
#

echo -e "\nInstalling Python...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$python_file

cd Python-$python_version

# Configure
export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC -diag-disable=1678,10148,111,188"
export CXX=icpc
export FC=ifort
export F90=$FC
export FFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC"
export LD=icc
export LDFLAGS="-L$install_dir/lib"
export CPPFLAGS="-I$install_dir/include"
./configure --prefix="$install_dir" \
            --build=x86_64-pc-linux-gnu  --host=x86_64-pc-linux-gnu \
            --enable-shared --disable-ipv6 \
            --enable-optimizations \
            --with-ensurepip=upgrade
#             --with-icc \

# Build
make -j 32

# Install
make install
cd $install_dir/bin
ln -s python$py_maj_min python

# Clean-up
unset CC
unset CFLAGS
unset CXX
unset FC
unset F90
unset FFLAGS
unset LD
unset LDFLAGS
unset CPPFLAGS


################################################################################
#
# Initialising for installing Python packages
#

echo -e "\nInitialising for installing Python packages...\n"

/usr/bin/mkdir -p "$install_dir/lib/python$py_maj_min/site-packages"
cd $install_dir
/usr/bin/ln -s lib lib64
PYTHONPATH="$install_dir/lib/python$py_maj_min/site-packages"


################################################################################
#
# Install wheel
#

echo -e "\nInstalling wheel...\n"


cd $build_dir
pip$py_maj_min install --prefix=$install_dir --no-deps $UEABS_DOWNLOADS/$wheel_file


################################################################################
#
# Optional: Install pytest and its dependencies to test NumPy and SciPy with
# import numpy
# numpy.test()
# import scipy
# scipy.text()
# We don't care about version numbers here as it is not important for the
# reproducibility of the benchmarks.
#

echo -e "\nInstalling pytest...\n"

cd $build_dir
pip$py_maj_min install --prefix=$install_dir pytest


################################################################################
#
# Install Cython
#

echo -e "\nInstalling Cython...\n"

cd $build_dir
pip$py_maj_min install --prefix=$install_dir --no-deps $UEABS_DOWNLOADS/$cython_file


################################################################################
#
# Install NumPy
#

echo -e "\nInstalling NumPy...\n"

cd $build_dir

# Uncompress
unzip $UEABS_DOWNLOADS/$numpy_file

cd numpy-$numpy_version

cat >site.cfg <<EOF
[DEFAULT]
library_dirs = $install_dir/lib:$MKLROOT/lib/intel64_lin
include_dirs = $install_dir/include:$MKLROOT/include
search_static_first=True
[mkl]
lapack_libs = mkl_rt
mkl_libs = mkl_rt
[fftw]
libraries = fftw3
EOF

export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC -diag-disable=1678,10148,111,188,3438,2650,3175,1890"
export CXX=icpc
export FC=ifort
export F90=$FC
export FFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export LD=icc

# Build NumPy.
# Note: Try python setup.py build --help-compiler and python setup.py build --help-fcompiler to list the
# choice of compilers. We did experience though that if the Intel compilers are selected explicitly,
# the wrong linker was used leading to problems there.
# Parallel build seems dangereous? We got some random failures.
python$py_maj_min setup.py config

python$py_maj_min setup.py build --compiler=intel --fcompiler=intelem

# Install NumPy
pip$py_maj_min install --prefix $install_dir --no-deps  --ignore-installed  --no-build-isolation .

# Brief test: Should not be run in the NumPy build directory...
cd $build_dir
python$py_maj_min -c "import numpy"

# Clean-up
unset CC
unset CFLAGS
unset CXX
unset FC
unset F90
unset FFLAGS
unset FOPT
unset LD


################################################################################
#
# Install SciPy
#

echo -e "\nInstalling SciPy...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$scipy_file

cd scipy-$scipy_version

export CC=icc
export CFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fwrapv -fPIC -diag-disable=1678,10148,111,188,3438"
export CXX=icpc
export FC=ifort
export F90=$FC
# FFLAGS is used in the fitpack Makefile
export FFLAGS="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
# FOPT is used in the odrpack Makefile
export FOPT="$opt_level $proc_opt_flags $fp_opt_flags -fPIC"
export LD=icc

# Build scipy
# SciPy contains Fortran code so a parallel build may be dangerous.
python$py_maj_min setup.py build --compiler=intel --fcompiler=intelem

# Install scipy
pip$py_maj_min install --prefix $install_dir --no-deps  --ignore-installed  --no-build-isolation .

# Brief test: Should not be run in the NumPy build directory...
cd $build_dir
python$py_maj_min -c "import scipy"

# Clean-up
unset CC
unset CFLAGS
unset CXX
unset FC
unset F90
unset FFLAGS
unset FOPT
unset LD


################################################################################
#
# Install ase
#

echo -e "\nInstalling ase...\n"

cd $build_dir
pip$py_maj_min install --prefix=$install_dir --no-deps $UEABS_DOWNLOADS/$ase_file
#pip install --prefix=$install_dir --no-deps ase==$ase_version

# Brief test
cd $build_dir
python$py_maj_min -c "import ase"


################################################################################
#
# Install GPAW-setups
#

echo -e "\nInstalling gpaw-setups...\n"

mkdir -p $install_dir/share/gpaw-setups
cd $install_dir/share/gpaw-setups
tar -xf $UEABS_DOWNLOADS/$GPAWsetups_file --strip-components=1


################################################################################
#
# Install GPAW
#

echo -e "\nInstalling GPAW...\n"

cd $build_dir

# Uncompress
tar -xf $UEABS_DOWNLOADS/$GPAW_file

cd gpaw-$GPAW_version

# Make the siteconfig.py script
cat >siteconfig.py <<EOF
print( 'GPAW EasyBuild INFO: Starting execution of the customization script' )
print( 'GPAW EasyBuild INFO: Variables at the start of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =               ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =            ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =            ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =               ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =           ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =            ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                    ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =               ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                    ', elpa )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )

# LibXC
include_dirs.append('$install_dir/include')
#libraries.append('xc')

# libvdwxc
libvdwxc = False

# ELPA
elpa = False

# Use regular FFTW
fftw = True
libraries += ['fftw3']

# ScaLAPACK
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# MKL BLAS
libraries += ['mkl_sequential','mkl_core', 'mkl_rt', ]

# Add other EasyBuild library directoryes.
library_dirs = os.environ['LIBRARY_PATH'].split(':')

# Set the compilers
compiler =    os.environ['CC']
mpicompiler = os.environ['MPICC']
mpilinker =   os.environ['MPICC']

print( 'GPAW EasyBuild INFO: Variables at the end of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =               ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =            ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =            ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =               ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =           ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =            ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                    ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =               ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                    ', elpa )
print( 'GPAW EasyBuild INFO: Ending execution of the customization script' )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )
EOF

# Now install gpaw
export CC=icc
export MPICC=mpiicc
export CFLAGS="-std=c99 $opt_level $proc_opt_flags $fp_opt_flags -qno-openmp-simd"
python$py_maj_min setup.py build -j $parallel

# Install GPAW
#python$py_maj_min setup.py install --prefix="$install_dir"
pip$py_maj_min install --prefix $install_dir --no-deps  --ignore-installed  --no-build-isolation .

# Brief test
cd $build_dir
python$py_maj_min -c "import gpaw"

# Clean-up
unset CC
unset MPICC
unset CFLAGS


################################################################################
#
# Finish the install
#

echo -e "\nCleaning up and making the LUA-module $packageName/$packageID...\n"


# Go to a different directory before cleaning up the build directory
cd $modules_dir
/bin/rm -rf $build_dir

# Create a module file
cat >$packageID.lua <<EOF
help([==[

Description
===========

GPAW $GPAW_version for the UEABS benchmark.

Configuration:
  * Parallel GPAW $GPAW_version with ase $ase_version
  * Using FFTW for the FFT computations and MKL for the BLAS/Lapack
  * Minimal use of system libraries or libraries that are installed
    through package managers such as EasyBuild or Spack.

Detailed configuration:
  * Compiler module: $compiler_module, used for all packages
  * Python dependencies:
      * zlib $zlib_version
      * ncurses $ncurses_version
      * readline $readline_version
      * SQLite $sqlite_version
      * libffi $libffi_version
  * FFTW $fftw_version
  * libxc $libxc_version
  * Python $python_version
  * Important Python packages: numpy-$numpy_version, scipy-$scipy_version, ase-$ase_version, gpaw-$GPAW_version
  * Additional Python packages: wheel-$wheel_version, cython-$cython_version
  * GPAW setups $GPAWsetups_version


More information
================
 - Homepage: http://wiki.fysik.dtu.dk/gpaw
 - Documentation:
    - GPAW web-based documentation: https://wiki.fysik.dtu.dk/gpaw/
    - Version information at https://gitlab.com/gpaw/gpaw/-/blob/$GPAW_version/doc/
    - ASE web-based documentation: https://wiki.fysik.dtu.dk/ase/
]==])

whatis([==[Description: GPAW $GPAW_version with ase $ase_version: UEABS benchmark configuration with Python $python_version, numpy-$numpy_version, scipy-$scipy_version, libxc $libxc_version and FFTW $fftw_version.]==])

conflict("GPAW")
conflict("$packageName")

if not ( isloaded("$compiler_module") ) then
    load("$compiler_module")
end

prepend_path("PATH",            "$install_dir/bin")
prepend_path("LD_LIBRARY_PATH", "$install_dir/lib")
prepend_path("LIBRARY_PATH",    "$install_dir/lib")
prepend_path("PYTHONPATH",      "$install_dir/lib/python$py_maj_min/site-packages")

setenv("GPAW_SETUP_PATH", "$install_dir/share/gpaw-setups")
EOF
