#!/bin/bash -x
#SBATCH -J 4x48
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=48
#SBATCH --time=00:30:00
#SBATCH --qos=prace

source $HOME/project/lib/gpaw-1.1.0/load.sh

srun gpaw-python input.py

