# Unified European Applications Benchmark Suite

The Unified European Application Benchmark Suite (UEABS) is a set of currently 13 application codes taken from the pre-existing PRACE and DEISA application benchmark suites, and extended with the PRACE Accelerator Benchmark Suite. The objective is providing a single benchmark suite of scalable, currently relevant and publicly available application codes and datasets, of a size which can realistically be run on large systems, and maintained into the future.

The UEABS activity was started during the PRACE-PP project and was publicly released by the PRACE-2IP project.
The PRACE "Accelerator Benchmark Suite" was a PRACE-4IP activity.
The UEABS has been and will be actively updated and maintained by the subsequent PRACE-IP projects.

Each application code has either one, or two input datasets. If there are two datasets, Test Case A is designed to run on Tier-1 sized systems (up to around 1,000 x86 cores, or equivalent) and Test Case B is designed to run on Tier-0 sized systems (up to around 10,000 x86 cores, or equivalent). If there is only one dataset (Test Case A), it is suitable for both sizes of system.

Contacts: (Ok to mention all BCOs here?, ask PMO for a UEABS contact mailing list address?), Walter Lioen <mailto:walter.lioen@surf.nl>

Current Release
---------------

The current release is Version 2.2 (December 31, 2021).
See also the [release notes and history](RELEASES.md).

Running the suite
-----------------

Instructions to run each test cases of each codes can be found in the subdirectories of this repository.

For more details of the codes and datasets, and sample results, please see the PRACE-6IP benchmarking deliverable D7.5 "Evaluation of Benchmark Performance" (November 30, 2021) at http://www.prace-ri.eu/public-deliverables/ .

The application codes that constitute the UEABS are:
---------------------------------------------------
<table>
  <thead>
    <tr>
      <th rowspan="2">Application</th>
      <th rowspan="2">Lines of<br/>Code</th>
      <th colspan="3">Parallelism</th>
      <th colspan="4">Language</th>
      <th rowspan="2">Code Description/Notes</th>
    </tr>
    <tr>
      <th>MPI</th>
      <th>OpenMP/<br/>Pthreads</th>
      <th>GPU</th>
      <th>Fortran</th>
      <th>Python</th>
      <th>C</th>
      <th>C++</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Alya
        <ul>
          <li><a href="https://www.bsc.es/computer-applications/alya-system">website</a></li>
          <li><a href="https://gitlab.com/bsc-alya/open-alya">source</a></li>
          <li><a href="alya/README.md">instructions</a></li>
          <li><a href="https://gitlab.com/bsc-alya/benchmarks/sphere-16M">Test Case A</a></li>
          <li><a href="https://gitlab.com/bsc-alya/benchmarks/sphere-132M">Test Case B</a></li>
        </ul>
      </td>
      <td>600,000</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td></td>
      <td>The Alya System is a Computational Mechanics code capable of solving different physics, each one with its own modelization characteristics, in a coupled way. Among the problems it solves are: convection-diffusion reactions, incompressible flows, compressible flows, turbulence, bi-phasic flows and free surface, excitable media, acoustics, thermal flow, quantum mechanics (DFT) and solid mechanics (large strain).</td>
    </tr>
    <tr>
      <td>Code_Saturne</td>
      <td>~350,000</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>The code solves the Navier-Stokes equations for imcompressible/compressible flows using a predictor-corrector technique. The Poisson pressure equation is solved by a Conjugate Gradient preconditioned by a multi-grid algorithm, and the transport equations by Conjugate Gradient-like methods. Advanced gradient reconstruction is also available to account for distorted meshes.</td>
    </tr>
    <tr>
      <td>CP2K
        <ul>
          <li><a href="https://www.cp2k.org/">CP2K website</a></li>
          <li><a href="https://github.com/cp2k/cp2k/releases">Source code</a></li>
          <li><a href="./cp2k/README.md">Build instructions</a></li>
          <li><a href="./cp2k/benchmarks/TestCaseA_H2O-512">Testcase A</a></li>
          <li><a href="./cp2k/benchmarks/TestCaseB_LiH-HFX">Testcase B</a></li>
          <li><a href="./cp2k/benchmarks/TestCaseC_H2O-DFT-LS">Testcase C</a></li>
        </ul>
      </td>
      <td>~1,150,000</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td></td>
      <td>CP2K is a freely available quantum chemistry and solid-state physics software package for performing atomistic simulations. It can be run with MPI, OpenMP and CUDA. All of CP2K is MPI parallelised, with some routines making use of OpenMP, which can be used to reduce the memory footprint. In addition some linear algebra operations may be offloaded to GPUs using CUDA.</td>
    </tr>
    <tr>
      <td>GADGET</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>GPAW
        <ul>
          <li><a href="https://wiki.fysik.dtu.dk/gpaw/">website</a></li>
          <li><a href="https://gitlab.com/gpaw/gpaw">GPAW GitLab</a></li>
          <li><a href="https://gitlab.com/mlouhivu/gpaw/tree/cuda">GPAW GPU development (cuda branch)</a></li>
          <li><a href="gpaw/README.md#mechanics-of-building-the-benchmark">Build instructions</a>
          <li><a href="gpaw/README.md#mechanics-of-running-the-benchmark">Run instructions</a>
          <li><a href="gpaw/benchmark/A_carbon-nanotube/input.py">Test Case A</a>
          <li><a href="gpaw/benchmark/B_copper-filament/input.py">Test Case B</a>
          <li><a href="gpaw/benchmark/C_silicon-cluster/input.py">Test Case C</a>
        </ul>
      </td>
      <td>132,000</td>
      <td>X</td>
      <td></td>
      <td>(X)</td>
      <td></td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td>
        GPAW is a density-functional theory (DFT)
        program for ab initio electronic structure calculations using the projector
        augmented wave method. It uses a uniform real-space grid representation of the
        electronic wavefunctions that allows for excellent computational scalability
        and systematic converge properties.
        The GPAW benchmark tests MPI parallelization and the quality of the provided mathematical
        libraries, including BLAS, LAPACK, ScaLAPACK, and FFTW-compatible library. There is
        also an experimental CUDA-based implementation for GPU systems, but it is not covered
        by this UEABS release.
      </td>
    </tr>
    <tr>
      <td>GROMACS
      <ul>
      <li><a href="http://www.gromacs.org">website</a></li>
      <li><a href="http://www.gromacs.org/Downloads">Source code</a></li>
      <li><a href="https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/gromacs">Build and Run Instructions</a></li>
      <li><a href="https://repository.prace-ri.eu/ueabs/GROMACS/2.2/GROMACS_TestCaseA.tar.xz">Test Case A</a></li>
      <li><a href="https://repository.prace-ri.eu/ueabs/GROMACS/2.2/GROMACS_TestCaseB.tar.xz">Test Case B</a></li>
      <li><a href="https://repository.prace-ri.eu/ueabs/GROMACS/2.2/GROMACS_TestCaseC.tar.xz">Test Case C</a></li>
      </ul>
      </td>
      <td>862,079</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td>X</td>
      <td>X</td>
      <td>GROMACS is a versatile package to perform molecular dynamics, i.e. simulate the Newtonian equations of motion for systems with hundreds to millions of particles. It is primarily designed for biochemical molecules like proteins, lipids and nucleic acids that have a lot of complicated bonded interactions, but since GROMACS is extremely fast at calculating the nonbonded interactions (that usually dominate simulations) many groups are also using it for research on non-biological systems, e.g. polymers. 
GROMACS supports all the usual algorithms you expect from a modern molecular dynamics implementation.</td>
    </tr>
    <tr>
      <td>NAMD
        <ul>
      <li><a href="http://www.ks.uiuc.edu/Research/namd/">website</a></li>
      <li><a href="http://www.ks.uiuc.edu/Development/Download/download.cgi?PackageName=NAMD">Source code</a></li>
      <li><a href="https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/namd">Build and Run Instructions</a></li>
      <li><a href="https://repository.prace-ri.eu/ueabs/NAMD/2.2/NAMD_TestCaseA.tar.gz">Test Case A</a></li>
      <li><a href="https://repository.prace-ri.eu/ueabs/NAMD/2.2/NAMD_TestCaseB.tar.gz">Test Case B</a></li>
      <li><a href="https://repository.prace-ri.eu/ueabs/NAMD/2.2/NAMD_TestCaseC.tar.gz">Test Case C</a></li>
      </ul>
      </td>
      <td>887,547</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td></td>
      <td>X</td>
      <td>NAMD is a parallel molecular dynamics code designed for high-performance simulation of large biomolecular systems. Based on Charm++ parallel objects, NAMD scales to hundreds of cores for typical simulations and beyond 500,000 cores for the largest simulations. NAMD uses the popular molecular graphics program VMD for simulation setup and trajectory analysis, but is also file-compatible with AMBER, CHARMM, and X-PLOR. NAMD is distributed free of charge with source code.
      </td>
    </tr>
    <tr>
      <td>NEMO
      <ul>
          <li><a href="https://www.nemo-ocean.eu/">website</a></li>
          <li><a href="https://forge.ipsl.jussieu.fr/nemo/chrome/site/doc/NEMO/guide/html/install.html#download-and-install-the-nemo-code">source</a></li>
          <li><a href="nemo/README.md">instructions</a></li>
          <li><a href="nemo/README.md#verification-of-results">Test Case A</a></li>
          <li><a href="nemo/README.md#verification-of-results">Test Case B</a></li>
        </ul>
      </td>
      <td>154,240</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td>X</td>
      <td></td>
      <td></td>
      <td>X</td>
      <td>NEMO (Nucleus for European Modelling of the Ocean) is a mathematical modelling framework for research activities and prediction services in ocean and climate sciences developed by a European consortium. It is intended to be a tool for studying the ocean and its interaction with the other components of the earth climate system over a large number of space and time scales. It comprises of the core engines namely OPA (ocean dynamics and thermodynamics), SI3 (sea ice dynamics and thermodynamics), TOP (oceanic tracers) and PISCES (biogeochemical process).</td>
    </tr>
    <tr>
      <td>PFARM</td>
      <td>21,434</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td></td>
      <td>PFARM uses an R-matrix ab-initio approach to calculate electron-atom and electron-molecule collisions data for a wide range of applications including atrophysics and nuclear fusion. It is written in modern Fortran/MPI/OpenMP and exploits highly-optimised dense linear algebra numerical library routines.</td>
    </tr>
    <tr>
      <td>QCD</td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>Quantum&nbsp;ESPRESSO
      <ul>
      <li><a href='https://www.quantum-espresso.org/'>Website</a></li>
      <li><a href='https://www.quantum-espresso.org/download-page/'>Source</a></li>
      <li><a href='quantum_espresso/README.md#installation-and-requirements'>Build and Run instructions</a></li>
      <li><a href='https://repository.prace-ri.eu/ueabs/Quantum_Espresso/QuantumEspresso_TestCaseA.tar.gz'>Test Case A</a></li>
      <li><a href='https://repository.prace-ri.eu/ueabs/Quantum_Espresso/QuantumEspresso_TestCaseB.tar.gz'>Test Case B</a></li>
      </ul>
      </td>
      <td>92,996</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td></td>
      <td></td>
      <td>Quantum Espresso is an integrated suite of Open-Source computer codes for electronic-structure calculations and materials modeling at the nanoscale. It is based on density-functional theory, plane waves, and pseudopotentials. It is written in MPI and OpenMP with a CUDA Fortran version
      available for Nvidia GPUs. In the benchmark suite we consider only the most used program, PWscf. </td>
    </tr>
    <tr>
      <td>SPECFEM3D
        <ul>
          <li><a href="https://geodynamics.org/cig/software/specfem3d_globe/">Website</a></li>
          <li><a href="https://github.com/geodynamics/specfem3d_globe.git">Source</a></li>
          <li><a href="specfem3D/README.md">Run and build instructions</a></li>
          <li><a href="specfem3D/test_case/SPECFEM3D_TestCase_A">Test Case A</a></li>
          <li><a href="specfem3D/test_case/SPECFEM3D_TestCase_B">Test Case B</a></li>
          <li><a href="https://github.com/geodynamics/specfem3d_globe/tree/master/EXAMPLES/small_benchmark_run_to_test_more_complex_Earth">Test Case C</a></li>
        </ul>
      </td>
      <td> ~120,000 (100k Fortran & 20k C)</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td></td>
      <td>The software package SPECFEM3D simulates three-dimensional global and regional seismic wave propagation based upon the spectral-element method (SEM).</td>
    </tr>
    <tr>
      <td>TensorFlow
        <ul>
          <li><a href="https://www.tensorflow.org/">website</a></li>
          <li><a href="https://github.com/maxwelltsai/DeepGalaxy">source</a></li>
          <li><a href="tensorflow/README.md">instructions</a></li>
          <li><a href="tensorflow/Testcase_A">Test Case A</a></li>
          <li><a href="tensorflow/Testcase_B">Test Case B</a></li>
          <li><a href="tensorflow/Testcase_C">Test Case C</a></li>
        </ul>
      </td>
      <td>~3,000,000</td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td></td>
      <td>X</td>
      <td>X</td>
      <td>X</td>
      <td>TensorFlow is a popular open-source library for symbolic math and linear algebra, with particular optimisation for neural-networks-based machine learning workflow. Maintained by Google, it is widely used for research and production in both the academia and the industry.</td>
    </tr>
  </tbody>
</table>

_**TODO for all BCOs: move all information below this line either in the Table above (using a short version in "Code Description/Notes") or to your top-level README / description. (And when done, remove the relevant text below.)**_


- [ALYA](#alya)
- [Code_Saturne](#saturne)
- [CP2K](#cp2k)
- [GADGET](#gadget)
- [GPAW](#gpaw)
- [GROMACS](#gromacs)
- [NAMD](#namd)
- [PFARM](#pfarm)
- [QCD](#qcd)
- [SPECFEM3D](#specfem3d)

# ALYA <a name="alya"></a>

The Alya System is a Computational Mechanics code capable of solving different physics, each one with its own modelization characteristics, in a coupled way. Among the problems it solves are: convection-diffusion reactions, incompressible flows, compressible flows, turbulence, bi-phasic flows and free surface, excitable media, acoustics, thermal flow, quantum mechanics (DFT) and solid mechanics (large strain). ALYA is written in Fortran 90/95 and parallelized using MPI and OpenMP.

- Web site: https://www.bsc.es/computer-applications/alya-system
- Code download: https://gitlab.com/bsc-alya/open-alya
- Build and run instructions: https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/alya/README.md
- Test Case A: https://gitlab.com/bsc-alya/benchmarks/sphere-16M
- Test Case B: https://gitlab.com/bsc-alya/benchmarks/sphere-132M

# Code_Saturne <a name="saturne"></a>

Code_Saturne is open-source multi-purpose CFD software, primarily developed by EDF R&D and maintained by them. It relies on the Finite Volume method and a collocated arrangement of unknowns to solve the Navier-Stokes equations, for incompressible or compressible flows, laminar or turbulent flows and non-Newtonian and Newtonian fluids. A highly parallel coupling library (Parallel Locator Exchange - PLE) is also available in the distribution to account for other physics, such as conjugate heat transfer and structure mechanics. For the incompressible solver, the pressure is solved using an integrated Algebraic Multi-Grid algorithm and the scalars are computed by conjugate gradient methods or Gauss-Seidel/Jacobi.

The original version of the code is written in C for pre-postprocessing, IO handling, parallelisation handling, linear solvers and gradient computation, and Fortran 95 for most of the physics implementation. MPI is used on distributed memory machines and OpenMP pragmas have been added to the most costly parts of the code to handle potential shared memory. The version used in this work (also freely available) relies also on CUDA to take advantage of potential GPU acceleration.

The equations are solved iteratively using time-marching algorithms, and most of the time spent during a time step is usually due to the computation of the velocity-pressure coupling, for simple physics. For this reason, the two test cases chosen for the benchmark suite have been designed to assess the velocity-pressure coupling computation, and rely on the same configuration, with a mesh 8 times larger for Test Case B than for Test Case A, the time step being halved to ensure a correct Courant number.

- Web site: https://code-saturne.org
- Code download: https://repository.prace-ri.eu/ueabs/Code_Saturne/2.1/CS_5.3_PRACE_UEABS.tar.gz
- Disclaimer: please note that by downloading the code from this website, you agree to be bound by the terms of the GPL license.
- Build and Run instructions: [code_saturne/Code_Saturne_Build_Run_5.3_UEABS.pdf](code_saturne/Code_Saturne_Build_Run_5.3_UEABS.pdf)
- Test Case A: https://repository.prace-ri.eu/ueabs/Code_Saturne/2.1/CS_5.3_PRACE_UEABS_CAVITY_13M.tar.gz
- Test Case B: https://repository.prace-ri.eu/ueabs/Code_Saturne/2.1/CS_5.3_PRACE_UEABS_CAVITY_111M.tar.gz

# CP2K <a name="cp2k"></a>

CP2K is a freely available quantum chemistry and solid-state physics software package that can perform atomistic simulations of solid state, liquid, molecular, periodic, material, crystal, and biological systems. CP2K provides a general framework for different modelling methods such as DFT using the mixed Gaussian and plane waves approaches GPW and GAPW. Supported theory levels include DFTB, LDA, GGA, MP2, RPA, semi-empirical methods (AM1, PM3, PM6, RM1, MNDO, ...), and classical force fields (AMBER, CHARMM, ...). CP2K can do simulations of molecular dynamics, metadynamics, Monte Carlo, Ehrenfest dynamics, vibrational analysis, core level spectroscopy, energy minimisation, and transition state optimisation using NEB or dimer method.

CP2K is written in Fortran 2008 and can be run in parallel using a combination of multi-threading, MPI, and CUDA. All of CP2K is MPI parallelised, with some additional loops also being OpenMP parallelised. It is therefore most important to take advantage of MPI parallelisation, however running one MPI rank per CPU core often leads to memory shortage. At this point OpenMP threads can be used to utilise all CPU cores without suffering an overly large memory footprint. The optimal ratio between MPI ranks and OpenMP threads depends on the type of simulation and the system in question. CP2K supports CUDA, allowing it to offload some linear algebra operations including sparse matrix multiplications to the GPU through its DBCSR acceleration layer. FFTs can optionally also be offloaded to the GPU. Benefits of GPU offloading may yield improved performance depending on the type of simulation and the system in question.

- Web site: https://www.cp2k.org/
- Code download: https://github.com/cp2k/cp2k/releases
- [Build & run instructions, details about benchmarks](./cp2k/README.md)
- Benchmarks:
 - [Test Case A](./cp2k/benchmarks/TestCaseA_H2O-512)
 - [Test Case B](./cp2k/benchmarks/TestCaseB_LiH-HFX)
 - [Test Case C](./cp2k/benchmarks/TestCaseC_H2O-DFT-LS)

# GADGET <a name="gadget"></a>

GADGET-4 (GAlaxies with Dark matter and Gas intEracT), an evolved and improved version of GADGET-3, is a freely available code for cosmological N-body/SPH simulations on massively parallel computers with distributed memory written mainly by Volker Springel, Max-Plank-Institute for Astrophysics, Garching, Germany, nd benefiting from numerous contributions, including Ruediger Pakmor, Oliver Zier, and Martin Reinecke. GADGET-4 supports collisionless simulations and smoothed particle hydrodynamics on massively parallel computers. All communication between concurrent execution processes is done either explicitly by means of the message passing interface (MPI), or implicitly through shared-memory accesses on processes on multi-core nodes. The code is mostly written in ISO C++ (assuming the C++11 standard), and should run on all parallel platforms that support at least MPI-3. So far, the compatibility of the code with current Linux/UNIX-based platforms has been confirmed on a large number of systems.

The code can be used for plain Newtonian dynamics, or for cosmological integrations in arbitrary cosmologies, both with or without periodic boundary conditions. Stretched periodic boxes, and special cases such as simulations with two periodic dimensions and one non-periodic dimension are supported as well. The modeling of hydrodynamics is optional. The code is adaptive both in space and in time, and its Lagrangian character makes it particularly suitable for simulations of cosmic structure formation. Several post-processing options such as group- and substructure finding, or power spectrum estimation are built in and can be carried out on the fly or applied to existing snapshots. Through a built-in cosmological initial conditions generator, it is also particularly easy to carry out cosmological simulations. In addition, merger trees can be determined directly by the code.

- Web site: https://wwwmpa.mpa-garching.mpg.de/gadget4
- Code download: https://gitlab.mpcdf.mpg.de/vrs/gadget4
- Build and run instructions: https://wwwmpa.mpa-garching.mpg.de/gadget4/02_running.html
- Benchmarks:
    - [Case A: Colliding galaxies with star formation](./gadget/4.0/gadget4-case-A.tar.gz)
    - [Case B: Cosmological DM-only simulation with IC creation](./gadget/4.0/gadget4-case-B.tar.gz)
    - [Case C: Adiabatic collapse of a gas sphere](./gadget/4.0/gadget4-case-C.tar.gz)
- [Code used in the benchmarks](./gadget/4.0/gadget4.tar.gz)
- [Build & run instructions, details about the benchmarks](./gadget/4.0/README.md)


# GROMACS <a name="gromacs"></a>

GROMACS is a versatile package to perform molecular dynamics, i.e. simulate the Newtonian equations of motion for systems with hundreds to millions of particles.

It is primarily designed for biochemical molecules like proteins, lipids and nucleic acids that have a lot of complicated bonded interactions, but since GROMACS is extremely fast at calculating the nonbonded interactions (that usually dominate simulations) many groups are also using it for research on non-biological systems, e.g. polymers.

GROMACS supports all the usual algorithms you expect from a modern molecular dynamics implementation, (check the online reference or manual for details), but there are also quite a few features that make it stand out from the competition:

- GROMACS provides extremely high performance compared to all other programs. A lot of algorithmic optimizations have been introduced in the code; we have for instance extracted the calculation of the virial from the innermost loops over pairwise interactions, and we use our own software routines to calculate the inverse square root. In GROMACS 4.6 and up, on almost all common computing platforms, the innermost loops are written in C using intrinsic functions that the compiler transforms to SIMD machine instructions, to utilize the available instruction-level parallelism. These kernels are available in either single and double precision, and in support all the different kinds of SIMD support found in x86-family (and other) processors.
- Also since GROMACS 4.6, we have excellent CUDA-based GPU acceleration on GPUs that have Nvidia compute capability >= 2.0 (e.g. Fermi or later)
- GROMACS is user-friendly, with topologies and parameter files written in clear text format. There is a lot of consistency checking, and clear error messages are issued when something is wrong. Since a C preprocessor is used, you can have conditional parts in your topologies and include other files. You can even compress most files and GROMACS will automatically pipe them through gzip upon reading.
- There is no scripting language – all programs use a simple interface with command line options for input and output files. You can always get help on the options by using the -h option, or use the extensive manuals provided free of charge in electronic or paper format.
- As the simulation is proceeding, GROMACS will continuously tell you how far it has come, and what time and date it expects to be finished.
- Both run input files and trajectories are independent of hardware endian-ness, and can thus be read by any version GROMACS, even if it was compiled using a different floating-point precision.
- GROMACS can write coordinates using lossy compression, which provides a very compact way of storing trajectory data. The accuracy can be selected by the user.
- GROMACS comes with a large selection of flexible tools for trajectory analysis – you won’t have to write any code to perform routine analyses. The output is further provided in the form of finished Xmgr/Grace graphs, with axis labels, legends, etc. already in place!
- A basic trajectory viewer that only requires standard X libraries is included, and several external visualization tools can read the GROMACS file formats.
- GROMACS can be run in parallel, using either the standard MPI communication protocol, or via our own “Thread MPI” library for single-node workstations.
- GROMACS contains several state-of-the-art algorithms that make it possible to extend the time steps is simulations significantly, and thereby further enhance performance without sacrificing accuracy or detail.
- The package includes a fully automated topology builder for proteins, even multimeric structures. Building blocks are available for the 20 standard aminoacid residues as well as some modified ones, the 4 nucleotide and 4 deoxinucleotide resides, several sugars and lipids, and some special groups like hemes and several small molecules.
- There is ongoing development to extend GROMACS with interfaces both to Quantum Chemistry and Bioinformatics/databases.
- GROMACS is Free Software, available under the GNU Lesser General Public License (LGPL), version 2.1. You can redistribute it and/or modify it under the terms of the LGPL as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.

Instructions:

- Web site: http://www.gromacs.org/
- Code download: http://www.gromacs.org/Downloads The UEABS benchmark cases require the use of a 2020 or newer versio.
- Test Case A: https://repository.prace-ri.eu/ueabs/GROMACS/2.2/GROMACS_TestCaseA.tar.xz
- Test Case B: https://repository.prace-ri.eu/ueabs/GROMACS/2.2/GROMACS_TestCaseB.tar.xz
- Test Case C: https://repository.prace-ri.eu/ueabs/GROMACS/2.2/GROMACS_TestCaseC.tar.xz
- Build and Run Instructions : https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/gromacs/README.md


# NAMD <a name="namd"></a>


NAMD is a widely used molecular dynamics application designed to simulate bio-molecular systems on a wide variety of compute platforms. NAMD is developed by the “Theoretical and Computational Biophysics Group” at the University of Illinois at Urbana Champaign. In the design of NAMD particular emphasis has been placed on scalability when utilizing a large number of processors. The application can read a wide variety of different file formats, for example force fields, protein structure, which are commonly used in bio-molecular science.

A NAMD license can be applied for on the developer’s website free of charge. Once the license has been obtained, binaries for a number of platforms and the source can be downloaded from the website.

Deployment areas of NAMD include pharmaceutical research by academic and industrial users. NAMD is particularly suitable when the interaction between a number of proteins or between proteins and other chemical substances is of interest. Typical examples are vaccine research and transport processes through cell membrane proteins.

NAMD is written in C++ and parallelised using Charm++ parallel objects, which are implemented on top of MPI.

- Web site: http://www.ks.uiuc.edu/Research/namd/
- Code download: https://repository.prace-ri.eu/git/UEABS/ueabs/blob/r1.3/namd/NAMD_Download_README.txt
- Build instructions: https://repository.prace-ri.eu/git/UEABS/ueabs/blob/r1.3/namd/NAMD_Build_README.txt
- Test Case A: https://repository.prace-ri.eu/ueabs/NAMD/2.2/NAMD_TestCaseA.tar.gz
- Test Case B: https://repository.prace-ri.eu/ueabs/NAMD/2.2/NAMD_TestCaseB.tar.gz
- Test Case C: https://repository.prace-ri.eu/ueabs/NAMD/2.2/NAMD_TestCaseC.tar.gz
- Build and Run Instructions : https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/NAMD/README.md

# PFARM <a name="pfarm"></a>

PFARM is part of a suite of programs based on the ‘R-matrix’ ab-initio approach to the variational solution of the many-electron Schrödinger
equation for electron-atom and electron-ion scattering. The package has been used to calculate electron collision data for astrophysical
applications (such as: the interstellar medium, planetary atmospheres) with, for example, various ions of Fe and Ni and neutral O, plus
other applications such as data for plasma modelling and fusion reactor impurities. The code has recently been adapted to form a compatible
interface with the UKRmol suite of codes for electron (positron) molecule collisions thus enabling large-scale parallel ‘outer-region’
calculations for molecular systems as well as atomic systems.

The PFARM outer-region application code EXDIG is domi-nated by the assembly of sector Hamiltonian matrices and their subsequent eigensolutions.
The code is written in Fortran 2003 (or Fortran 2003-compliant Fortran 95), is parallelised using MPI and OpenMP and is designed to take
advantage of highly optimised, numerical library routines. Hybrid MPI / OpenMP parallelisation has also been introduced into the code via
shared memory enabled numerical library kernels.

Accelerator-based implementations have been implemented for EXDIG, using off-loading (MKL or CuBLAS/CuSolver) for the standard (dense) eigensolver calculations that dominate overall run-time.

Code download: https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/pfarm
- Build & Run instructions: https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/pfarm/PFARM_Build_Run_README.txt
- Test Case 1: https://repository.prace-ri.eu/ueabs/PFARM/2.2/test_case_1_atom
- Test Case 2: https://repository.prace-ri.eu/ueabs/PFARM/2.2/test_case_2_mol



# QCD <a name="qcd"></a>

| **General information**                 | **Scientific field** | **Language** | **MPI** | **OpenMP** | **GPU**             | **LoC** | **Code description**                                                                                                                                  |
|------------------|----------------------|--------------|---------|------------|---------------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| <br>[- Bench](https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/qcd/part_1) <br>[- Summary](https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/qcd/part_1/README.md) | lattice Quantum Chromodynamics        Part 1 | C      | yes     | yes        | yes (CUDA) | --  | Accelerator enabled kernel E of UEABS QCD CPU part using targetDP model. Test case A - 8x64x64x64. Conjugate Gradient solver involving Wilson Dirac stencil.  Domain Decomposition, Memory bandwidth, strong scaling, MPI latency. |
| <br>[- Source](https://lattice.github.io/quda/) <br>[- Bench](https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/qcd/part_2) <br>[- Summary](https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/qcd/part_2/README.md) | lattice Quantum Chromodynamics        Part 2 - QUDA | C++     | yes     | yes        | yes (CUDA) | --  | Part 2: GPU is using a QUDA kernel for running on NVIDIA GPUs. [Test case A - 96x32x32x32] Small problem size. CG solver. Domain Decomposition, Memory bandwidth, strong scaling, MPI latency. [Test case B - 126x64x64x64] Moderate problem size. CG solver on Wilson Dirac stencil. Bandwidth bounded |
| <br>[- Source](http://jeffersonlab.github.io/qphix/) <br>[- Bench](https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/qcd/part_2) <br>[- Summary](https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/qcd/part_2/README.md) | lattice Quantum Chromodynamics        Part 2 - QPHIX | C++      | yes     | yes        | no | --  | Part 2: Xeon(Phi) is using a QPhiX kernel which is optimize to run on x86, in particular Intel Xeon (Phi). [Test case A - 96x32x32x32] Small problem size. CG solver involving Wilson Dirac stencil.  Domain Decomposition, Memory bandwidth, strong scaling, MPI latency. [Test case B - 126x64x64x64] Moderate problem size. CG solver on Wilson Dirac stencil. Bandwidth bounded |
| <br>[- Source](https://repository.prace-ri.eu/ueabs/QCD/1.3/QCD_Source_TestCaseA.tar.gz) <br>[- Bench](https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/qcd/part_cpu) <br>[- Summary](https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/qcd/part_cpu/README.md) | lattice Quantum Chromodynamics - CPU Part - legacy UEABS | C/Fortran      | yes     | yes/no        | No | --  | CPU part based on UEABS QCD CPU part (legacy) benchmark kernels (last update 2017). Based on 5 different Benchmark applications representative for the European Lattice QCD community (see doc for more details). |



# SPECFEM3D <a name="specfem3d"></a>
The software package SPECFEM3D simulates three-dimensional global and regional seismic wave propagation based upon the spectral-element method (SEM). All SPECFEM3D_GLOBE software is written in Fortran90 with full portability in mind, and conforms strictly to the Fortran95 standard. It uses no obsolete or obsolescent features of Fortran77. The package uses parallel programming based upon the Message Passing Interface (MPI).

- Web site: http://geodynamics.org/cig/software/specfem3d_globe/
- Code download: http://geodynamics.org/cig/software/specfem3d_globe/
- Test Case A: https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/specfem3d/test_cases/SPECFEM3D_TestCaseA
- Test Case B: https://repository.prace-ri.eu/git/UEABS/ueabs/-/tree/r2.2-dev/specfem3d/test_cases/SPECFEM3D_TestCaseB
- Test Case C (Validation case) : https://github.com/geodynamics/specfem3d_globe/tree/master/EXAMPLES/small_benchmark_run_to_test_more_complex_Earth
- Run & and build instructions: https://repository.prace-ri.eu/git/UEABS/ueabs/-/blob/r2.2-dev/specfem3d/README.md
