#!/bin/bash
#
# Configure the modules for Vaughan.
#

gpaw_20_1=1
gpaw_20_10=1
gpaw_21_1=1
gpaw_vaughan=0
gpaw_Cfg1=1
gpaw_Cfg2=0
gpaw_Cfg3=0
gpaw_Cfg4=0
gpaw_Cfg5=1
gpaw_UEABS=1

use_modules=()
# Add the vaughan modules
# Note that this will need an additional directory in the MODULEPATH.
if (( $gpaw_vaughan )); then
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-Python-3.8.3')                            # Vaughan module
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-Python-3.8.3-MKLFFTW')                    # Vaughan module
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-IntelPython3')                            # Vaughan module
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-IntelPython3-MKLFFTW')                    # Vaughan module
fi
# Cfg1
if (( $gpaw_Cfg1 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-FFTW-Cfg1-UEABS-GPAW')          # Cfg1
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-MKLFFTW-Cfg1-UEABS-GPAW')       # Cfg1
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-FFTW-Cfg1-UEABS-GPAW')         # Cfg1
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-MKLFFTW-Cfg1-UEABS-GPAW')      # Cfg1
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-FFTW-Cfg1-UEABS-GPAW')          # Cfg1
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-MKLFFTW-Cfg1-UEABS-GPAW')       # Cfg1
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1
fi
# Cfg3
if (( $gpaw_Cfg3 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg3-UEABS-GPAW')      # Cfg3
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-MKLFFTW-Cfg3-UEABS-GPAW')   # Cfg3
fi
# Cfg4
if (( $gpaw_Cfg4 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg4-UEABS-GPAW')      # Cfg4
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-MKLFFTW-Cfg4-UEABS-GPAW')   # Cfg4
fi
# Cfg5
if (( $gpaw_Cfg5 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg5-UEABS-GPAW')      # Cfg5
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-MKLFFTW-Cfg5-UEABS-GPAW')   # Cfg5
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-NumPyFFT-Cfg5-UEABS-GPAW')  # Cfg5
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-FFTW-Cfg5-UEABS-GPAW')     # Cfg5
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-MKLFFTW-Cfg5-UEABS-GPAW')  # Cfg5
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-NumPyFFT-Cfg5-UEABS-GPAW') # Cfg5
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-FFTW-Cfg5-UEABS-GPAW')      # Cfg5
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-MKLFFTW-Cfg5-UEABS-GPAW')   # Cfg5
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-NumPyFFT-Cfg5-UEABS-GPAW')  # Cfg5
fi
# GPAW-UEABS
if (( $gpaw_UEABS )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/20.1.0-IntelPython3-icc')                                 # UEABS IntelPython3-based Archive
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW-UEABS/20.10.0-IntelPython3-icc')                                # UEABS IntelPython3-based Archive
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/21.1.0-IntelPython3-icc')                                 # UEABS IntelPython3-based Archive
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/20.1.0-Python38icc-FFTW-icc')                             # UEABS Python EasyBuild + FFTW Intel
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW-UEABS/20.10.0-Python39icc-FFTW-icc')                            # UEABS Python EasyBuild + FFTW Intel
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/21.1.0-Python39icc-FFTW-icc')                             # UEABS Python EasyBuild + FFTW Intel
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/20.1.0-Python38-FFTW-icc')                                # UEABS Python + FFTW Intel
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW-UEABS/20.10.0-Python39-FFTW-icc')                               # UEABS Python + FFTW Intel
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/21.1.0-Python39-FFTW-icc')                                # UEABS Python + FFTW Intel
fi


module purge
MODULEPATH=$VSC_SCRATCH/UEABS/CalcUA-$VSC_INSTITUTE_CLUSTER-$VSC_ARCH_LOCAL/Modules

for (( c1=0; c1<${#use_modules[*]}; c1++ ))
do

    echo -e "\n\033[31mTrying to load ${use_modules[$c1]}.\033[0m"

    module purge
    module load ${use_modules[$c1]}
    module list

done
