# Installation scripts for CalcUA-leibniz-broadwell

## Install subdirectory

The script `prepare.sh` can be used to set up the directory structure, populate the
Modules-link directory, set up EasyBuild and also set up the module UEABS which contains
a number of environment variables that are useful to point to the different file locations
and tools versions and a number of bash functions that are useful for managing the
project and build process.

The script `rebuild_pbs.sh` submits a number of Torque jobs to build all tested configurations,
both with EasyBuild and direct builds (the latter submitted to the UEABS GitLab).


## Testing subdirectory

  * ``test-load-modules.sh``: Tries to load all modules. The code is at the same time
    a test bed for similar code in the common part of the jobscripts.

## Jobs subdirectory

Code that is common to the job scripts independent of the core configuration or the
test case.


