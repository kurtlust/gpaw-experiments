###############################################################################
#
# Common part of the job script.
#
# Variables used:
#
# - Name of the input file, path relative to this directory
# inputfile='../input.py'
#
# - Determine the number of runs
# nr_runs=3
# do_cold=0
#
# - Put all output files in the slrum .out file
# embed_output=0
#
# - Determine the set of modules to run for
# quick=1         # Overwrites everything, just for a quick test with 3 versions of the code.
# gpaw_20_1=1     # Run tests with GPAW 20.1.0
# gpaw_20_10=1    # Run tests with GPAW 20.10.0
# gpaw_21_1=1     # Run tests with GPAW 21.1.0
# gpaw_vaughan=0  # Run test using the regular Vaughan modules
# gpaw_Cfg1=1     # Run tests using the Cfg1 module EasyBuild module family
# gpaw_Cfg2=0     # Run tests using the Cfg2 module EasyBuild module family
# gpaw_Cfg3=0     # Run tests using the Cfg3 module EasyBuild module family
# gpaw_Cfg4=0     # Run tests using the Cfg4 module EasyBuild module family
# gpaw_Cfg5=1     # Run tests using the Cfg5 module EasyBuild module family
# gpaw_UEABS=1    # Run tests with the manual builds for UEABS
#

cd $PBS_O_WORKDIR

module purge
MODULEPATH=$VSC_SCRATCH/UEABS/CalcUA-$VSC_INSTITUTE_CLUSTER-$VSC_ARCH_LOCAL/Modules

jobid=$PBS_JOBID
jobname=$PBS_JOBNAME

summary="results-${jobname}-${jobid}.csv"

use_modules=()
# Add the leibniz modules
# Note that this will need an additional directory in the MODULEPATH.
if (( $gpaw_leibniz )); then
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-Python-3.8.3')                            # Leibniz module
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-Python-3.8.3-MKLFFTW')                    # Leibniz module
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-IntelPython3')                            # Leibniz module
    (( $gpaw_20_1 ))  && use_modules+=('calcua/2020a GPAW/20.1.0-intel-2020a-IntelPython3-MKLFFTW')                    # Leibniz module
fi
# Cfg1
if (( $gpaw_Cfg1 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-FFTW-Cfg1-UEABS-GPAW')          # Cfg1
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-MKLFFTW-Cfg1-UEABS-GPAW')       # Cfg1
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-FFTW-Cfg1-UEABS-GPAW')         # Cfg1
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-MKLFFTW-Cfg1-UEABS-GPAW')      # Cfg1
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-FFTW-Cfg1-UEABS-GPAW')          # Cfg1
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-MKLFFTW-Cfg1-UEABS-GPAW')       # Cfg1
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1
fi
# Cfg3
if (( $gpaw_Cfg3 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg3-UEABS-GPAW')      # Cfg3
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-MKLFFTW-Cfg3-UEABS-GPAW')   # Cfg3
fi
# Cfg4
if (( $gpaw_Cfg4 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg4-UEABS-GPAW')      # Cfg4
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-MKLFFTW-Cfg4-UEABS-GPAW')   # Cfg4
fi
# Cfg5
if (( $gpaw_Cfg5 )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg5-UEABS-GPAW')      # Cfg5
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-MKLFFTW-Cfg5-UEABS-GPAW')   # Cfg5
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-NumPyFFT-Cfg5-UEABS-GPAW')  # Cfg5
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-FFTW-Cfg5-UEABS-GPAW')     # Cfg5
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-MKLFFTW-Cfg5-UEABS-GPAW')  # Cfg5
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-NumPyFFT-Cfg5-UEABS-GPAW') # Cfg5
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-FFTW-Cfg5-UEABS-GPAW')      # Cfg5
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-MKLFFTW-Cfg5-UEABS-GPAW')   # Cfg5
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-NumPyFFT-Cfg5-UEABS-GPAW')  # Cfg5
fi
# GPAW-UEABS
if (( $gpaw_UEABS )); then
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/20.1.0-IntelPython3-icc')                                 # UEABS IntelPython3-based Archive
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW-UEABS/20.10.0-IntelPython3-icc')                                # UEABS IntelPython3-based Archive
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/21.1.0-IntelPython3-icc')                                 # UEABS IntelPython3-based Archive
    (( $gpaw_20_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/20.1.0-Python38-FFTW-icc')                                # UEABS Python + FFTW Intel
    (( $gpaw_20_10 )) && use_modules+=('UEABS/2.2 GPAW-UEABS/20.10.0-Python39-FFTW-icc')                               # UEABS Python + FFTW Intel
    (( $gpaw_21_1 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/21.1.0-Python39-FFTW-icc')                                # UEABS Python + FFTW Intel
fi
#
# Overwrite for a quick test
#
if (( $quick )); then
    use_modules=( \
      'UEABS/2.2 GPAW/20.1.0-intel-2020a-Python_icc-3.8.7-FFTW-Cfg5-UEABS-GPAW' \
      'UEABS/2.2 GPAW/20.10.0-intel-2020a-Python_icc-3.9.1-FFTW-Cfg5-UEABS-GPAW' \
      'UEABS/2.2 GPAW/21.1.0-intel-2020a-Python_icc-3.9.1-FFTW-Cfg5-UEABS-GPAW' \
    )
fi

#
# Create a subdirectory and run from there to separate the output
#
/usr/bin/mkdir -p "results-${jobname}-${jobid}"
cd "results-${jobname}-${jobid}"

#
# Store some additional information about the job in the Slurm output
#
# - The job script

cat <<EOF

###############################################################################
#
# PBS job script
#

EOF

cat $0

# - The common part (this file)

cat <<EOF

###############################################################################
#
# Common/sourced part of the SLURM job script
#

EOF

cat ${VSC_DATA}/Projects/PRACE/GPAW-experiments/Systems/CalcUA-leibniz-broadwell/Jobs/benchmark-multiple.pbs.sh

# - The input file

cat <<EOF

###############################################################################
#
# Input file
#

EOF

echo -e "Input file: $inputfile\n"

cat ../$inputfile

cat <<EOF

###############################################################################
#
# Regular output
#

EOF

#
# Do the actual runs
#

echo '"Module", "gpaw/ase/numpy/scipy", "test", "run", "time", "iterations", "dipole", "fermi", "energy"' >$summary

for (( c1=0; c1<${#use_modules[*]}; c1++ ))
do

  c1string=$(printf "%02d" $(($c1+1)))

  # Load the appropriate modules
  module purge
  module load ${use_modules[$c1]}

  echo -e "\n###############################################################################" \
          "\n#\n# run $((c1+1)): ${use_modules[$c1]}\n#" \
          "\n###############################################################################\n" \
          "\nModules loaded:\n"
  module list

  python_version=$(python -V)
  gpaw_version=$(python -c "import gpaw ; print( gpaw.__version__ )")
  ase_version=$(python -c "import ase ; print( ase.__version__ )")
  numpy_version=$(python -c "import numpy ; print( numpy.__version__ )")
  scipy_version=$(python -c "import scipy ; print( scipy.__version__ )")

  echo -e "\nInfo from Python:\n" \
          " * Python interpreter: $python_version\n" \
          " * gpaw $gpaw_version\n" \
          " * ase $ase_version\n" \
          " * numpy $numpy_version\n" \
          " * scipy $scipy_version\n"

  # Is this pre-20.1.0 or after?
  if [[ $(which gpaw-python 2> /dev/null) == *"gpaw-python" ]]
  then
    start_mode=0
  else
    start_mode=1
  fi

  if (( $do_cold != 0 ))
  then
    # Do a cold run first
    echo -e "\n###############################################################################" \
            "\n#\n# ${use_modules[$c1]} cold run\n#\n"

    [ $start_mode -eq 0 ] && mpirun gpaw-python ../$inputfile
    [ $start_mode -eq 1 ] && mpirun gpaw python ../$inputfile

    cat output.txt
    mv output.txt output-${c1string}-cold-${jobname}-${jobid}.txt

  fi

  for (( c2=1; c2<=$nr_runs; c2++ ))
  do

    c2string=$(printf "%02d" $c2)

    echo -e "\n###############################################################################" \
            "\n#\n# ${use_modules[$c1]} run ${c2string}\n#\n"

    [ $start_mode -eq 0 ] && mpirun gpaw-python ../$inputfile
    [ $start_mode -eq 1 ] && mpirun gpaw python ../$inputfile

    # Do we want the output file in the slurm.out file?
    (( $embed_output )) && cat output.txt
    # Extract some data to report form the output file.
    bmtime=$(grep "Total:" output.txt | sed -e 's/Total: *//' | cut -d " " -f 1)
    iterations=$(grep "Converged after" output.txt | cut -d " " -f 3)
    dipole=$(grep "Dipole" output.txt | cut -d " " -f 5 | sed -e 's/)//')
    fermi=$(grep "Fermi level:" output.txt | cut -d ":" -f 2 | sed -e 's/ //g')
    energy=$(grep "Extrapolated: " output.txt | cut -d ":" -f 2 | sed -e 's/ //g')
    # Output to the slurm.out file
    echo -e "\nResult information:\n" \
        " * Time:                   $bmtime s\n" \
        " * Number of iterations:   $iterations\n" \
        " * Dipole (3rd component): $dipole\n" \
        " * Fermi level:            $fermi\n" \
        " * Extrapolated energy:    $energy\n"
    # Output to the summary spreadsheet
    echo "\"${use_modules[$c1]}\", \"$gpaw_version/$ase_version/$numpy_version/$scipy_version\"," \
         "\"$((c1+1))\", \"$c2\", \"$bmtime\", \"$iterations\", \"$dipole\", \"$fermi\", \"$energy\"" >> $summary
    mv output.txt output-${c1string}-${c2string}-${jobname}-${jobid}.txt

  done

done

#
# Summary information
#

cat <<EOF

###############################################################################
#
# Summary information
#

EOF

cat $summary
