#
# The first part of this script, up to and including the building of EasyBuild, is done
# in the script itself as this is a very quick process. After that, the building of
# the software is organised by multiple job submissions and job dependencies to make
# sure the compilation advances a bit faster if the cluster has enough available
# capacity. We could of course do even better by putting each EasyBuild build
# in a separate process but that would become cumbersome.
#

#
# Version of the UEABS benchmark
#
ueabs_version='2.2'

# Name of the site and the identifier that we use for the system.
# We tend to combine the name of the cluster with the processor architecture which
# helps if the cluster has several CPU partitions with a different architecture.
site="CalcUA"
system=leibniz-broadwell

# Initialise module system for the build
module purge
MODULEPATH=$VSC_SCRATCH/UEABS/$site-$system/Modules:$VSC_SCRATCH/UEABS/$site-$system/Modules-link
module load UEABS/$ueabs_version


###############################################################################
###############################################################################
#
# Now we start the builds, using job dependencies
#
# Note that due to the way the clean-up works in the scripts, one need to run
# each script on a dedicated node.
#
###############################################################################
###############################################################################


#
# Ensure variables that are needed by some of the scripts are properly exported
#
export JOBMODULEPATH=$MODULEPATH
export ueabs_version

#
# First phase: Build the buildtools module to have a consistent set of build tools.
#
cd $UEABS_EBREPOSITORY/UEABS/buildtools
buildtools_jobid=$(qsub -L tasks=1:lprocs=28 -l walltime=45:00 -v JOBMODULEPATH,ueabs_version -N UEABS_buildtools build_buildtools.sh)

#
# Build GPAW modules from the all-in-one configurations.
#
cd $UEABS_EBREPOSITORY/UEABS/GPAW/1_Bundle
qsub -L tasks=1:lprocs=28 -l walltime=24:00:00 -v JOBMODULEPATH,ueabs_version -N UEABS_Cfg1 -W depend=afterok:$buildtools_jobid build_Cfg1.sh

#
# Build GPAW modules from the two bundle configuration.
#
#cd $UEABS_EBREPOSITORY/UEABS/GPAW/2_TwoBundle
#qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,ueabs_version -N UEABS_Cfg2 -W depend=afterok:$buildtools_jobid build_Cfg2.sh

#
# Build GPAW modules from the third configuration: Python as a bundle
#
cd $UEABS_EBREPOSITORY/UEABS/GPAW/3_PythonBundle
#qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,ueabs_version -N UEABS_Cfg3 -W depend=afterok:$buildtools_jobid build_Cfg3.sh

#
# Build GPAW modules from the fourth configuration: Python dependencies only as a bundle
#
cd $UEABS_EBREPOSITORY/UEABS/GPAW/4_PythonDepsBundle
#qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,ueabs_version -N UEABS_Cfg4 -W depend=afterok:$buildtools_jobid build_Cfg4.sh

#
# Build GPAW modules from the fifth configuration: All separate modules
#
cd $UEABS_EBREPOSITORY/UEABS/GPAW/5_AllSeparate
Cfg5_python_jobid=$(qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,ueabs_version -N UEABS_Cfg5_step1 -W depend=afterok:$buildtools_jobid build_Cfg5_python.sh)
qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,ueabs_version -N UEABS_Cfg5_step2 -W depend=afterok:$Cfg5_python_jobid build_Cfg5_GPAW.sh

#
# Rebuild the GPAW-UEABS modules
#
# We could do all builds in parallel as soon as they have their dependencies, but if all those jobs would
# launch simulataneously, we experience a very high load on the scratch file system.
#
# Note also that if the necessary files are not downloaded beforehand, running those scripts in parallel may have
# undesired effects as they all download their sources to the same directory.
#

cd $UEABS_REPOSITORY/gpaw/build/examples/$UEABS_SITE-$UEABS_SYSTEM

qsub -L tasks=1:lprocs=28 -l walltime=24:00:00 -N UEABS_GPAW_UEABS_independent -W depend=afterok:$buildtools_jobid build_independent.sh
