#!/bin/bash

#
# Version of the UEABS benchmark
#
ueabs_version='2.2'

# Name of the site and the identifier that we use for the system.
# We tend to combine the name of the cluster with the processor architecture which
# helps if the cluster has several CPU partitions with a different architecture.
site="CalcUA"
system=leibniz-broadwell

# Directory with this repository and where all valuable files will be stored.
repo_dir="${VSC_DATA}/Projects/PRACE/GPAW-experiments"
# Scratch directory where files will be stored that can be easily regenerated.
# It can be on the same file system as $repo_dir. And in fact, even
# $repo_dir/EasyBuild/Systems/$site-$system would still work as no directories that already
# exist in there are overwritten. But it does make git management a bit more
# difficult.
ueabs_work_dir="${VSC_SCRATCH}/UEABS"

# Directory with the UEABS repo. We currently do not refer to a clone of the official
# repo but to our work version in the $repo_dir repo.
ueabs_repo_dir=$repo_dir/UEABS

easybuild_version=4.3.1

EB_dir="$repo_dir/EasyBuild"
EBsys_dir="$repo_dir/EasyBuild/Systems/$site-$system"

#
# Enable aliases in shell scripts
#
# shopt -s expand_aliases

#
# Create the directory structure
#

# All builds
moduleslink_dir=$ueabs_work_dir/$site-$system/Modules-link
modules_dir=$ueabs_work_dir/$site-$system/Modules
packages_dir=$ueabs_work_dir/$site-$system/Packages
# Manual builds
downloads_dir=$ueabs_work_dir/Downloads
# EasyBuild-only
EBpackages_dir=$packages_dir
EBmodules_dir=$modules_dir
EBrepolink_dir=$EBsys_dir/Repo-link
EBrepo_dir=$ueabs_work_dir/$site-$system/Repo
EBsources_dir=$ueabs_work_dir/Sources
EBetc_dir=$repo_dir/EasyBuild/etc

cd $ueabs_work_dir
# All software installations - EasyBuild and manual
mkdir -p $moduleslink_dir
mkdir -p $modules_dir
mkdir -p $packages_dir
# Manual installations
mkdir -p $downloads_dir
# EasyBuild installations
mkdir -p $EBrepo_dir
mkdir -p $EBsources_dir
# The next two EasyBuild directories should exist already from the repo we
# store this script in, but you never know...
mkdir -p $EBrepolink_dir
mkdir -p $EBetc_dir

#
# Create the Modules-link structure
#

cd $moduleslink_dir

mkdir -p $moduleslink_dir/EasyBuild ; cd $moduleslink_dir/EasyBuild 
[[ -f $easybuild_version.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-x86_64/EasyBuild/$easybuild_version.lua

mkdir -p $moduleslink_dir/binutils ; cd $moduleslink_dir/binutils
[[ -f 2.34-GCCcore-9.3.0.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/binutils/2.34-GCCcore-9.3.0.lua

mkdir -p $moduleslink_dir/GCCcore ; cd $moduleslink_dir/GCCcore
[[ -f 9.3.0.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/GCCcore/9.3.0.lua

mkdir -p $moduleslink_dir/intel ; cd $moduleslink_dir/intel
[[ -f 2020a.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-admin-x86_64/intel/2020a.lua

mkdir -p $moduleslink_dir/iimpi ; cd $moduleslink_dir/iimpi
cat >>2020a.lua <<EOF
help(  [[Dummy iimpi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iimpi module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink_dir/impi ; cd $moduleslink_dir/impi
cat >>phony.lua <<EOF
help(  [[Dummy impi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy impi module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink_dir/imkl ; cd $moduleslink_dir/imkl
cat >>phony.lua <<EOF
help(  [[Dummy imkl module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy imkl module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink_dir/iccifort ; cd $moduleslink_dir/iccifort
cat >>phony.lua <<EOF
help(  [[Dummy iccifort module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iccifort module to make easybuild happy with our intel toolchain.]])
EOF

#
# Generate EasyBuild configuration file
# Rather then using the gpp route, we'll generate it using a bash here document
# as not all systems have gpp installed and as it is clearer to outsiders what's
# happening.
#

cd $EB_dir/etc

# gpp-generated equivalent:
# gpp -DDATADIR=$repo_dir/EasyBuild -DSCRATCHDIR=$ueabs_work_dir -DSITE=$site -DSYSTEM=$system -DUSER=$USER \
#   -o ${site}-${system}.cfg-gen template.cfg-tmpl
# EOF
cat >${site}-${system}.cfg-gen <<EOF
# EasyBuild configuration file for UEABS builds using the recipes in this
# repository and the directory structure as explained in the
# Systems/README.md-file in the repository.

[MAIN]

[basic]
robot=$ueabs_work_dir/$site-$system/Repo:$repo_dir/EasyBuild/UEABS:$repo_dir/EasyBuild/Systems/$site-$system/Repo-link

[config]
buildpath=/dev/shm/$USER
installpath-software=$EBpackages_dir
installpath-modules=$EBmodules_dir
repositorypath=$EBrepo_dir
sourcepath=$EBsources_dir
module-syntax=Lua
modules-tool=Lmod
include-module-naming-schemes=$EB_dir/module_naming_scheme/*.py
module-naming-scheme=CalcUAMNS
suffix-modules-path=
include-easyblocks=$EB_dir/Custom-EasyBlocks/*/*.py
#include-toolchains=$EB_dir/Custom-toolchains/*/*.py

[override]
allow-loaded-modules=EasyBuild,EasyBuild-UEABS
check-ebroot-env-vars=ignore

EOF


#
# Build a module with our goodies and environment variable definitions for 
# consistent builds.
#

mkdir -p $modules_dir/UEABS ; cd  $modules_dir/UEABS
cat >$ueabs_version.lua <<EOF
whatis([==[Environment (including functions) for building and running the UEABS benchmark suite (version $ueabs_version)]==])

help([==[

Description
===========
This module sets up the environment for building and running the UEABS 
benchmarks on this system. It defines a number of environment variables
pointing to various directories for sources, software and modules, and
a number of functions that are useful for managing the packages.


Usage
=====

The following general environment variables are defined:
  * UEABS_VERSION: Version of the UEABS benchmark. MAy seem useless if
    it is already used in the name of the module, but still easy to
    use in various scripts.
  * UEABS_SITE: Site for which this module is intended to be used.
  * UEABS_SYSTEM: System for which this module is intended to be used.
  * UEABS_REPOSITORY: Place where the UEABS repo is stored in the file system.
    This can be a clone of the official repo, or a working version
    contained in another development repo, depending on how this
    module was generated.
  * UEABS_PACKAGES: Directory where software packages can be installed.
    This directory is organised in the EasyBuild-way (packages are in 
    a subdirectory <name of package>/<version string of the module>
    corresponding to the module name) though it can be used for other
    installs also.
  * UEABS_MODULES: Directory for the module files corresponding to the
    packages in $UEABS_packages.
  * UEABS_MODULESLINK: Directory used to link to existing system modules.
    This is useful if one wants to create a personal build environment that
    is as isolated as possible from the system one without reinstalling
    everything, to ensure full control over what software is being used.
  * UEABS_DOWNLOADS: Directory to dump downloaded software without having
    to organize it the way EasyBuild (or another package manager) would
    prefer it.
The following environment variables are specified for using EasyBuild:
  * UEABS_EBREPOSITORY: EasyBuild directory structure in the experiments
    repository.
  * UEABS_EBPACKAGES: Directory where EasyBuild installs software packages,
    currently a synonym for UEABS_PACKAGES
  * UEABS_EBMODULES: Directory where EasyBuild installs the modules,
    currently a synonym of UEABS_MODULES.
  * UEABS_EBREPO: Repo directory for EasyBuild for all software that is
    installed using EasyBuild in UEABS_PACKAGES/UEABS_MODULES.
  * UEABS_EBREPOLINK: Repo-compatible directory for dummy EasyConfig files
    to be able to use the modules in UEABS_MODULESLINK in EasyBuild.
  * UEABS_EBSOURCES: Directory for software downloads organised the 
    EasyBuild way.
  * UEABS_EBETC: Directory with the EasyBuild configuration files.
  * UEABS_EBVERSION: Version of EasyBuild used for this version of the 
    UEABS benchmarks.
  * EASYBUILD_CONFIGFILES: Points to the configuration file for EasyBuild,
    so that we can simply use a system-installed EasyBuild (if any) and 
    the eb-command without having to specify the configuration file.

The following bash functions are defined:
  * clear-tmp: Looks in /dev/shm and /tmp for files and directories owned
    by the current user and erases them.
  * clear-build: Erases a specific version of a package (when used with
    the name of the module as the argument) or all versions (when just
    using the package name).
  * echopath: Prints a PATH-style variable in a more userfriendly way.
    It takes one argument: the name of the PATH-style variable (not the
    value, so without the $)

The following aliases are defined
  * cdp and pdp: Change to / push to the directory containing the repository
    for our UEABS experiments.
  * cdpw and pdpw: Change to / push to the work directory
  * cdpe and pdpe: Change to / push to the EasyBuild tree in the repository.
  * cdpu and pdpu: Change to / push to the UEABS repository or work version
    that we use.
]==])

conflict('UEABS')

prepend_path( 'MODULEPATH', "$moduleslink_dir" )
prepend_path( 'MODULEPATH', "$modules_dir" )

setenv( 'UEABS_VERSION',      "$ueabs_version" )
setenv( 'UEABS_SITE',         "$site" )
setenv( 'UEABS_SYSTEM',       "$system" )

setenv( 'UEABS_REPOSITORY',   "$ueabs_repo_dir" )
setenv( 'UEABS_PACKAGES',     "$packages_dir" )
setenv( 'UEABS_MODULES',      "$modules_dir" )
setenv( 'UEABS_MODULESLINK',  "$moduleslink_dir" )
setenv( 'UEABS_DOWNLOADS',    "$downloads_dir" )

setenv( 'UEABS_EBREPOSITORY', "$EB_dir" )
setenv( 'UEABS_EBPACKAGES',   "$EBpackages_dir" )
setenv( 'UEABS_EBMODULES',    "$EBmodules_dir" )
setenv( 'UEABS_EBREPO',       "$EBrepo_dir" )
setenv( 'UEABS_EBREPOLINK',   "$EBrepolink_dir" )
setenv( 'UEABS_EBSOURCES',    "$EBsources_dir" )
setenv( 'UEABS_EBETC',        "$EBetc_dir" )
setenv( 'UEABS_EBVERSION',    "$easybuild_version" )

setenv( 'EASYBUILD_CONFIGFILES', "$EBetc_dir/$site-$system.cfg-gen" )

prepend_path( 'MODULEPATH', "$moduleslink_dir" )
prepend_path( 'MODULEPATH', "$modules_dir" )

--
-- Shell function clear-build, only implemented for bash
--
set_shell_function('clear-build',
[==[
  local root="$ueabs_work_dir/$site-$system" ;

  local modulename=\$1 ;
  local package=\${modulename%/*} ;
  local version=\${modulename#*/} ;

  if [[ \$package == \$version ]] ;
  then
    [[ -d \$root/Packages/\$package ]] && /bin/rm -rf \$root/Packages/\$package ;
    [[ -d \$root/Repo/\$package ]]     && /bin/rm -rf \$root/Repo/\$package ;
    [[ -d \$root/Modules/\$package ]]  && /bin/rm -rf \$root/Modules/\$package ;
  else
    [[ -d \$root/Packages/\$package/\$version ]]         && /bin/rm -rf \$root/Packages/\$package/\$version ;
    [[ -f \$root/Repo/\$package/\$package-\$version.eb ]] && /bin/rm -f  \$root/Repo/\$package/\$package-\$version.eb ;
    [[ -f \$root/Modules/\$package/\$version.lua ]]      && /bin/rm -f  \$root/Modules/\$package/\$version.* ;
  fi
]==]
, "echo 'Not implemented'")


--
-- Shell function clear-tmp, only implemented for bash
--
set_shell_function('clear-tmp',
[==[
    find /tmp     -maxdepth 1 -user \$USER -exec /bin/rm -rf '{}' \;;
    find /dev/shm -maxdepth 1 -user \$USER -exec /bin/rm -rf '{}' \;;
]==]
, "echo 'Not implemented'")

--
-- Shell function echopath, only implemented for bash
--
set_shell_function('echopath',
[==[
    echo "\$1=";
    eval echo \\\$\$1 | tr ":" "\n" | nl -]==]
, "echo 'Not implemented'")

--
-- Aliases
--
set_alias( 'cdp',  "cd $repo_dir" )
set_alias( 'pdp',  "pushd $repo_dir" )
set_alias( 'cdpw', "cd $ueabs_work_dir" )
set_alias( 'pdpw', "pushd $ueabs_work_dir" )
set_alias( 'cdpe', "cd $EB_dir" )
set_alias( 'pdpe', "pushd $EB_dir" )
set_alias( 'cdpu', "cd $ueabs_repo_dir" )
set_alias( 'pdpu', "pushd $ueabs_repo_dir" )
EOF


#
# Finish the initialisation of the environment.
#

cat <<EOF

Make sure you put the following directories in your MODULEPATH to build the benchmarks:
  * $moduleslink_dir
  * $modules_dir

EOF

#
# Test loading the UEABS module
#
module purge
MODULEPATH=$modules_dir:$moduleslink_dir
module load UEABS/$ueabs_version

echo "Check in prepare.sh - not exported outside this script:"
module list
echopath MODULEPATH


################################################################################
################################################################################
#
# Prepare our custom EasyBuild (EasyBuild-UEABS)
# NOT USED ANYMORE
#
################################################################################
################################################################################
#
#module load EasyBuild/$easybuild_version
#
## Just to be sure we also unset EBROOTEASYBUILDMINUEABS in case it would be in the environment
## after uncorrectly unloading the module.
#cd $EBsys_dir/EasyBuild
#unset EBROOTEASYBUILDMINUEABS
#eb --configfiles=$EBetc_dir/$site-$system.cfg-gen EasyBuild-UEABS-$easybuild_version.eb -f
#
##
## Clean-up (only makes sense for rubbish left behind by building EasyBuild-UEABS).
##
#find /tmp -maxdepth 1 -user $USER -exec /bin/rm -rf '{}' \;;
#[ -d /dev/shm/$USER ] && /bin/rm -rf /dev/shm/$USER
#
##
## From now on, when loading EasyBuild-UEABS, there is an alias eb-ueabs that enables using
## EasyBuild without having the pass the --configfiles argument. However, as EASYBUILD_CONFIGFILES
## is also set, we can call eb as well.
##
#
#module load EasyBuild-UEABS/$easybuild_version

#
# It may still be needed to set up the Repo-link directory if this would be a new system for
# which this has not yet been done.
#




