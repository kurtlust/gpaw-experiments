# Example system installations

The `Systems` directory structure contains the basic scripts that we used to
install the UEABS benchmarks on a number of systems.

These systems are:

  * [CalcUA-leibniz-broadwell](CalcUA-leibniz-broadwell) is a Tier-2 system installed at the University of 
    Antwerpen in 2017. The system has 152 nodes with two 14-core Intel Broadwell 
    class Xeon CPUs. During the testing period, the system ran CentOS 7. The 
    software on the system is managed throughEasyBuild though with some local 
    adjustments to the standard EasyBuild setup.
  * [CalcUA-vaughan-rome](CalcUA-vaughan-rome) is a more recent Tier-2 system installed at the University
    of Antwerpen in 2020. This system is equiped with AMD Rome class processores 
    with 32 cores per socket and runs CentOS 8. The setup is otherwise similar 
    to CalcUA-leibniz-broadwell

This directory is complementary to two other root subdirectories in this
repository which both have a systems-specific subdirectory of their own:
  * UEABS: Contains a superset of the files that make it to the official UEABS
    repository. This includes only build scripts in a format suitable for that
    repository. See 
    [the README file in the UEABS directory](../UEABS/README.md) for 
    further information.
  * EasyBuild: This contains all EasyBuild-related material that we used in 
    testing the UEABS benchmark suite. Though not officially supported by PRACE,
    it does contain a lot of EasyBuild recipes for building in particular many
    variants of GPAW. See [the README file in the EasyBuild directory](../EasyBuild/README.md)
    for further information on using EasyBuild.

## Typical setup of the directory structure

For our experiments with the UEABS benchmark suite, we've chosen a directory
structure that makes it possible to mix modules already present on the system,
specific EasyBuild-built modules and manual builds form build scripts from the
UEABS repository or our own UEABS-repository-style scripts. Hence the directory
structure that we will create is very reminiscent of the directory structure
that EasyBuild would use (though with a simplified naming scheme that we use
at UAntwerp).

Moreover, the setup is such that we can create a module structure that is 
(almost) completely independent of the system module structure, though it is not
clear yet if this will work on all systems.

For each system, we have a single subdirectory where we will install all
module files and software. It is set up in such a way that we can even 
accomodate sites with a shared storage for multiple clusters, and clusters
with multiple node types (when it comes to CPU architecture). Hence we use
a name of the form <site>-<cluster>-<arch>, see the example systems above.

That subdirectory has as at a minimum the following subdirectories:
  * Packages: The actual software installations. Each package is installed in
    a subdirectory <package name>/<version info>, corresponding to the module
    <package name>/<version info>.
  * Modules: The directory for the module file structure corresponding to the 
    packages in the Packages subdirectory.
  * Modules-link: This will be present on most systems that we use. It is 
    used to store symbolic links to or copies from module files already on the
    system, in case we want to isolate as much as possible from the modules
    on the system and create a UEABS-specific environment where all modules that
    are not needed are not accessible.
Beside the subdirectory <site>-<cluster>-<arch>, there is at least one other
subdirectory that is common to all systems:
  * Downloads: Directory where we store downloaded files from the UEABS-style
    build scripts.
    
When we decide to also use EasyBuild on a system, the following additional 
directories are created:
  * Outside of <site>-<cluster>-<arch>:
      * Sources: Directory for sources downloaded by EasyBuild. We let EasyBuild
        organise the structure of this directory, which is why we keep a separate
        Downloads directory 
  * As a subdirectory of <site>-<cluster>-<arch>:
      * Repo: EasyBuild repo directory, used by EasyBuild to store copies of the
        recipe files used to build the packages in the Packages and Modules 
        subdirectory.
      * There is no Repo-link subdirectory in analogy to the Modules-link subdirectory.
        This directory instead exisits in the system-specific files of the EasyBuild
        tree in this directory. The reason is that these files are not always taken
        from the host system or the [EasyBuilders repository](https://github.com/easybuilders/easybuild-easyconfigs/) 
        but tricked to make EasyBuild believe the software was installed through
        EasyBuild. We want to be able to easily version-control these files so
        we store them directly in the repository and do not copy them from the
        system in the scripts that prepare the directory structure nor do we 
        generate them as bash here documents in those scripts.

