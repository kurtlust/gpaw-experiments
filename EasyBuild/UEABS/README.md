# Using EasyBuild to build the GPAW benchmark

## Supported configuration types

 1. It is possible to install GPAW including a Python interpreter and all dependencies by a
    single EasyConfig file (hence using a single module). For this, use any of the configuration
    in the `UEABS/GPAW/BenchmarkConfig` subdirectory. This is the way resulting in the fewest modules,
    but it is slow and if it goes wrong, it is hard to debug the whole proces.

 1. Install a Python interpreter from an EasyConfig that includes all dependencies needed to get the 
    Python interpreter to work for the GPAW benchmark (one of the `-bundle-UEABS-GPAW`
    EasyConfig files in `UEABS/Python`). Next install `libxc` and, if needed, `FFTW` from 
    EasyConfig files in the `UEAB/GPAWdependencies` subdirectory. Finally, install GPAW
    (which includes other needed Python modules such as NumPy, SciPy and ase) 
    from one of the EasyConfig files in `UEABS/GPAW/SeparatePython`.

 1. The third option is to install the dependencies for Python as a separate module.

     1. First install the `python-deps` package from the 
        `UEABS/PythonDependencies` subdirectory.

     1. Next install a suitable Python interpreter using one of the `0UEABS-GPAW`-files
        in `UEABS/PythonDependencies`.

     1. Now one can proceed as in the previous case, installing `libxc` and `FFTW` as needed
        and then install GPAW. Some editing may be needed to get the dependencies in the 
        provided EasyConfig files right.

 1. Finally, one can even install the dependencies of Python as separate packages.
    We do provide some EasyConfig files that we tested. However, to not create four
    different sets of EasyConfig files for installing GPAW, some editing will be needed
    in the Python EasyConfig files to get the dependency lists right.
 

## Overview of EasyConfigs for the GPAW benchmark

### buildtools-subdirectory

This subdirectory contains a number of build tools that usually can be compiled
with the system compiler and update OS-installed tools that may be too old 
to compile GPAW and/or its dependencies

  * Perl: Included in case you want to install your own OpenSSL. Some Linux systems
    may either have a Perl-installation that is too old or may lack certain Perl
    packages that are needed. This sometimes causes failure in the test step of
    OpenSSL. **The default Perl EasyBlock in EasyBuild 4.2.x does not support
    building Perl with the system compiler which is why we included a slightly
    modified one in the Custom-EasyBlocks tree. It was adapted from the EasyBuild
    4.2.0 one.**


### Dependencies for Python that can be GCC-compiled without performance consequences

These EasyConfig files are provided as separate EasyConfigs for build configuration
`4_AllSeparate`.

  * ncurses: Needed for libreadline.
  * libreadline: Needed simply to keep EasyBuild happy as it tests for the
    readline Python package which is only installed if libreadline is found.
  * libffi: Needed to keep EasyBuild happy as it tests against the _ctypes Python
    package which requires libffi.
  * zlib: Needed for OpenSSL.
  * OpenSSL: Included to keep EasyBuild happy.


### Dependencies that we expect may have more influence on the performance

See the subdirectory `GPAWDependencies`.

  * FFTW: This is only an optional dependency. GPAW can also use FFT routines in
    NumPy, or the FFTW interface of the MKL libraries, and our testing showed that
    this works well if a highly optimized FFT library is used to build NumPy.
  * libxc: Mandatory dependency for GPAW.
      * libxc needs autoconf and libtool to install.
  * ELPA: **Not needed as the benchmarks currently use a different eigensolver.**


### Python subdirectory

This subdirectory contains EasyConfig files for building Python with various compilers. 
Note that depending on the libraries found on the system, they may not install the 
complete Python Standard Library. However, they do include the dependencies, either 
in the EasyConfig itself or through loaded dependencies, that are needed to build a 
Python interpreter suitable for running the GPAW benchmarks (without the GPAW optional 
components that are not needed for the benchmark).

There are two sets of EasyConfig files:
  * The `-bundle-UEABS-GPAW` EasyConfig files: Those files contain all depencencies 
    in the same EasyConfig and only require suitable build tools and a compiler toolchain 
    to be build. They do contain all needed dependencies that would otherwise be installed
    from the `PythonDependencies` subdirectory.
  * The `-UEABS-GPAW` EasyConfig files: These files load the dependencies through a 
    module file created by the EasyConfig files in the `PythonDependencies` subdirectory.

All Python EasyBuild recipes do install a few packages that are needed to install the 
further components needed for GPAW.


### GPAW subdirectory

This subdirectory contains various configurations of GPAW.

For an intel-compiled GPAW, we provide three configurations:
  * Using MKL for linear algebra and FFTW with the MKL-FFTW interface for the FFT operations.
  * Using MKL for linear algebra and FFTW for the FFT operations
  * Using the NumPy FFT routines which actually isn't detrimental to the performance
    if NumPy was compiled with a heavily optimized FFT library.

There are two subdirectories:
  * BenchmarkConfig: This directory contains Bundle-style EasyConfigs that compile
    Python with all important dependencies and then install the minimal set of Python
    packages needed to run the benchmarks. It only needs the buildtools package and 
    a suitable EasyBuild toolchain setup.
  * SeparatePython: EasyConfigs to install on top of an already existing EasyBuild
    Python module. These bundles still compile the additional libraries that are 
    deemed performance-critical (libxc and, if needed, FFTW).

The `Development`-subdirectory contains versions with the optional Python dependencies
of GPAW also included, and versions that only build GPAW and rely on separate modules 
for all dependencies.


## Instructions for specific EasyConfig files

### Subdirectory dependencies

#### OpenSSL

  * OpenSSL uses a standard configure - make - make install build process.
    Hence the inclusion of `make` in the OS dependencies.
  * OpenSSL needs a sufficiently recent version of Perl to run the tests.
     * Running the tests can be disabled by outcommenting the run_tests line.
     * On CentOS 7 it appears that the `perl` and `perl-core` packages are needed
       to successfully use the system Perl. Hence both packages are specified in the
       OS dependencies. 
     * As an alternative, the Perl module included in the buildtools-subdirectory
       can be used. We did not check for OS dependencies so building may fail depending
       on the system.

#### Freetype (only needed when using the matplotlib Python package)

  * Since version 2.9, freetype includes intrinsics in one of the source file that 
    are not recognized by the Intel compiler (tested with 2018-2020). Hence a patch 
    is used to remove those calls and replace by a generic code path


### Python packages

#### NumPy

NumPy in EasyBuild is installed through an EasyBlock derived from FortranPythonPackage

  * Configure phase: 
      * Builds a `site.cfg` file
      * Does the regular discovery for a Python package
      * Runs `python setup configure`
  * Build phase: Runs `python setup build`, adding `buildopts` and appropriate 
    `--compiler` and `--fcompiler` options. Note that this is done through the 
    FortranPythonPackage generic EasyBlock.
  * Test phase: The NumPy EasyBlock does perform a number of texts. Search in the log 
    for `INFO Time for .* matrix dot product`.
  * Install phase: Uses the regular install procedure for Python packages so will honour 
    all regular Python package installation options (such as use_pip et.)

Notes:
  * NumPy can use SuiteSparse, but it is not clear if the GPAW benchmarks benefit
    from it.

#### SciPy

TODO

### ase

TODO

### GPAW

TODO


## Installing with EasyBuild

 1. If there is no EasyBuild installation on the cluster, start by installing EasyBuild 
    using the instructions on 
    [the EasyBuild web site: bootstrap procedure]()
    
 1. To make work with EasyBuild easier, we do usually install a wrapper module that 
    defines an alias to call EasyBuild and adds the argument to select the right 
    system-specific EasyBuild configuration file. Examples can be found in the 
    `Systems` subdirectory for the systems that we tested on, in the 
    `EasyBuild` subdirectory for each system.
    
 1. One must make sure that the toolchains are visible to EasyBuild when building
    UEABS coponents. This requires two components:
    
     1. The necessary EasyConfig files. 
        
        * On an EasyBuild system, there is usually a
          `repo` subdirectory that stores them, so this should be added to the search path.

        * Usually each package installed with EasyBuild also has an `easybuild` subdirectory
          that contains the EasyConfig file. One could copy the files from there and put them
          in a place where EasyBuild can find them, or put a symbolic link in a place where 
          EasyBuild can find them. A good suggestion for that place is the `repo`-subdirectory
          used for the UEABS packages.

        * As a method of last resort, these files could be re-created, e.g., from the ones in the
          EasyBuild repository. The important part is to get all dependencies right. The precise
          install instructions do not matter as we do not want to re-install the software.
           
     1. The necessary module files.

         * One can simply make sure that the system-installed modules can be found by EasyBuild
          when calling it.

         * Or one can put symbolic links to the system-installed modules in a directory where 
           EasyBuild will look for modules. A good candidate is the directory tree where EasyBuild
           will install module files for the UEABS components.

         * As a method of last resort, one can recreate the module files.
           
 1. Once the toolchains are set up (or even in parallel, as we only use the sytem 
    compilers and libraries for it) it is advised to build the `buildtools` module from
    the `UEABS/buildtools` subdirectory. This is not always needed, but it does contain a number
    of tools that may be too old (or may be missing) on some systems to successfully build the
    other components.
    
 1. The path to be taken from there depends on the choice of the 
    benchmarker:
    
     1. It is possible to install GPAW including a Python interpreter and all dependencies by a
        single EasyConfig file (hence using a single module). For this, use any of the configuration
        in the `UEABS/GPAW/BenchmarkConfig` subdirectory. This is the way resulting in the fewest modules,
        but it is slow and if it goes wrong, it is hard to debug the whole proces.

     1. Install a Python interpreter from an EasyConfig that includes all dependencies needed to get the 
        Python interpreter to work for the GPAW benchmark (one of the `-bundle-UEABS-GPAW`
        EasyConfig files in `UEABS/Python`). Next install `libxc` and, if needed, `FFTW` from 
        EasyConfig files in the `UEAB/GPAWdependencies` subdirectory. Finally, install GPAW
        (which includes other needed Python modules such as NumPy, SciPy and ase) 
        from one of the EasyConfig files in `UEABS/GPAW/SeparatePython`.

     1. The third option is to install the dependencies for Python as a separate module.

         1. First install the `python-deps` package from the 
           `UEABS/PythonDependencies` subdirectory.

         1. Next install a suitable Python interpreter using one of the `0UEABS-GPAW`-files
            in `UEABS/PythonDependencies`.

         1. Now one can proceed as in the previous case, installing `libxc` and `FFTW` as needed
            and then install GPAW. Some editing may be needed to get the dependencies in the 
            provided EasyConfig files right.

     1. Finally, one can even install the dependencies of Python as separate packages.
        We do provide some EasyConfig files that we tested. However, to not create four
        different sets of EasyConfig files for installing GPAW, some editing will be needed
        in the Python EasyConfig files to get the dependency lists right.



