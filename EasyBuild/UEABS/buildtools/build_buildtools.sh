#!/usr/bin/bash
#
# Assumptions about environment variables exported to this script:
# - MODULEPATH is set correctly for access to the UEABS modules
#   For PBS, we need JOBMODULEPATH instead.
# - ueabs_version is set so that the script can determine which version
#   of the UEABS module should be used. This will in turn determine which
#   versions of EasyBuild will be used and of buildtools will be built.
#

# Change to the working directory if we run in a PBS/Torque job context.
# Also set 
if [ -n "$PBS_O_WORKDIR" ] ; 
then 
  cd $PBS_O_WORKDIR
  export MODULEPATH=$JOBMODULEPATH
fi

# Load the UEABS settings module
if [ -n "$ueabs_version" ] ; then
  echo "Loading UEABS/$ueabs_version..."
  module load UEABS/$ueabs_version
else
  echo "No value given for the environment variable ueabs_version, loading the default UEABS module"
  module load UEABS
fi

# Now load EasyBuild.
module load EasyBuild/$UEABS_EBVERSION

# Build buildtools. 
eb buildtools-$UEABS_VERSION.eb -f

#
# Clean-up
#
find /tmp -maxdepth 1 -user $USER -exec /bin/rm -rf '{}' \;;
[ -d /dev/shm/$USER ] && /bin/rm -rf /dev/shm/$USER
