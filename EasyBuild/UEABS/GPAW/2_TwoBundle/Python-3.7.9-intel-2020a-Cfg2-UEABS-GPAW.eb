easyblock = 'Bundle'

local_UEABS_version =        '2.2'
local_intel_version =        '2020a'
local_gcc_version =          '9.3.0'

local_ncurses_version =      '6.2'      # https://ftp.gnu.org/pub/gnu/ncurses/
local_libreadline_version =  '8.0'      # https://ftp.gnu.org/pub/gnu/readline/
local_libffi_version =       '3.3'      # ftp://sourceware.org/pub/libffi/
local_zlib_version =         '1.2.11'   # https://zlib.net/
local_OpenSSL_version =      '1.1.1k'   # https://www.openssl.org/source/
local_SQLite_version =       '3.33.0'    # https://ftp.gnu.org/pub/gnu/readline/

local_Python_3_7_version =   '3.7.9'    # https://www.python.org/downloads/

local_pip_version =          '20.2.3'   # https://pypi.org/project/pip/
local_setuptools_version =   '50.3.0'   # https://pypi.org/project/setuptools/
local_wheel_version =        '0.35.1'   # https://pypi.org/project/wheel/

name =          'Python'
version =       local_Python_3_7_version
versionsuffix = '-Cfg2-UEABS-GPAW'

local_pymaj   = ''.join(version.split('.')[:1])
local_pycpver = ''.join(version.split('.')[:2])

homepage = 'http://python.org/'

whatis = [
    'Description: Python %(version)s interpreter with a range of optional packages'
]

description = """
This is the Python %(version_major_minor)s interpreter with only those packages
that are essential to install your own packages.
"""

usage = """
There is ample documentation about Python available on the web; please check
there or any good book about Python for usage information.

To get a list of installed Python packages, load this module file and then
execute
pydoc modules
"""

toolchain =     {'name': 'intel', 'version': local_intel_version}
toolchainopts = {'pic': True}

dependencies = []

builddependencies = [
    ('buildtools', local_UEABS_version, '', True),
]

local_SQLite_version_str = '%%(version_major)s%s00' % ''.join('%02d' % int(x) for x in local_SQLite_version.split('.')[1:])

default_easyblock = 'ConfigureMake'

components = [
#
# Terminal libraries
#
    ('ncurses', local_ncurses_version, {
        'sources':     [ {
                          'filename':    SOURCE_TAR_GZ,
                          'source_urls': [GNU_SOURCE]
                       } ],
        'patches':     ['ncurses-%(version)s_gcc-5.patch'],
        'start_dir':   '%(namelower)s-%(version)s',
        'configopts':  '--with-shared --enable-overwrite', # First build: default build.
    }),
    ('ncurses', local_ncurses_version, {
        'sources':       [ {
                            'filename':    SOURCE_TAR_GZ,
                            'source_urls': [GNU_SOURCE]
                         } ],
        'patches':       ['ncurses-%(version)s_gcc-5.patch'],
        'start_dir':     '%(namelower)s-%(version)s',
        'preconfigopts': 'make distclean && ',
        'configopts':    '--with-shared --enable-overwrite --enable-ext-colors --enable-widec --includedir=%(installdir)s/include/ncursesw/' # Second build: the UTF-8 enabled version (ncursesw)
    }),
    ('libreadline', local_libreadline_version, {
        'sources':       [ {
                            'filename':    'readline-%(version)s.tar.gz',
                            'source_urls': ['http://ftp.gnu.org/gnu/readline']
                         } ],
        'start_dir':     'readline-%(version)s',
        'preconfigopts': "LD_LIBRARY_PATH=%(installdir)s/lib:$LD_LIBRARY_PATH LDFLAGS='-L%(installdir)s/lib -lncurses'",
    }),
    ('libffi', local_libffi_version, {
        'sources':     [ {
                          'filename':    SOURCELOWER_TAR_GZ,
                          'source_urls': ['ftp://sourceware.org/pub/libffi/', 'http://www.mirrorservice.org/sites/sourceware.org/pub/libffi/']
                       } ],
        'start_dir':   '%(namelower)s-%(version)s',
        'configopts':  '--disable-multi-os-directory', # Avoid the use of lib64, put everything in lib.
    }),
    ('zlib', local_zlib_version, {
        'sources':     [ {
                          # https://www.zlib.net/zlib-1.2.11.tar.gz
                          'filename':    SOURCELOWER_TAR_GZ,
                          'source_urls': ['https://www.zlib.net/']
                       } ],
        'start_dir':   '%(namelower)s-%(version)s',
    }),
    ('OpenSSL', local_OpenSSL_version, {
        'easyblock':   'EB_OpenSSL',
        'sources':     [ {
                          'filename':    SOURCELOWER_TAR_GZ,
                          'source_urls': ['https://www.openssl.org/source/']
                       } ],
        'start_dir':   '%(namelower)s-%(version)s',
    }),
    ('SQLite', local_SQLite_version, { # Needs libreadline
        'sources':     [ {
                          # https://www.sqlite.org/2020/sqlite-src-3330000.zip
                          'filename':    'sqlite-autoconf-%s.tar.gz' % local_SQLite_version_str,
                          'source_urls': ['http://www.sqlite.org/2020/']
                       } ],
        'start_dir':   '%(namelower)s-autoconf-' + local_SQLite_version_str,
        'configopts':  'CFLAGS="$CFLAGS -DSQLITE_DISABLE_INTRINSIC"',
    }),
    ('Python', local_Python_3_7_version, {
        'easyblock':        'EB_Python',
        'sources':          [ {
                               'filename':    SOURCE_TGZ,
                               'source_urls': ['http://www.python.org/ftp/%(namelower)s/%(version)s/']
                            } ],
        'start_dir':        '%(name)s-%(version)s',
        'preconfigopts':    'CFLAGS="$CFLAGS -fwrapv" ',
        'optimized':        False,
        'use_lto':          None,
        'ulimit_unlimited': False,
    })
]


################################################################################
#
# Installation process of the extensions
#

exts_defaultclass = 'PythonPackage'
exts_filter = ("python -c 'import %(ext_name)s'", '')
exts_default_options = {
    'download_dep_fail': True,
    'sanity_pip_check':  False,
    'source_urls':       [PYPI_SOURCE],
    'use_pip':           True,
}

# It is not possible to install Python extensions with the PythonPackage EasyBlock and pip
# when Python is installed as part of the bundle. The problem is that EasyBuild itself
# already calls pip outside the build and install steps, and there is no way to set
# the PATH OK to include the pip executable.
exts_list = [
#
# Reploace the included pip and setuptools with newer versions.
#
    ('pip', local_pip_version, {
        'use_pip':     False,
        'checksums':   ['30c70b6179711a7c4cf76da89e8a0f5282279dfb0278bec7b94134be92543b6d'],
    }),
    ('setuptools', local_setuptools_version, {
        'source_tmpl': '%(name)s-%(version)s.zip',
        'checksums':   ['39060a59d91cf5cf403fa3bacbb52df4205a8c3585e0b9ba4b30e0e19d4c4b18'],
    }),
    ('wheel', local_wheel_version, {
        'checksums':   ['99a22d87add3f634ff917310a3d87e499f19e663413a52eb9232c447aa646c9f'],
    }),
]

modextrapaths = {
     'PYTHONPATH': ['lib/python%s/site-packages' % local_pycpver],
     'PATH':       ['bin'],
}

moduleclass = 'lang'
