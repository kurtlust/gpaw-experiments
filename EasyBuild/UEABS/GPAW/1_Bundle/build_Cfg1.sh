#!/usr/bin/bash
#
# Configuration 1: All-in-one bundle
#
# Assumptions about environment variables exported to this script:
# - MODULEPATH is set correctly for access to the UEABS modules
#   For PBS, we need JOBMODULEPATH instead.
# - ueabs_version is set so that the script can determine which version
#   of the UEABS module should be used. This will in turn determine which
#   versions of EasyBuild will be used and of buildtools will be built.
#

# Change to the working directory if we run in a PBS/Torque job context.
# Also set
if [ -n "$PBS_O_WORKDIR" ] ;
then
  cd $PBS_O_WORKDIR
  export MODULEPATH=$JOBMODULEPATH
fi

# Load the UEABS settings module
if [ -n "$ueabs_version" ] ; then
  echo "Loading UEABS/$ueabs_version..."
  module load UEABS/$ueabs_version
else
  echo "No value given for the environment variable ueabs_version, loading the default UEABS module"
  module load UEABS
fi

# Now load EasyBuild.
module load EasyBuild/$UEABS_EBVERSION

function build() {

    echo -e "\n################################################################################" \
            "\nBuilding $1\n\n"
    eb $1 -f

}

set +x

#
# GPAW
#

# Default parallel configuration

#build GPAW-1.5.2-intel-2020a-Python-3.7.9-Cfg1-UEABS-GPAW.eb

#build GPAW-19.8.1-intel-2020a-Python-3.7.9-FFTW-Cfg1-UEABS-GPAW.eb
#build GPAW-19.8.1-intel-2020a-Python-3.7.9-MKLFFTW-Cfg1-UEABS-GPAW.eb
#build GPAW-19.8.1-intel-2020a-Python-3.7.9-NumPyFFT-Cfg1-UEABS-GPAW.eb

build GPAW-20.1.0-intel-2020a-Python-3.8.7-FFTW-Cfg1-UEABS-GPAW.eb
build GPAW-20.1.0-intel-2020a-Python-3.8.7-MKLFFTW-Cfg1-UEABS-GPAW.eb
build GPAW-20.1.0-intel-2020a-Python-3.8.7-NumPyFFT-Cfg1-UEABS-GPAW.eb

build GPAW-20.10.0-intel-2020a-Python-3.9.4-FFTW-Cfg1-UEABS-GPAW.eb
build GPAW-20.10.0-intel-2020a-Python-3.9.4-MKLFFTW-Cfg1-UEABS-GPAW.eb
build GPAW-20.10.0-intel-2020a-Python-3.9.4-NumPyFFT-Cfg1-UEABS-GPAW.eb

build GPAW-21.1.0-intel-2020a-Python-3.9.4-FFTW-Cfg1-UEABS-GPAW.eb
build GPAW-21.1.0-intel-2020a-Python-3.9.4-MKLFFTW-Cfg1-UEABS-GPAW.eb
build GPAW-21.1.0-intel-2020a-Python-3.9.4-NumPyFFT-Cfg1-UEABS-GPAW.eb

#
# Clean-up
#
find /tmp -maxdepth 1 -user $USER -exec /bin/rm -rf '{}' \;;
[ -d /dev/shm/$USER ] && /bin/rm -rf /dev/shm/$USER
