easyblock = 'Bundle'

local_UEABS_version =        '2.2'
local_gcc_version =          '9.3.0'
local_binutils_version =     '2.34'

local_ncurses_version =      '6.2'      # https://ftp.gnu.org/pub/gnu/ncurses/
local_libreadline_version =  '8.0'      # https://ftp.gnu.org/pub/gnu/readline/
local_libffi_version =       '3.3'      # ftp://sourceware.org/pub/libffi/
local_zlib_version =         '1.2.11'   # https://zlib.net/
local_OpenSSL_version =      '1.1.1g'   # https://www.openssl.org/source/
local_SQLite_version =       '3.33.0'    # https://ftp.gnu.org/pub/gnu/readline/

local_Python_3_9_version =   '3.9.1'    # https://www.python.org/downloads/

local_pip_version =          '20.2.3'   # https://pypi.org/project/pip/
local_setuptools_version =   '50.3.0'   # https://pypi.org/project/setuptools/
local_wheel_version =        '0.35.1'   # https://pypi.org/project/wheel/

name =          'Python'
version =       local_Python_3_9_version
versionsuffix = '-Cfg3-UEABS-GPAW'

local_pymaj   = ''.join(version.split('.')[:1])
local_pycpver = ''.join(version.split('.')[:2])

homepage = 'http://python.org/'

whatis = [
    'Description: Python %(version)s interpreter with a range of optional packages'
]

description = """
This is the Python %(version_major_minor)s interpreter with only those packages
that are essential to install your own packages.
"""

usage = """
There is ample documentation about Python available on the web; please check
there or any good book about Python for usage information.

To get a list of installed Python packages, load this module file and then
execute
pydoc modules
"""

toolchain =     {'name': 'GCCcore', 'version': local_gcc_version}
toolchainopts = {'pic': True}

dependencies = []

builddependencies = [
    ('binutils',   local_binutils_version          ),
    ('buildtools', local_UEABS_version,    '', True),
]

local_SQLite_version_str = '%%(version_major)s%s00' % ''.join('%02d' % int(x) for x in local_SQLite_version.split('.')[1:])

default_easyblock = 'ConfigureMake'

components = [
#
# Terminal libraries
#
    ('ncurses', local_ncurses_version, {
        'sources':     [ {
                          'filename':    SOURCE_TAR_GZ,
                          'source_urls': [GNU_SOURCE]
                       } ],
        'patches':     ['ncurses-%(version)s_gcc-5.patch'],
        'start_dir':   '%(namelower)s-%(version)s',
        'configopts':  '--with-shared --enable-overwrite', # First build: default build.
    }),
    ('ncurses', local_ncurses_version, {
        'sources':       [ {
                            'filename':    SOURCE_TAR_GZ,
                            'source_urls': [GNU_SOURCE]
                         } ],
        'patches':       ['ncurses-%(version)s_gcc-5.patch'],
        'start_dir':     '%(namelower)s-%(version)s',
        'preconfigopts': 'make distclean && ',
        'configopts':    '--with-shared --enable-overwrite --enable-ext-colors --enable-widec --includedir=%(installdir)s/include/ncursesw/' # Second build: the UTF-8 enabled version (ncursesw)
    }),
    ('libreadline', local_libreadline_version, {
        'sources':       [ {
                            'filename':    'readline-%(version)s.tar.gz',
                            'source_urls': ['http://ftp.gnu.org/gnu/readline']
                         } ],
        'start_dir':     'readline-%(version)s',
        'preconfigopts': "LD_LIBRARY_PATH=%(installdir)s/lib:$LD_LIBRARY_PATH LDFLAGS='-L%(installdir)s/lib -lncurses'",
    }),
    ('libffi', local_libffi_version, {
        'sources':     [ {
                          'filename':    SOURCELOWER_TAR_GZ,
                          'source_urls': ['ftp://sourceware.org/pub/libffi/', 'http://www.mirrorservice.org/sites/sourceware.org/pub/libffi/']
                       } ],
        'start_dir':   '%(namelower)s-%(version)s',
        'configopts':  '--disable-multi-os-directory', # Avoid the use of lib64, put everything in lib.
    }),
    ('zlib', local_zlib_version, {
        'sources':     [ {
                          # https://www.zlib.net/zlib-1.2.11.tar.gz
                          'filename':    SOURCELOWER_TAR_GZ,
                          'source_urls': ['https://www.zlib.net/']
                       } ],
        'start_dir':   '%(namelower)s-%(version)s',
    }),
    ('OpenSSL', local_OpenSSL_version, {
        'easyblock':   'EB_OpenSSL',
        'sources':     [ {
                          'filename':    SOURCELOWER_TAR_GZ,
                          'source_urls': ['https://www.openssl.org/source/']
                       } ],
        'start_dir':   '%(namelower)s-%(version)s',
    }),
    ('SQLite', local_SQLite_version, { # Needs libreadline
        'sources':     [ {
                          # https://www.sqlite.org/2020/sqlite-src-3330000.zip
                          'filename':    'sqlite-autoconf-%s.tar.gz' % local_SQLite_version_str,
                          'source_urls': ['http://www.sqlite.org/2020/']
                       } ],
        'start_dir':   '%(namelower)s-autoconf-' + local_SQLite_version_str,
    }),
    ('Python', local_Python_3_9_version, {
        'easyblock':        'EB_Python',
        'sources':          [ {
                               'filename':    SOURCE_TGZ,
                               'source_urls': ['http://www.python.org/ftp/%(namelower)s/%(version)s/']
                            } ],
        'start_dir':        '%(name)s-%(version)s',
        'preconfigopts':    'CFLAGS="$CFLAGS -fwrapv" ',
        'installopts':      ' && ln -s %(installdir)s/bin/pip3 %(installdir)s/bin/pip',
        'optimized':        False,
        'use_lto':          None,
        'ulimit_unlimited': False,
    })
]


################################################################################
#
# Installation process of the extensions
#

exts_defaultclass = 'PythonPackage'
exts_filter = ("python -c 'import %(ext_name)s'", '')
exts_default_options = {
    'download_dep_fail': True,
    'sanity_pip_check':  False,
    'source_urls':       [PYPI_SOURCE],
    'use_pip':           True,
}

# It is not possible to install Python extensions with the PythonPackage EasyBlock and pip
# when Python is installed as part of the bundle. The problem is that EasyBuild itself
# already calls pip outside the build and install steps, and there is no way to set
# the PATH OK to include the pip executable.
exts_list = [
#
# Reploacing the included pip and setuptools with newer versions doesn't seem to work as
# it should in Python 3.9.1.
#
#    ('pip', local_pip_version, {
#        'use_pip': False,
#        'checksums': ['99bbde183ec5ec037318e774b0d8ae0a64352fe53b2c7fd630be1d07e94f41e5'],
#    }),
#    ('setuptools', local_setuptools_version, {
#        'checksums': ['1b18ef17d74ba97ac9c0e4b4265f123f07a8ae85d9cd093949fa056d3eeeead5'],
#    }),
    ('wheel', local_wheel_version, {
        'checksums':   ['e11eefd162658ea59a60a0f6c7d493a7190ea4b9a85e335b33489d9f17e0245e'],
    }),
]

modextrapaths = {
     'PYTHONPATH': ['lib/python%s/site-packages' % local_pycpver],
     'PATH':       ['bin'],
}

moduleclass = 'lang'
