easyblock = 'Bundle'

local_UEABS_version =        '2.2'
local_intel_version =        '2020a'

local_Python_3_8_version =   '3.8.7'     # https://www.python.org/downloads/

local_six_version =          '1.15.0'
local_attrs_version =        '20.3.0'
local_iniconfig_version =    '1.1.1'
local_pyparsing_version =    '2.4.7'
local_packaging_version =    '20.4'
local_pluggy_version =       '0.13.1'
local_py_version =           '1.9.0'
local_toml_version =         '0.10.2'
local_pytest_version =       '6.1.2'

local_Cython_version =       '0.29.21'   # https://pypi.org/project/Cython/

local_libxc_version =        '4.3.4'     # https://www.tddft.org/programs/libxc/download/
local_FFTW_version =         '3.3.8'     # http://www.fftw.org/

local_GPAWsetup_version =    '0.9.20000' # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html

local_numpy_20_1_version =   '1.18.5'    # https://pypi.org/project/numpy/
local_scipy_20_1_version =   '1.5.4'     # https://pypi.org/project/scipy/
local_ASE_20_1_version =     '3.19.3'    # Official corresponding ase release is 3.19.0 or later
local_GPAW_20_1_version =    '20.1.0'

local_UEABS_suffix = '-Cfg3-UEABS-GPAW'

name =          'GPAW'
version =       local_GPAW_20_1_version
versionsuffix = '-Python_icc-%(pyver)s-MKLFFTW' + local_UEABS_suffix

homepage = 'http://wiki.fysik.dtu.dk/gpaw'

whatis = [
    "Description: GPAW: DFT and beyond within the projector-augmented wave method.",
    'This module also includes the compatible ASE version ' + local_ASE_20_1_version,
    'Python packages: gpaw-' + local_GPAW_20_1_version + ', ase-' + local_ASE_20_1_version + 
    ', numpy-' + local_numpy_20_1_version + ', scipy-' + local_scipy_20_1_version + ', cython-' + local_Cython_version
]

description = """
GPAW is a density-functional theory (DFT) Python code based on the
projector-augmented wave (PAW) method and the atomic simulation environment
(ASE). It uses plane-waves, atom-centered basis-functions or real-space
uniform grids combined with multigrid methods.

This module also includes a matching version of ASE (version %(ASE_version)s),
the Atomic Simulation Environment, which is used internally by GPAW.

The module also contains the Atomic PAW setup files (version %(GPAWsetup_version)s)
in the directory $GPAW_SETUP_FILES.

The GPAW manual advises the OMP_NUM_THREADS should be set to 1 so we do this
in the module.

The version in this module is compiled using the Intel MKL libraries for FFT.
The optional libvdwxc support is NOT included in this module.
The optional ELPA support is NOT included in this module.
""" % {
    'ASE_version':       local_ASE_20_1_version,  
    'GPAWsetup_version': local_GPAWsetup_version,  
}

docurls = [
    'GPAW web-based documentation: https://wiki.fysik.dtu.dk/gpaw/',
    'ASE web-based documentation:https://wiki.fysik.dtu.dk/ase/',
]

toolchain =     {'name': 'intel', 'version': local_intel_version}
toolchainopts = {'pic': True}

dependencies = [
    ('Python',   local_Python_3_8_version, local_UEABS_suffix),
    ('libxc',    local_libxc_version,      local_UEABS_suffix),
]

#
# EasyBuild on our machine (EasyBuild 4.2.2, CentOS 7.7 Python 2.7.5
# Did not accept multiple lines of the form [xxx] and produced warnings of
# EOF at scatnning triple-quoted string literal. Hence the construction
# where we use %(FB)s which we dan substitute with [.
#
local_gpaw_customize = """
print( 'GPAW EasyBuild INFO: Starting execution of the customization script' )
print( 'GPAW EasyBuild INFO: Variables at the start of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =               ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =            ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =            ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =               ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =           ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =            ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                    ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =               ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                    ', elpa )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )

# LibXC via EasyBuild
include_dirs.append(os.path.join(os.getenv('EBROOTLIBXC'), 'include'))
#libraries.append('xc')

# libvdwxc
libvdwxc = False

# ELPA
elpa = False
#include_dirs.append(os.path.join(os.getenv('EBROOTELPA'), 'include/elpa'))
#libraries.append('elpa')

# Use Intel MKL FFTW
fftw = True
libraries += ['fftw3xc_intel_pic'] # MKL BLAS libs are added further down and these contain the other MKL libraries also needed for the FFT.

# ScaLAPACK
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# MKL BLAS
libraries += ['mkl_sequential','mkl_core', 'mkl_rt', ]

# Add other EasyBuild library directoryes.
library_dirs = os.getenv('LIBRARY_PATH').split(':')

# Set the compilers
compiler =    'icc'
mpicompiler = 'mpiicc'
mpilinker =   'mpiicc'

print( 'GPAW EasyBuild INFO: Variables at the end of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =               ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =            ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =            ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =               ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =           ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =            ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                    ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =               ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                    ', elpa )
print( 'GPAW EasyBuild INFO: Ending execution of the customization script' )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )
"""

default_easyblock = 'Tarball'

components = [
    ( 'GPAW-setups', local_GPAWsetup_version, { 
        # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html
        'sources':   [{
                        'filename':    SOURCELOWER_TAR_GZ,
                        'source_urls': ['https://wiki.fysik.dtu.dk/gpaw-files/'], 
                        'extract_cmd': 'mkdir -p share/gpaw-setups ; cd share/gpaw-setups ; tar -xf %s --strip-components=1',
                     }]  
    }),
]

# This is a bundle of Python packages
exts_defaultclass = 'PythonPackage'
exts_filter = ("python -c 'import %(ext_name)s'", '')
exts_default_options = {
    'download_dep_fail': False, # To avoid EasyBuild complaining about the optional dependencies of ASE
    'sanity_pip_check':  False, # Otherwise requires the optional dependencies of GPAW.
    'source_urls':       [PYPI_SOURCE],
    'use_pip':           True,
    'use_pip_for_deps':  False,
}

exts_list = [
    #
    # Python packages only needed if you want to run NumPy or SciPy tests.
    #
    ('six', local_six_version, {
        'checksums': ['30639c035cdb23534cd4aa2dd52c3bf48f06e5f4a941509c8bafd8ce11080259'],
    }),
    ('attrs', local_attrs_version, {
        'checksums': ['832aa3cde19744e49938b91fea06d69ecb9e649c93ba974535d08ad92164f700'],
        'modulename': 'attr',
    }),
    ('iniconfig', local_iniconfig_version, {
        'checksums': ['bc3af051d7d14b2ee5ef9969666def0cd1a000e121eaea580d4a313df4b37f32'],
    }),
    ('pyparsing', local_pyparsing_version, {
        'checksums': ['c203ec8783bf771a155b207279b9bccb8dea02d8f0c9e5f8ead507bc3246ecc1'],
    }),
    ('packaging', local_packaging_version, {
        'checksums': ['4357f74f47b9c12db93624a82154e9b120fa8293699949152b22065d556079f8'],
    }),
    ('pluggy', local_pluggy_version, {
        'checksums': ['15b2acde666561e1298d71b523007ed7364de07029219b604cf808bfa1c765b0'],
    }),
    ('py', local_py_version, {
        'checksums': ['9ca6883ce56b4e8da7e79ac18787889fa5206c79dcc67fb065376cd2fe03f342'],
    }),
    ('toml', local_toml_version, {
        'checksums': ['b3bda1d108d5dd99f4a20d24d9c348e91c4db7ab1b749200bded2f839ccbe68f'],
    }),
    ('pytest', local_pytest_version, {
        'checksums': ['c0a7e94a8cdbc5422a51ccdad8e6f1024795939cc89159a0ae7f0b316ad3823e'],
    }),
    #
    # Essential packages
    #
    ('Cython', local_Cython_version, { # Needed for NumPy according to the NumPy documentation.
        'checksums': ['e57acb89bd55943c8d8bf813763d20b9099cc7165c0f16b707631a7654be9cad'],
    }),
    ('numpy', local_numpy_20_1_version, {
        'patches': ['numpy-%(version)s-mkl.patch'],
        'source_tmpl': '%(name)s-%(version)s.zip',
        'checksums': [
            '34e96e9dae65c4839bd80012023aadd6ee2ccb73ce7fdf3074c62f301e63120b',  # numpy-1.18.5.zip
            'e3264d986b52c81fe5edaf8bb83bef0dc036706e2a9e3c52901ce30a38edf100',  # numpy-1.18.5-mkl.patch
        ],
    }),
    ('scipy', local_scipy_20_1_version, {
        'checksums':      ['4a453d5e5689de62e5d38edf40af3f17560bfd63c9c5bd228c18c1f99afa155b'],  # scipy-1.5.4.tar.gz
    }),    
    ('ase', local_ASE_20_1_version, {
        'checksums':      ['27c378b983dfacd49398236e7232c28590c218c31bb2205695818552c772bc4b'],
    }),
    ('gpaw', local_GPAW_20_1_version, {
        'checksums':      ['c84307eb9943852d78d966c0c8856fcefdefa68621139906909908fb641b8421'],
        'preinstallopts': 'cat >siteconfig.py <<EOF\n' + local_gpaw_customize + 'EOF\n' + 
                          'unset CC && unset LDSHARED && CFLAGS="$CFLAGS -qno-openmp-simd" ',
    }),
]

# Sanity check changed compared to version 19.9.1!
sanity_check_paths = {
    'files': [],
#    'files': ['bin/gpaw%s' % x for x in ['', '-analyse-basis', '-basis', '-mpisim', '-plot-parallel-timings',
#                                         '-runscript', '-setup', '-upfplot']],
    'dirs':  ['lib/python%(pyshortver)s/site-packages']
}

#sanity_check_commands = ["python -c 'import tkinter' "]

# As the main EasyBlock is "Bundle" PYTHONPATH will not be set automatically
modextrapaths = {
    'PYTHONPATH':      ['lib/python%(pyshortver)s/site-packages'],
}

modextravars = {
    'GPAW_SETUP_PATH': '%(installdir)s/share/gpaw-setups', # We don't use modextrapaths as the path should be  
                                                           # comma-separated rather than colon-separated anyway.
    'OMP_NUM_THREADS': '1',
}

modluafooter = """
family("GPAW")
"""

moduleclass = 'chem'
