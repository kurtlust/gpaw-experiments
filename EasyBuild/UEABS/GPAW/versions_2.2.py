local_UEABS_version =        '2.2'
local_intel_version =        '2020a'
local_gcc_version =          '9.3.0'
local_binutils_version =     '2.34'

local_ncurses_version =      '6.2'       # https://ftp.gnu.org/pub/gnu/ncurses/
local_libreadline_version =  '8.0'       # https://ftp.gnu.org/pub/gnu/readline/
local_libffi_version =       '3.3'       # ftp://sourceware.org/pub/libffi/
local_zlib_version =         '1.2.11'    # https://zlib.net/
local_OpenSSL_version =      '1.1.1g'    # https://www.openssl.org/source/
local_SQLite_version =       '3.33.0'    # https://ftp.gnu.org/pub/gnu/readline/

local_Python_3_7_version =   '3.7.9'     # https://www.python.org/downloads/
local_Python_3_8_version =   '3.8.5'     # https://www.python.org/downloads/

local_pip_version =          '20.2.3'    # https://pypi.org/project/pip/
local_setuptools_version =   '50.3.0'    # https://pypi.org/project/setuptools/
local_wheel_version =        '0.35.1'    # https://pypi.org/project/wheel/

local_zipp_version =         '3.4.0'     # Only needed for Python 3.7 and older
local_importlibmd_version =  '2.0.0'     # Only needed for Python 3.7 and older.
local_six_version =          '1.15.0'
local_attrs_version =        '20.3.0'
local_iniconfig_version =    '1.1.1'
local_pyparsing_version =    '2.4.7'
local_packaging_version =    '20.4'
local_pluggy_version =       '0.13.1'
local_py_version =           '1.9.0'
local_toml_version =         '0.10.2'
local_pytest_version =       '6.1.2'

local_Cython_version =       '0.29.21'   # https://pypi.org/project/Cython/

local_numpy_18_version =     '1.18.5'    # https://pypi.org/project/numpy/
local_numpy_19_version =     '1.19.2'    # https://pypi.org/project/numpy/
local_scipy_4_version =      '1.4.1'     # https://pypi.org/project/scipy/
local_scipy_5_version =      '1.5.3'     # https://pypi.org/project/scipy/

local_libxc_version =        '4.3.4'     # https://www.tddft.org/programs/libxc/download/
local_FFTW_version =         '3.3.8'     # http://www.fftw.org/

local_GPAWsetup_version =    '0.9.20000' # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html

local_numpy_1_5_version =    '1.18.5'    # https://pypi.org/project/numpy/, GPAW fails with numpy 1.19.x
local_scipy_1_5_version =    '1.4.1'     # https://pypi.org/project/scipy/
local_ASE_1_5_version =      '3.17.0'    # Official corresponding ase release is 3.17.0 
local_GPAW_1_5_version =     '1.5.2'

local_numpy_19_8_version =   '1.18.5'    # https://pypi.org/project/numpy/, GPAW fails with numpy 1.19.x
local_scipy_19_8_version =   '1.5.4'     # https://pypi.org/project/scipy/
local_ASE_19_8_version =     '3.18.2'    # Official corresponding ase release is 3.18.0 
local_GPAW_19_8_version =    '19.8.1'

local_numpy_20_1_version =   '1.18.5'    # https://pypi.org/project/numpy/, GPAW fails with numpy 1.19.x
local_scipy_20_1_version =   '1.5.4'     # https://pypi.org/project/scipy/
local_ASE_20_1_version =     '3.19.3'    # Official corresponding ase release is 3.19.0 or later
local_GPAW_20_1_version =    '20.1.0'

local_numpy_20_10_version =  '1.19.4'    # https://pypi.org/project/numpy/, GPAW fails with numpy 1.19.x
local_scipy_20_10_version =  '1.5.4'     # https://pypi.org/project/scipy/
local_ASE_20_10_version =    '3.20.1'    # Official corresponding ase release is 3.20.1
local_GPAW_20_10_version =   '20.10.0'
