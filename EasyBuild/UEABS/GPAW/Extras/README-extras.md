# Extras for using EasyBuild to build the GPAW benchmark

This directory contains additional EasyConfig files that are useful
to compile a Python interpreter with sufficient graphics support to
run `matplotlib` which is an optional dependency of `ase`.


## Overview of EasyConfigs for the GPAW benchmark

### Dependencies for Python that can be GCC-compiled without performance consequences

These EasyConfig files are provided as separate EasyConfigs for build configuration
`4_AllSeparate`.

The following packages are only needed if the matplotlib Python package is 
installed, which is an optional package for ase and not needed for running the
benchmarks.
  * bzip2: Needed for freetype.
  * libpng: Needed for freetype.
  * freetype: Needed for the matplotlib Python extension.


