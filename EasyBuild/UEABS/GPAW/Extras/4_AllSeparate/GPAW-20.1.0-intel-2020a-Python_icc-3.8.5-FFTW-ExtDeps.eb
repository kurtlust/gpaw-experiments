easyblock = 'Bundle'

local_intel_version =      '2020a'
local_gcc_version =        '9.3.0'
local_binutils_version =   '2.34'
local_buildtools_version = '1.0'

local_ASE_version =       '3.19.3'
local_GPAW_version =      '20.1.0'
local_GPAWsetup_version = '0.9.20000' # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html
local_Python3_version =   '3.8.5'     # https://www.python.org/downloads/
local_FFTW_version =      '3.3.8'     # http://www.fftw.org/
local_libxc_version =     '4.3.4'     # https://www.tddft.org/programs/libxc/download/

name =                    'GPAW'
version =                 local_GPAW_version
versionsuffix =           '-Python_icc-%(pyver)s-FFTW-ExtDeps'
local_dep_versionsuffix = '-UEABS-GPAW'

homepage = 'http://wiki.fysik.dtu.dk/gpaw'

whatis = [
    "Description: GPAW: DFT and beyond within the projector-augmented wave method, configuration suitable for the UEABS benchmark suite.",
    'This module also includes the compatible ASE version ' + local_ASE_version,
]

description = """
GPAW is a density-functional theory (DFT) Python code based on the
projector-augmented wave (PAW) method and the atomic simulation environment
(ASE). It uses plane-waves, atom-centered basis-functions or real-space
uniform grids combined with multigrid methods.

This module also includes a matching version of ASE (version %(ASE_version)s),
the Atomic Simulation Environment, which is used internally by GPAW.

The module also contains the Atomic PAW setup files (version %(GPAWsetup_version)s)
in the directory $GPAW_SETUP_FILES.

The GPAW manual advises the OMP_NUM_THREADS should be set to 1 so we do this
in the module.

The version in this module is compiled using the Intel MKL libraries for FFT.
The optional libvdwxc support is NOT included in this module.
The optional ELPA support is included in this module.
""" % {
    'ASE_version':       local_ASE_version,  
    'GPAWsetup_version': local_GPAWsetup_version,  
}

docurls = [
    'GPAW web-based documentation: https://wiki.fysik.dtu.dk/gpaw/',
    'ASE web-based documentation:https://wiki.fysik.dtu.dk/ase/',
]

toolchain =     {'name': 'intel', 'version': local_intel_version}
toolchainopts = {'pic': True}

dependencies = [
    ('Python',   local_Python3_version, local_dep_versionsuffix),
    ('FFTW',     local_FFTW_version,    local_dep_versionsuffix),
    ('libxc',    local_libxc_version,   local_dep_versionsuffix),
]

#
# EasyBuild on our machine (EasyBuild 4.2.2, CentOS 7.7 Python 2.7.5
# Did not accept multiple lines of the form [xxx] and produced warnings of
# EOF at scatnning triple-quoted string literal. Hence the construction
# where we use %(FB)s which we dan substitute with [.
#
local_matplotlib_customize = """
%(FB)segg_info]

%(FB)slibs]
# By default, Matplotlib builds with LTO, which may be slow if you re-compile
# often, and don't need the space saving/speedup.
enable_lto = False
# By default, Matplotlib downloads and builds its own copy of FreeType, and
# builds its own copy of Qhull.  You may set the following to True to instead
# link against a system FreeType/Qhull.
system_freetype = True
system_qhull = False

%(FB)spackages]
# There are a number of data subpackages from Matplotlib that are
# considered optional. All except 'tests' data (meaning the baseline
# image files) are installed by default, but that can be changed here.
tests = False
sample_data = True

%(FB)sgui_support]
# Matplotlib supports multiple GUI toolkits, known as backends.
# The MacOSX backend requires the Cocoa headers included with XCode.
# You can select whether to build it by uncommenting the following line.
# It is never built on Linux or Windows, regardless of the config value.
#
#macosx = True

%(FB)src_options]
# User-configurable options
#
# Default backend, one of: Agg, Cairo, GTK3Agg, GTK3Cairo, MacOSX, Pdf, Ps,
# Qt4Agg, Qt5Agg, SVG, TkAgg, WX, WXAgg.
#
# The Agg, Ps, Pdf and SVG backends do not require external dependencies.  Do
# not choose MacOSX if you have disabled the relevant extension modules.  The
# default is determined by fallback.
#
backend = Agg
""" % { 'FB' : '['}

local_gpaw_customize = """
print( 'GPAW EasyBuild INFO: Starting execution of the customization script' )
print( 'GPAW EasyBuild INFO: Variables at the start of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =               ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =            ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =            ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =               ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =           ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =            ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                    ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =               ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                    ', elpa )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )

# LibXC via EasyBuild
include_dirs.append(os.path.join(os.getenv('EBROOTLIBXC'), 'include'))
#libraries.append('xc')

# libvdwxc
libvdwxc = False

# ELPA
elpa = False
#include_dirs.append(os.path.join(os.getenv('EBROOTELPA'), 'include/elpa'))
#libraries.append('elpa')

# Use regular FFTW
fftw = True
libraries += ['fftw3']

# ScaLAPACK
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# MKL BLAS
libraries += ['mkl_sequential','mkl_core', 'mkl_rt', ]

# Add other EasyBuild library directoryes.
library_dirs = os.getenv('LIBRARY_PATH').split(':')

# Set the compilers
compiler =    'icc'
mpicompiler = 'mpiicc'
mpilinker =   'mpiicc'

print( 'GPAW EasyBuild INFO: Variables at the end of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =               ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =            ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =            ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =         ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =    ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs = ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =            ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =        ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =             ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =               ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =      ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =         ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =           ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =       ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =            ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                    ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =               ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                    ', elpa )
print( 'GPAW EasyBuild INFO: Ending execution of the customization script' )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )
"""

default_easyblock = 'Tarball'

components = [
    ( 'GPAW-setups', local_GPAWsetup_version, { 
        # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html
        'sources':   [{
                        'filename':    SOURCELOWER_TAR_GZ,
                        'source_urls': ['https://wiki.fysik.dtu.dk/gpaw-files/'], 
                        'extract_cmd': 'mkdir -p share/gpaw-setups ; cd share/gpaw-setups ; tar -xf %s --strip-components=1',
                     }]  
    }),
]

# This is a bundle of Python packages
exts_defaultclass = 'PythonPackage'
exts_filter = ("python -c 'import %(ext_name)s'", '')
exts_default_options = {
    'download_dep_fail': True,
    'sanity_pip_check':  False, # Otherwise requires the optional dependencies of GPAW.
    'source_urls':       [PYPI_SOURCE],
    'use_pip':           False,
}

exts_list = [
    ('Cython', '0.29.21', { # Needed for NumPy according to the NumPy documentation.
        'checksums': ['e57acb89bd55943c8d8bf813763d20b9099cc7165c0f16b707631a7654be9cad'],
    }),
    # NumPy
    ('numpy', '1.18.5', {
        'patches': ['numpy-%(version)s-mkl.patch'],
        'source_tmpl': '%(name)s-%(version)s.zip',
        'checksums': [
            '34e96e9dae65c4839bd80012023aadd6ee2ccb73ce7fdf3074c62f301e63120b',  # numpy-1.18.5.zip
            'e3264d986b52c81fe5edaf8bb83bef0dc036706e2a9e3c52901ce30a38edf100',  # numpy-1.18.5-mkl.patch
        ],
     }),
    ('scipy', '1.4.1', {
        'patches': ['scipy-1.4.1-fix-pthread.patch'],
        'checksums': [
            'dee1bbf3a6c8f73b6b218cb28eed8dd13347ea2f87d572ce19b289d6fd3fbc59',  # scipy-1.4.1.tar.gz
            '4e2162a93caddce63a1aa2dfb6c181774a4f6615950e1d60c54bb4308fee3bb3',  # scipy-1.4.1-fix-pthread.patch
        ],
    }),    
    # ASE and its dependencies
    ('Werkzeug', '1.0.1', {
        'checksums':      ['6c80b1e5ad3665290ea39320b91e1be1e0d5f60652b964a3070216de83d2e47c'],
    }),
    ('MarkupSafe', '1.1.1', {
        'checksums':      ['29872e92839765e546828bb7754a68c418d927cd064fd4708fab9fe9c8bb116b'],
    }),
    ('Jinja2', '2.11.2', { # Contains only .py files so probably safe as zipped egg?
        'checksums':      ['89aab215427ef59c34ad58735269eb58b1a5808103067f7bb9d5836c651b3bb0'],
    }),
    ('itsdangerous', '1.1.0', { # Installs as a zipped egg by itself.
        'checksums':      ['321b033d07f2a4136d3ec762eac9f16a10ccd60f53c0c91af90217ace7ba1f19'],
    }),
    ('click', '7.1.2', { # Included in the Python 3.7.4 extension list.
        'checksums':      ['d2b5255c7c6349bc1bd1e59e08cd12acbbd63ce649f2588755783aa94dfb6b1a'],
    }),
    ('Flask', '1.1.2', {
        'checksums':      ['4efa1ae2d7c9865af48986de8aeb8504bf32c7f3d6fdc9353d34b21f4b127060'],
    }),
    ('six', '1.15.0', {}),
    ('cycler', '0.10.0', {}),
    ('kiwisolver', '1.2.0', {}),
    ('pyparsing', '3.0.0a2', {}),
    ('python-dateutil', '2.8.1', { 'modulename': 'dateutil' }),
    ('Pillow', '7.2.0', { # --disable-jpeg does not work...
        'prebuildopts': 'MAXCONCURRENCY=20 ',
        'buildopts':    '--enable-zlib --enable-freetype --enable-jpeg --disable-tiff --disable-lcms --disable-webp --disable-webpmux --disable-jpeg2000 --disable-imagequant --disable-xcb',
        'buildcmd':     'build_ext',
        'modulename':   'PIL',
    }),
    ('matplotlib', '3.3.0', {
        'use_pip':           False,
        'prebuildopts':      'cat >setup.cfg <<EOF\n' + local_matplotlib_customize + 'EOF\n',  
    }),
    ('ase', local_ASE_version, {
        'checksums':      ['27c378b983dfacd49398236e7232c28590c218c31bb2205695818552c772bc4b'],
    }),
    # GPAW and its dependencies
    ('gpaw', version, {
#        'use_pip':        False,
        'prebuildopts':   'cat >siteconfig.py <<EOF\n' + local_gpaw_customize + 'EOF\n' + 
                          'unset CC && unset LDSHARED && CFLAGS="$CFLAGS -qno-openmp-simd" ',
        'buildopts':      ' && echo -e "\\nGPAW EasyBuild INFO: configuration log:\\n" && cat configuration.log && echo -e "\\nEnd of GPAW configuration log\\n"',
        'preinstallopts': 'unset CC && unset LDSHARED && CFLAGS="$CFLAGS -qno-openmp-simd" ',
    }),
]

# Sanity check changed compared to version 19.9.1!
sanity_check_paths = {
    'files': [],
#    'files': ['bin/gpaw%s' % x for x in ['', '-analyse-basis', '-basis', '-mpisim', '-plot-parallel-timings',
#                                         '-runscript', '-setup', '-upfplot']],
    'dirs':  ['lib/python%(pyshortver)s/site-packages']
}

#sanity_check_commands = ["python -c 'import tkinter' "]

# As the main EasyBlock is "Bundle" PYTHONPATH will not be set automatically
modextrapaths = {
    'PYTHONPATH':      ['lib/python%(pyshortver)s/site-packages'],
}

modextravars = {
    'GPAW_SETUP_PATH': '%(installdir)s/share/gpaw-setups', # We don't use modextrapaths as the path should be  
                                                           # comma-separated rather than colon-separated anyway.
    'OMP_NUM_THREADS': '1',
}

moduleclass = 'chem'
