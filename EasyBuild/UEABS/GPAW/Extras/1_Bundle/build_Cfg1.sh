#
# Configuration 1: All-in-one bundle
#

#
# GPAW
#

# Non-default parallel configuration

eb GPAW-1.5.2-intel-2020a-Python-3.7.9-parallel_so-Cfg1-UEABS-GPAW.eb -f

eb GPAW-19.8.1-intel-2020a-Python-3.7.9-FFTW-parallel_so-Cfg1-UEABS-GPAW.eb -f
eb GPAW-19.8.1-intel-2020a-Python-3.7.9-MKLFFTW-parallel_so-Cfg1-UEABS-GPAW.eb -f
eb GPAW-19.8.1-intel-2020a-Python-3.7.9-NumPyFFT-parallel_so-Cfg1-UEABS-GPAW.eb -f

