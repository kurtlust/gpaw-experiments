easyblock = 'EB_Python'

local_UEABS_version =        '2.2'
local_intel_version =        '2020a'
local_gcc_version =          '9.3.0'
local_binutils_version =     '2.34'

local_ncurses_version =      '6.2'      # https://ftp.gnu.org/pub/gnu/ncurses/
local_libreadline_version =  '8.0'      # https://ftp.gnu.org/pub/gnu/readline/
local_libffi_version =       '3.3'      # ftp://sourceware.org/pub/libffi/
local_zlib_version =         '1.2.11'   # https://zlib.net/
local_OpenSSL_version =      '1.1.1k'   # https://www.openssl.org/source/
local_SQLite_version =       '3.33.0'   # https://ftp.gnu.org/pub/gnu/readline/

local_Python3_9_version =    '3.9.4'    # https://www.python.org/downloads/

local_pip_version =          '21.0.1'   # https://pypi.org/project/pip/
local_setuptools_version =   '53.0.0'   # https://pypi.org/project/setuptools/
local_wheel_version =        '0.36.2'   # https://pypi.org/project/wheel/

name =          'Python'
version =       local_Python3_9_version
versionsuffix = '-Cfg5-UEABS-GPAW'

local_pymaj   = ''.join(version.split('.')[:1])
local_pycpver = ''.join(version.split('.')[:2])

homepage = 'http://python.org/'

whatis = [
    'Description: Python %(version)s interpreter with a range of optional packages'
]

description = """
This is the Python %(version_major_minor)s interpreter with only those packages
that are essential to install your own packages.
"""

usage = """
There is ample documentation about Python available on the web; please check
there or any good book about Python for usage information.

To get a list of installed Python packages, load this module file and then
execute
pydoc modules
"""

toolchain =     {'name': 'intel', 'version': local_intel_version}
toolchainopts = {'pic': True}

source_urls = ['http://www.python.org/ftp/%(namelower)s/%(version)s/']
sources =     [SOURCE_TGZ]
checksums =   ['4b0e6644a76f8df864ae24ac500a51bbf68bd098f6a173e27d3b61cdca9aa134']

dependencies = [
    ('ncurses',     local_ncurses_version,     versionsuffix),
    ('libreadline', local_libreadline_version, versionsuffix),
    ('libffi',      local_libffi_version,      versionsuffix),
    ('zlib',        local_zlib_version,        versionsuffix),
    ('OpenSSL',     local_OpenSSL_version,     versionsuffix),
    ('SQLite',      local_SQLite_version,      versionsuffix),
]

builddependencies = [
    ('buildtools', local_UEABS_version,    '', True),
]


################################################################################
#
# Configure the Python build
#

preconfigopts = ''

# Uncomment and adapt the following line when using the system OpenSSL libraries to avoid linking with the wrong ones.
#preconfigopts  = 'OPENSSL_INCLUDES="/usr/include" OPENSSL_LDFLAGS="-L/usr/lib64" OPENSSL_LIBS="-lssl -lcrypto" ' # May be needed to avoid linking with the libssl from NSS if present.

# Uncomment the next line with the Intel 2020 compilers and possibly some other ones also
preconfigopts += 'CFLAGS="$CFLAGS -fwrapv" ' # Needed to work a compile problem of some Python versions with some versions of the Intel compilers.

configopts = ''
# --with-system-ffi is ignored on the cluster
#configopts += '--with-system-expat --with-system-libmpdec --with-dbmliborder=gdbm '

# symlink 'pip' command to 'pip3' that is included with Python installation
# required so we can update pip to version included in extensions, using pip
installopts = " && ln -s %(installdir)s/bin/pip3 %(installdir)s/bin/pip"

# Python EasyBlock options. This are in fact the default values in EB 4.2 but since
# we have bad experiences with defaults changing we prefer to specify them anyway.
optimized        = True
use_lto          = None
ulimit_unlimited = False

################################################################################
#
# Installation process of the extensions
#

exts_default_options = {
    'download_dep_fail': True,
    'sanity_pip_check':  True,
    'source_urls':       [PYPI_SOURCE],
    'use_pip':           True,
}

# order is important!
exts_list = [
#
# Reploacing the included pip and setuptools with newer versions doesn't seem to work as
# it should in Python 3.9.1.
#
#    ('pip', local_pip_version, {
#        'use_pip': False,
#        'checksums': ['99bbde183ec5ec037318e774b0d8ae0a64352fe53b2c7fd630be1d07e94f41e5'],
#    }),
#    ('setuptools', local_setuptools_version, {
#        'checksums': ['1b18ef17d74ba97ac9c0e4b4265f123f07a8ae85d9cd093949fa056d3eeeead5'],
#    }),
    ('wheel', local_wheel_version, {
        'checksums':   ['e11eefd162658ea59a60a0f6c7d493a7190ea4b9a85e335b33489d9f17e0245e'],
    }),
]

moduleclass = 'lang'
