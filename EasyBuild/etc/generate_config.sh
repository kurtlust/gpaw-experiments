#!/bin/bash

#
# Configuration for CalCUA systems
#

datadir="/data/antwerpen/202/vsc20259/Projects/PRACE/GPAW-experiments/EasyBuild"
scratchdir="/scratch/antwerpen/202/vsc20259/UEABS"
site="CalcUA"

# Leibniz (Intel Broadwell)
system=leibniz-broadwell
gpp -DDATADIR=$datadir -DSCRATCHDIR=$scratchdir -DSITE=$site -DSYSTEM=$system -DUSER=vsc20259 \
  -o ${site}-${system}.cfg-gen template.cfg-tmpl

# Vaughan (AMD Rome)
system=vaughan-rome
gpp -DDATADIR=$datadir -DSCRATCHDIR=$scratchdir -DSITE=$site -DSYSTEM=$system -DUSER=vsc20259 \
  -o ${site}-${system}.cfg-gen template.cfg-tmpl
cat >>${site}-${system}.cfg-gen <<EOF
hide-deps=imkl,impi,icc,ifort,iccifort,iimpi
hide-toolchains=icc,ifort,iccifort,iimpi
optarch=Intel:march=core-avx2 -mtune=core-avx2;GCC:march=znver2 -mtune=znver2
EOF
