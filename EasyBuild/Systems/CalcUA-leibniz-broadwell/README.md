# Leibniz at University of Antwerpen

Leibniz is an Intel Broadwell based system at the University of Antwerpen, Belgium.
Software on the cluster is managed through EasyBuild. Most software is installed with
the Intel compilers and EasyBuild-compatible Intel modules, implying that a more recent
version of the GNU compilers is used rather than the system GNU compilers.

The cluster uses [LMOD](https://www.tacc.utexas.edu/research-development/tacc-projects/lmod) 
as the environment module system.

There is one major difference with a traditional EasyBuild software installation though:
At the University of Antwerpen, the Intel Parallel Studio XE is installed by hand in 
a single directory tree (not the multiple trees, one per component, as is done in a 
traditional EasyBuild installation). We use a hand-generated module for the Intel compilers 
and some fake EasyConfigs and corresponding modules for the components that EasyBuild 
expects simply to keep EasyBuild happy.

## Subdirectories in this repository.

  * Subdirectory EasyBuild: Wrapper EasyBuild modules that define an alias to call 
    EasyBuild specifying the right configuration file.
  * Subdirectory Repo-link: EasyConfig files that correspond to modules on our system.
    In our software installation library pointed to by the EasyBuild configuration file,
    we simply installed links to the existing modules.
  * Subdirectory Modules: Some examples for the hand-written module files that we use
    on our system. The binutils and GCCcore modules are generated with EasyBuild.

## Local installation directory structure

The local install directory that we used, has 4 subdirectories:
  * Modules-link
      * Link to the EasyBuild modules directory (making all installed versions of EasyBuild 
        available)
      * binutils subdirectory with links to the binutils modules on the system needed 
        for the Intel toolchains that we tested with.
      * GCCcore subdirectory with links to the GCCcore versions relevant for the Intel 
        toolchains that we tested with.
      * intel subdirectory with links to the intel modules that we used for testing.
      * And similarly for the iccifort, iimpi, impi and imkl modules needed for the 
        EasyBuild Intel toolchains. Note that on the CalcUA systems, a non-standard 
        setup of the Intel toolchains is used, installing the Intel compilers by hand,
        manually creating an intel module with all changes to variables and paths, 
        and dummy modules for iccifort, iimpi, impi and imkl only to keep EasyBuild
        happy. The systems also use a non-standard EasyConfig for the Intel compilers
        matching with that structure.
  * Modules: All modules generated through EasyBuild for the UEABS benchmarking project.
  * Packages: Contains the actual software installations done by EasyBuild.
  * Repo: Repo directorty used by EasyBuild to gather all EasyConfig files for packages
    installed through EasyBuild.

There is no Repo-link in that structure in analogy to the Modules-link. As indicated 
above, we store the EasyConfig files in the Repo-link subdirectory of this directory
so that we can edit them (and manage the edits) if needed.

The configuration file for EasyBuild is derived from the standard template 
`EasyBuild/etc/template.cfg-tmpl` in this repository.

At CalcUA, a custom naming scheme is used for EasyBuild that does not use the subdivision
in module classes in the standard EasyBuild naming scheme.
