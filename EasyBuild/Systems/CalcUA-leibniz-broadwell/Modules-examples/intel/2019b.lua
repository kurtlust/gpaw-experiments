local root =                 "/apps/antwerpen/x86_64/centos7/intel-psxe/2019_update4"
local compilerversion =      "2019.4.243"
local compilermajorversion = "2019"
local ebversion =            "2019b"
local productversionstring = "2019 Update 4"

local GCCversion =           "8.3.0"
local binutilsversion =      "2.32"

local compilerroot =         root .. "/compilers_and_libraries_" .. compilerversion .. "/linux"
local docroot =              root .. "/documentation_" .. compilermajorversion .. "/en"

helpstring = string.gsub( string.gsub( [[

Description
===========
Intel Parallel Studio XE PVERSION update MVERSION Cluster Edition.
This module gives access to:
* C/C++ and Fortran compilers
* MPI libraries
* The Math Kernel Library (MKL), including BLAS, LAPACK, ScaLAPACK and Intel FFT routines
* The Thread Building Blocks (TBB) library
* The Integrated Performance Primitives (IPP) library
* The Data Analytics Acceleration Library (DAAL) library
Further development tools can be found in the inteldevtools/EBVERSION module.


More information
================
 - Homepage: https://software.intel.com/en-us/intel-parallel-studio-xe
]], "PVERSION", productversionstring ), "EBVERSION", ebversion )
help( helpstring)

whatisstring = string.gsub( string.gsub( 
    [[Description: Intel Parallel Studio XE PVERSION Cluster Edition: Compilers, Intel MPI and the MKL, IPP, TBB and DAAL libraries. See the inteldevtools/EBVERSION module for the development tools - https://software.intel.com/en-us/intel-parallel-studio-xe ]], 
    "PVERSION", productversionstring ), "EBVERSION", ebversion )
whatis( whatisstring )
whatis([[URL:         https://software.intel.com/en-us/intel-parallel-studio-xe]])

conflict("intel")

-- Note that the toolchain verifier _verify_toolcahin in the EAsyBuild file tools/toolchain/toolchain.py
-- will fail to detect the GCCcore and binutils modules as it parses the module file using a simple
-- regular expression (in routine dependencies_for in tools/module_generator).
-- This is currently not a problem as they are not mandatory components in the Intel toolchain.
-- If this would become a problem, load("GCCcore/" .. GCCversion) might work, but do not use
-- a space before the double quote of GCCcore or the regular expression that is used for parsing
-- does not match anymore!
GCCcoremodule = "GCCcore/" .. GCCversion
if not isloaded( GCCcoremodule ) then
    load( GCCcoremodule )
end

binutilsmodule = "binutils/" .. binutilsversion .. "-GCCcore-" .. GCCversion
if not isloaded( binutilsmodule ) then
    load( binutilsmodule )
end

--
-- When EasyBuild is loaded, we load a number of phony modules to tick 
-- EasyBuild into believing a regular Intel toolchain with all submodules
-- has been loaded.
-- Note that we need this strange construct to make sure that module load
-- and module unload work correctly. Otherwise the phony modules
-- are removed too early, causing an error message about not being
-- able to remove the icc/phony etc. modules.
-- 
-- Note that the following construction is discouraged in the "Dependent
-- modules" section of the Lmod manual on readthedocs, but we still chose
-- to use it as we think the side effects will not occur in the cases where
-- we use this
--
-- if ( isloaded( "EasyBuild" ) or mode() == "unload" ) then
--
-- Actually we don't need this construct at all, EasyBuild only needs to find
-- the load lines and as it parses with a simple regular expression, we can put 
-- them in a block that is never executed.
--
if false then

    -- Note that since EasyBuild will be parsing the following lines, the syntax is more restrictive
    -- than what LUA allows. We should explicitly specify module names, and there should be no whitespace
    -- between the opening parantheses and the double quote. Nor should we be using single quotes as a 
    -- string delimiter.
    load("iccifort/phony")
    load("impi/phony")
    load("imkl/phony")

end

--
-- License
--
prepend_path( "INTEL_LICENSE_FILE",   "/apps/antwerpen/licenses/intel/license.lic" )

--
-- Compilers
--
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/compiler/lib/intel64_lin" )

prepend_path( "LIBRARY_PATH",         compilerroot .. "/compiler/lib/intel64_lin" )

prepend_path( "MANPATH",              docroot .. "/man/common" )

-- The first one contains the compilervars scripts, the next one all compiler executables.
prepend_path( "PATH",                 compilerroot .. "/bin" )
prepend_path( "PATH",                 compilerroot .. "/bin/intel64" )

prepend_path( "NLSPATH",              compilerroot .. "/compiler/lib/intel64_lin/locale/%l_%t/%N" )

--
-- Debuggers
--
prepend_path( "LD_LIBRARY_PATH",      root .. "/debugger_" .. compilermajorversion .. "/libipt/intel64/lib" )

--
-- MPI-related
--
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/mpi/intel64/lib" )
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/mpi/intel64/lib/release" )
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/mpi/intel64/libfabric/lib" )


prepend_path( "LIBRARY_PATH",		      compilerroot .. "/mpi/intel64/libfabric/lib" )
-- The next line is usefull for packages that try to compile programs that need
-- mpi.h without using the MPI wrappers (e.g., the Rmpi R-package).
prepend_path( "LIBRARY_PATH",         compilerroot .. "/mpi/intel64/lib" )

prepend_path( "MANPATH",              compilerroot .. "/mpi/man" )

prepend_path( "PATH",                 compilerroot .. "/mpi/intel64/bin" )
prepend_path( "PATH",                 compilerroot .. "/mpi/intel64/libfabric/bin" )

-- The next two are usefull for packages that try to compile programs that need
-- mpi.h without using the MPI wrappers (e.g., the Rmpi R-package).
prepend_path( "CPATH",                compilerroot .. "/mpi/intel64/include" )
prepend_path( "FPATH",                compilerroot .. "/mpi/intel64/include" )

prepend_path( "CLASSPATH",            compilerroot .. "/mpi/intel64/lib/mpi.jar" )

setenv( "I_MPI_ROOT",                 compilerroot .. "/mpi" )
setenv( "I_MPI_OFI_LIBRARY_INTERNAL", "1" )

-- Fabric configuration
prepend_path( "FI_PROVIDER_PATH",     compilerroot .. "/mpi/intel64/libfabric/lib/prov" )
setenv( "FI_PROVIDER",                "verbs;ofi_rxm")

--
-- Libraries
--
-- - IPP

prepend_path( "CPATH",                compilerroot .. "/ipp/include" )
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/ipp/lib/intel64_lin" )
prepend_path( "LIBRARY_PATH",         compilerroot .. "/ipp/lib/intel64_lin" )

setenv( "IPPROOT",                    compilerroot .. "/ipp" )

-- - MKL

prepend_path( "CPATH",                compilerroot .. "/mkl/include/fftw" )
prepend_path( "CPATH",                compilerroot .. "/mkl/include" )
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/mkl/lib/intel64_lin" )
prepend_path( "LIBRARY_PATH",         compilerroot .. "/mkl/lib/intel64_lin" )
prepend_path( "NLSPATH",              compilerroot .. "/mkl/lib/intel64_lin/locale/%l_%t/%N" )
prepend_path( "PKG_CONFIG_PATH",      compilerroot .. "/mkl/bin/pkgconfig" )

setenv( "MKLROOT",                    compilerroot .. "/mkl" )

-- - TBB

prepend_path( "CPATH",                compilerroot .. "/tbb/include" )
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/tbb/lib/intel64_lin/gcc4.7" )
prepend_path( "LIBRARY_PATH",         compilerroot .. "/tbb/lib/intel64_lin/gcc4.7" )

setenv( "TBBROOT",                    compilerroot .. "/tbb" )

-- - DAAL

prepend_path( "CPATH",                compilerroot .. "/daal/include" )
prepend_path( "LD_LIBRARY_PATH",      compilerroot .. "/daal/lib/intel64_lin" )
prepend_path( "LIBRARY_PATH",         compilerroot .. "/daal/lib/intel64_lin" )
prepend_path( "CLASSPATH",            compilerroot .. "/daal/lib/daal.jar" )

setenv( "DAALROOT",                   compilerroot .. "/daal")

-- - PSTL (Parallel STL)

prepend_path( "CPATH",                compilerroot .. "/pstl/include" )

setenv( "PSTLROOTROOT",               compilerroot .. "/pstl")

--
-- EasyBuild toolchain compatibility
-- Some environment variables that others may potentially be using in
-- Makefiles etc.
--

setenv( "EBROOTINTEL",       root )
setenv( "EBROOTICC",         root )
setenv( "EBROOTIFORT",       root )
setenv( "EBROOTICCIFORT",    root )
setenv( "EBROOTIMKL",        compilerroot )
setenv( "EBROOTIMPI",        compilerroot .. "/mpi" )
setenv( "EBROOTIIMPI",       root )
setenv( "EBROOTTBB",         compilerroot .. "/tbb" )

setenv( "EBVERSIONINTEL",    ebversion )
setenv( "EBVERSIONICC",      compilerversion )
setenv( "EBVERSIONIFORT",    compilerversion )
setenv( "EBVERSIONICCIFORT", compilerversion )
setenv( "EBVERSIONIMKL",     compilerversion )
setenv( "EBVERSIONIMPI",     compilerversion )
setenv( "EBVERSIONIIMPI",    ebversion )
setenv( "EBVERSIONTBB",      ebversion )

--
-- Setting variables to make sure mpirun etc. use the resource manager in
-- the appropriate way
--

-- if { [info exists ::env(PBS_VERSION) ] } {
if os.getenv("PBS_VERSION") then
    setenv( "I_MPI_HYDRA_BOOTSTRAP", "pbsdsh" )
    setenv( "I_MPI_HYDRA_RMK",       "pbs" )
end

-- if { [info exists ::env(SLURM_JOB_ID) ] } {
if os.getenv("SLURM_JOB_ID") then
    setenv( "I_MPI_HYDRA_BOOTSTRAP", "slurm" )
    setenv( "SLURM_MPI_TYPE",        "pmi2" )
end

