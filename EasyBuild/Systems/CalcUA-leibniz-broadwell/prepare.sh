#!/bin/bash

# Name of the site and the identifier that we use for the system.
# We tend to combine the name of the cluster with the processor architecture which
# helps if the cluster has several CPU partitions with a different architecture.
site="CalcUA"
system=leibniz-broadwell

# Directory with this repository and where all valuable files will be stored.
datadir="${VSC_DATA}/Projects/PRACE/GPAW-experiments/EasyBuild"
# Scratch directory where files will be stored that can be easily regenerated.
# It can be on the same file system as $datadir. And in fact, even
# $datadir/Systems/$site-$system would still work as no directories that already
# exist in there are overwritten. But it does make git management a bit more
# difficult.
scratchdir="${VSC_SCRATCH}/UEABS"

easybuild_version=4.3.1
ebdir="$datadir/Systems/$site-$system"

#
# Generate EasyBuild configuration file
#

cd $datadir/etc
gpp -DDATADIR=$datadir -DSCRATCHDIR=$scratchdir -DSITE=$site -DSYSTEM=$system -DUSER=vsc20259 \
  -o ${site}-${system}.cfg-gen template.cfg-tmpl

#
# Create the directory structure
#

cd $scratchdir
mkdir -p $site-$system/Modules
mkdir -p $site-$system/Modules-link
mkdir -p $site-$system/Packages
mkdir -p $site-$system/Repo

#
# Create the Modules-link structure
#

moduleslink=$scratchdir/$site-$system/Modules-link
cd $moduleslink

mkdir -p $moduleslink/EasyBuild ; cd $moduleslink/EasyBuild 
[[ -f $easybuild_version.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-x86_64/EasyBuild/$easybuild_version.lua

mkdir -p $moduleslink/binutils ; cd $moduleslink/binutils
[[ -f 2.34-GCCcore-9.3.0.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/binutils/2.34-GCCcore-9.3.0.lua

mkdir -p $moduleslink/GCCcore ; cd $moduleslink/GCCcore
[[ -f 9.3.0.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/GCCcore/9.3.0.lua

mkdir -p $moduleslink/intel ; cd $moduleslink/intel
[[ -f 2020a.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-admin-x86_64/intel/2020a.lua

mkdir -p $moduleslink/iimpi ; cd $moduleslink/iimpi
cat >>2020a.lua <<EOF
help(  [[Dummy iimpi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iimpi module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink/impi ; cd $moduleslink/impi
cat >>phony.lua <<EOF
help(  [[Dummy impi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy impi module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink/imkl ; cd $moduleslink/imkl
cat >>phony.lua <<EOF
help(  [[Dummy imkl module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy imkl module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink/iccifort ; cd $moduleslink/iccifort
cat >>phony.lua <<EOF
help(  [[Dummy iccifort module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iccifort module to make easybuild happy with our intel toolchain.]])
EOF

#
# Now we are ready to use EasyBuild
#

cat <<EOF

Make sure you put the following directories in your modulepath to build the benchmarks:
  * $scratchdir/$site-$system/Modules-link
  * $scratchdir/$site-$system/Modules

EOF

module purge
MODULEPATH=$scratchdir/$site-$system/Modules:$scratchdir/$site-$system/Modules-link
#module use $scratchdir/$site-$system/Modules-link
#module use $scratchdir/$site-$system/Modules

module load EasyBuild/$easybuild_version

cd $ebdir/EasyBuild
eb --configfiles=$datadir/etc/$site-$system.cfg-gen EasyBuild-UEABS-$easybuild_version.eb -f

#
# From now on, when loading EasyBuild-UEABS, there is an alias eb-ueabs that enables using
# EasyBuild without having the pass the --configfiles argument.
# (can likely also be accomplished through an environment variable rather than a command line
# option).
#

module load EasyBuild-UEABS/$easybuild_version

#
# It may still be needed to set up the Repo-link directory if this would be a new system for
# which this has not yet been done.
#




