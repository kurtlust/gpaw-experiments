#!/bin/bash
# 
# The first part of this script, up to and including the building of EasyBuild, is done 
# in the script itself as this is a very quick process. After that, the building of
# the software is organised by multiple job submissions and job dependencies to make
# sure the compilation advances a bit faster if the cluster has enough available
# capacity. We could of course do even better by putting each EasyBuild build
# in a separate process but that would become cumbersome.
#

#
# Version of the UEABS benchmark
#
ueabs_version='2.2'

# Name of the site and the identifier that we use for the system.
# We tend to combine the name of the cluster with the processor architecture which
# helps if the cluster has several CPU partitions with a different architecture.
site="CalcUA"
system=leibniz-broadwell

# Directory with this repository and where all valuable files will be stored.
datadir="${VSC_DATA}/Projects/PRACE/GPAW-experiments/EasyBuild"
# Scratch directory where files will be stored that can be easily regenerated.
# It can be on the same file system as $datadir. And in fact, even
# $datadir/Systems/$site-$system would still work as no directories that already
# exist in there are overwritten. But it does make git management a bit more
# difficult.
scratchdir="${VSC_SCRATCH}/UEABS"

easybuild_version=4.3.1
ebdir="$datadir/Systems/$site-$system"

#
# Enable aliases in shell scripts
#
#shopt -s expand_aliases

#
# Generate EasyBuild configuration file
#

cd $datadir/etc
#gpp -DDATADIR=$datadir -DSCRATCHDIR=$scratchdir -DSITE=$site -DSYSTEM=$system -DUSER=vsc20259 \
#  -o ${site}-${system}.cfg-gen template.cfg-tmpl
#cat >>${site}-${system}.cfg-gen <<EOF
#hide-deps=imkl,impi,icc,ifort,iccifort,iimpi
#hide-toolchains=icc,ifort,iccifort,iimpi
#EOF

#
# Create the directory structure
#

cd $scratchdir
mkdir -p $site-$system/Modules
mkdir -p $site-$system/Modules-link
mkdir -p $site-$system/Packages
mkdir -p $site-$system/Repo

#
# Create the Modules-link structure
#

moduleslink=$scratchdir/$site-$system/Modules-link
cd $moduleslink

mkdir -p $moduleslink/EasyBuild ; cd $moduleslink/EasyBuild 
[[ -f $easybuild_version.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-x86_64/EasyBuild/$easybuild_version.lua

mkdir -p $moduleslink/binutils ; cd $moduleslink/binutils
[[ -f 2.34-GCCcore-9.3.0.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/binutils/2.34-GCCcore-9.3.0.lua

mkdir -p $moduleslink/GCCcore ; cd $moduleslink/GCCcore
[[ -f 9.3.0.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/GCCcore/9.3.0.lua

mkdir -p $moduleslink/intel ; cd $moduleslink/intel
[[ -f 2020a.lua ]] || ln -s /apps/antwerpen/modules/centos7/software-admin-x86_64/intel/2020a.lua

mkdir -p $moduleslink/iimpi ; cd $moduleslink/iimpi
cat >>2020a.lua <<EOF
help(  [[Dummy iimpi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iimpi module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink/impi ; cd $moduleslink/impi
cat >>phony.lua <<EOF
help(  [[Dummy impi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy impi module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink/imkl ; cd $moduleslink/imkl
cat >>phony.lua <<EOF
help(  [[Dummy imkl module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy imkl module to make easybuild happy with our intel toolchain.]])
EOF

mkdir -p $moduleslink/iccifort ; cd $moduleslink/iccifort
cat >>phony.lua <<EOF
help(  [[Dummy iccifort module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iccifort module to make easybuild happy with our intel toolchain.]])
EOF


#
# Now we are ready to use EasyBuild from the system
#

cat <<EOF

Make sure you put the following directories in your modulepath to build the benchmarks:
  * $scratchdir/$site-$system/Modules-link
  * $scratchdir/$site-$system/Modules

EOF

module purge
MODULEPATH=$scratchdir/$site-$system/Modules:$scratchdir/$site-$system/Modules-link

module load EasyBuild/$easybuild_version

# Just to be sure we also unset EBRAAOTEASYBUILDMINUEABS in case it would be in the environment
# after uncorrectly unloading the module.
cd $ebdir/EasyBuild
unset EBROOTEASYBUILDMINUEABS
eb --configfiles=$datadir/etc/$site-$system.cfg-gen EasyBuild-UEABS-$easybuild_version.eb -f

###############################################################################
#
# Now we start the builds, using job dependencies
#
# Note that due to the way the clean-up works in the scripts, one need to run
# each script on a dedicated node.
#

#
# Ensure variables that are needed by some of the scripts are properly exported
#
export JOBMODULEPATH=$MODULEPATH
export ueabs_version
export easybuild_version

#
# First phase: Build the buildtools module to have a consistent set of build tools.
#
cd $datadir/UEABS/buildtools
buildtools_jobid=$(qsub -L tasks=1:lprocs=28 -l walltime=45:00 -v JOBMODULEPATH,ueabs_version,easybuild_version -N UEABS_buildtools build_buildtools.sh)

#
# Build GPAW modules from the all-in-one configurations.
#
cd $datadir/UEABS/GPAW/1_Bundle
qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,easybuild_version -N UEABS_Cfg1 -W depend=afterok:$buildtools_jobid build_Cfg1.sh

#
# Build GPAW modules from the two bundle configuration.
#
#cd $datadir/UEABS/GPAW/2_TwoBundle
#qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,easybuild_version -N UEABS_Cfg2 -W depend=afterok:$buildtools_jobid build_Cfg2.sh

#
# Build GPAW modules from the third configuration: Python as a bundle
#
cd $datadir/UEABS/GPAW/3_PythonBundle
qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,easybuild_version -N UEABS_Cfg3 -W depend=afterok:$buildtools_jobid build_Cfg3.sh

#
# Build GPAW modules from the fourth configuration: Python dependencies only as a bundle
#
cd $datadir/UEABS/GPAW/4_PythonDepsBundle
qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,easybuild_version -N UEABS_Cfg4 -W depend=afterok:$buildtools_jobid build_Cfg4.sh

#
# Build GPAW modules from the fifth configuration: All separate modules
#
cd $datadir/UEABS/GPAW/5_AllSeparate
Cfg5_python_jobid=$(qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,easybuild_version -N UEABS_Cfg5_step1 -W depend=afterok:$buildtools_jobid build_Cfg5_python.sh)
qsub -L tasks=1:lprocs=28 -l walltime=12:00:00 -v JOBMODULEPATH,easybuild_version -N UEABS_Cfgs_step2 -W depend=afterok:$Cfg5_python_jobid build_Cfg5_GPAW.sh


#
# Rebuild the GPAW-manual modules
#

cd $datadir/../UEABS/build/examples/$site-$system

#sbatch -c 1 -n 64 -t 3:00:00 --dependency=afterok:$Cfg5_python_jobid --jobname UEABS_1_5_2_Python37icc_icc    build_1.5.2_Python37icc_icc.sh
#sbatch -c 1 -n 64 -t 3:00:00 --dependency=afterok:$Cfg5_python_jobid --jobname UEABS_1_5_2_Python37icc_eblibs build_1.5.2_Python37_icc_eblibs.sh
#sbatch -c 1 -n 64 -t 3:00:00 --dependency=afterok:$Cfg5_python_jobid --jobname UEABS_20_10_0_Python39icc_icc  build_20.10.0_Python39icc_icc.sh

#sbatch -c 1 -n 64 -t 3:00:00 --dependency=afterok:$buildtools_jobid  --jobname UEABS_1_5_2_IntelPython3_icc   build_1.5.2_IntelPython3_icc.sh
#sbatch -c 1 -n 64 -t 3:00:00 --dependency=afterok:$buildtools_jobid  --jobname UEABS_1_5_2_Python37_icc       build_1.5.2_Python37_icc.sh
#sbatch -c 1 -n 64 -t 3:00:00 --dependency=afterok:$buildtools_jobid  --jobname UEABS_20_10_0_Python39_icc     build_20.10.0_Python39_icc.sh


#
# Clean-up (only makes sense for rubbish left behind by building EasyBuild-UEABS).
#
find /tmp -maxdepth 1 -user $USER -exec /bin/rm -rf '{}' \;;
[ -d /dev/shm/$USER ] && /bin/rm -rf /dev/shm/$USER
