#!/bin/bash

ueabs_version='2.2'

# Name of the site and the identifier that we use for the system.
# We tend to combine the name of the cluster with the processor architecture which
# helps if the cluster has several CPU partitions with a different architecture.
site="CalcUA"
system=leibniz-broadwell

# Directory with this repository and where all valuable files will be stored.
datadir="${VSC_DATA}/Projects/PRACE/GPAW-experiments/EasyBuild"
# Scratch directory where files will be stored that can be easily regenerated.
# It can be on the same file system as $datadir. And in fact, even
# $datadir/Systems/$site-$system would still work as no directories that already
# exist in there are overwritten. But it does make git management a bit more
# difficult.
scratchdir="${VSC_SCRATCH}/UEABS"

module purge
MODULEPATH=$scratchdir/$site-$system/Modules:$scratchdir/$site-$system/Modules-link

module load EasyBuild-UEABS

#
# Build the buildtools module to have a consistent set of build tools.
#
cd $datadir/buildtools
eb buildtools-$ueabs_version.eb -f

#
# Build GPAW modules from the all-in-one configurations.
#
cd $datadir/GPAW/1_Bundle
source build_Cfg1.sh

#
# Build GPAW modules from the second configuration: Python as a bundle
#
cd $datadir/GPAW/2_PythonBundle
source build_Cfg2.sh

#
# Build GPAW modules from the third configuration: Python dependencies only as a bundle
#
cd $datadir/GPAW/3_PythonDepsBundle
source build_Cfg3.sh

#
# Build GPAW modules from the fourth configuration: All separate modules
#
cd $datadir/GPAW/5_AllSeparate
source build_Cfg5.sh

