# Example system installations

We refer to the individual subdirectories for more information

  * CalcUA-leibniz-broadwell is a Tier-2 system installed at the University of 
    Antwerpen in 2017. The system has 152 nodes with two 14-core Intel Broadwell 
    class Xeon CPUs. During the testing period, the system ran CentOS 7. The 
    software on the system is managed throughEasyBuild though with some local 
    adjustments to the standard EasyBuild setup.
  * CalcUA-vaughan-rome is a more recent Tier-2 system installed at the University
    of Antwerpen in 2020. This system is equiped with AMD Rome class processores 
    with 32 cores per socket and runs CentOS 8. The setup is otherwise similar 
    to CalcUA-leibniz-broadwell




