#! /bin/bash

ebdevrootscratch=${VSC_SCRATCH}/UEABS

[ -d $ebdevrootscratch/CalcUA-leibniz-broadwell/Modules-link ] || exit 
cd $ebdevrootscratch/CalcUA-leibniz-broadwell/Modules-link

echo "Linking EasyBuild..."
[ -d EasyBuild ] || ln -s /apps/antwerpen/modules/centos7/software-x86_64/EasyBuild

echo "Linking GCCcore..."
[ -d GCCcore ] || mkdir GCCcore
pushd GCCcore >& /dev/null
[ -f 9.3.0.lua ] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/GCCcore/9.3.0.lua
[ -f 8.3.0.lua ] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2019b/GCCcore/8.3.0.lua
popd >& /dev/null

echo "Linking binutils..."
[ -d binutils ] || mkdir binutils
pushd binutils >& /dev/null
[ -f 2.34-GCCcore-9.3.0.lua ] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2020a/binutils/2.34-GCCcore-9.3.0.lua
[ -f 2.32-GCCcore-8.3.0.lua ] || ln -s /apps/antwerpen/modules/centos7/software-broadwell/2019b/binutils/2.32-GCCcore-8.3.0.lua
popd >& /dev/null

echo "Linking intel..."
[ -d intel ] || mkdir intel
pushd intel >& /dev/null
[ -f 2020a ] || ln -s /apps/antwerpen/modules/centos7/software-admin-x86_64/intel/2020a
[ -f 2019b ] || ln -s /apps/antwerpen/modules/centos7/software-admin-x86_64/intel/2019b
popd >& /dev/null

#
# Creating stub modules to keep easybuild happy
#

[ -d iccifort ] || mkdir iccifort
pushd iccifort >& /dev/null
cat >phony.lua <<EOF
help(  [[Dummy iccifort module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iccifort module to make easybuild happy with our intel toolchain.]])
EOF
popd >& /dev/null

[ -d iimpi ] || mkdir iimpi
pushd iimpi >& /dev/null
cat >phony.lua <<EOF
help(  [[Dummy iimpi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy iimpi module to make easybuild happy with our intel toolchain.]])
EOF
popd >& /dev/null

[ -d impi ] || mkdir impi
pushd impi >& /dev/null
cat >phony.lua <<EOF
help(  [[Dummy impi module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy impi module to make easybuild happy with our intel toolchain.]])
EOF
popd >& /dev/null

[ -d imkl ] || mkdir imkl
pushd imkl >& /dev/null
cat >phony.lua <<EOF
help(  [[Dummy imkl module to make easybuild happy with our intel toolchain, should not be used by regular users.]])
whatis([[Description: Dummy imkl module to make easybuild happy with our intel toolchain.]])
EOF
popd >& /dev/null

