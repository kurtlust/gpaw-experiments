easyblock = 'Bundle'

local_UEABS_version =        '2.2'
local_intel_version =        '2020a'

local_Python_3_8_version =   '3.8.5'     # https://www.python.org/downloads/

local_Cython_version =       '0.29.21'   # https://pypi.org/project/Cython/
local_numpy_version =        '1.18.5'    # https://pypi.org/project/numpy/
local_scipy_version =        '1.4.1'     # https://pypi.org/project/scipy/

local_libxc_version =        '4.3.4'     # https://www.tddft.org/programs/libxc/download/

local_GPAWsetup_version =    '0.9.20000' # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html

local_ASE_20_1_version =     '3.19.3'    # Official corresponding ase release is 3.19.0 or later

local_GPAW_20_1_version =    '20.1.0'

local_UEABS_suffix = '-Cfg5-UEABS-GPAW'

name =          'GPAW'
version =       local_GPAW_20_1_version
versionsuffix = '-Python_icc-%(pyver)s-NumPyFFT' + local_UEABS_suffix

homepage = 'http://wiki.fysik.dtu.dk/gpaw'

whatis = [
    "Description: GPAW: DFT and beyond within the projector-augmented wave method.",
    'This module also includes the compatible ASE version ' + local_ASE_20_1_version,
]

description = """
GPAW is a density-functional theory (DFT) Python code based on the
projector-augmented wave (PAW) method and the atomic simulation environment
(ASE). It uses plane-waves, atom-centered basis-functions or real-space
uniform grids combined with multigrid methods.

This module also includes a matching version of ASE (version %(ASE_version)s),
the Atomic Simulation Environment, which is used internally by GPAW.

The module also contains the Atomic PAW setup files (version %(GPAWsetup_version)s)
in the directory $GPAW_SETUP_FILES.

The GPAW manual advises the OMP_NUM_THREADS should be set to 1 so we do this
in the module.

The version in this module is compiled using NumPy for FFT (which itself is using MKL).
The optional libvdwxc support is NOT included in this module.
The optional ELPA support is NOT included in this module.
""" % {
    'ASE_version':       local_ASE_20_1_version,  
    'GPAWsetup_version': local_GPAWsetup_version,  
}

docurls = [
    'GPAW web-based documentation: https://wiki.fysik.dtu.dk/gpaw/',
    'ASE web-based documentation:https://wiki.fysik.dtu.dk/ase/',
]

toolchain =     {'name': 'intel', 'version': local_intel_version}
toolchainopts = {'pic': True}

dependencies = [
    ('Python',   local_Python_3_8_version, local_UEABS_suffix),
    ('libxc',    local_libxc_version,      local_UEABS_suffix),
]

builddependencies = [
    ('buildtools', local_UEABS_version, '', True),
]

#
# EasyBuild on our machine (EasyBuild 4.2.2, CentOS 7.7 Python 2.7.5
# Did not accept multiple lines of the form [xxx] and produced warnings of
# EOF at scatnning triple-quoted string literal. Hence the construction
# where we use %(FB)s which we dan substitute with [.
#
local_gpaw_customize = """
print( 'GPAW EasyBuild INFO: Starting execution of the customization script' )
print( 'GPAW EasyBuild INFO: Variables at the start of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =                   ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =                ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =                ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =             ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =        ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs =     ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =                ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =            ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                    ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =                 ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =                   ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =          ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =             ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =               ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =           ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =                ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                        ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =                   ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                    ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                        ', elpa )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )

# LibXC from Bundle. The library is automatically included in the list, but the include path
# is not known.
include_dirs.append('%(installdir)s/include')

# libvdwxc
libvdwxc = False

# ELPA
elpa = False

# Use Intel MKL FFTW
fftw = False

# ScaLAPACK
scalapack = True
libraries += ['mkl_scalapack_lp64', 'mkl_blacs_intelmpi_lp64']

# MKL BLAS
libraries += ['mkl_sequential','mkl_core', 'mkl_rt', ]

# Add other EasyBuild library directoryes.
library_dirs = os.getenv('LIBRARY_PATH').split(':')

# Set the compilers
compiler =    'icc'
mpicompiler = 'mpiicc'
mpilinker =   'mpiicc'

print( 'GPAW EasyBuild INFO: Variables at the end of the customization script' )
print( 'GPAW EasyBuild INFO: libraries =                   ', libraries )
print( 'GPAW EasyBuild INFO: mpi_libaries =                ', mpi_libraries )
print( 'GPAW EasyBuild INFO: library_dirs =                ', library_dirs )
print( 'GPAW EasyBuild INFO: mpi_libary_dirs =             ', mpi_library_dirs )
print( 'GPAW EasyBuild INFO: runtime_library_dirs =        ', runtime_library_dirs )
print( 'GPAW EasyBuild INFO: mpi_runtime_libary_dirs =     ', mpi_runtime_library_dirs )
print( 'GPAW EasyBuild INFO: include_dirs =                ', include_dirs )
print( 'GPAW EasyBuild INFO: mpi_include_dirs =            ', mpi_include_dirs )
print( 'GPAW EasyBuild INFO: compiler =                    ', compiler )
print( 'GPAW EasyBuild INFO: mpicompiler =                 ', mpicompiler )
print( 'GPAW EasyBuild INFO: mpilinker =                   ', mpilinker )
print( 'GPAW EasyBuild INFO: extra_compile_args =          ', extra_compile_args )
print( 'GPAW EasyBuild INFO: extra_link_args =             ', extra_link_args )
print( 'GPAW EasyBuild INFO: define_macros =               ', define_macros )
print( 'GPAW EasyBuild INFO: mpi_define_macros =           ', mpi_define_macros )
print( 'GPAW EasyBuild INFO: undef_macros =                ', undef_macros )
print( 'GPAW EasyBuild INFO: fftw =                        ', fftw )
print( 'GPAW EasyBuild INFO: scalapack =                   ', scalapack )
print( 'GPAW EasyBuild INFO: libvdwxc =                    ', libvdwxc )
print( 'GPAW EasyBuild INFO: elpa =                        ', elpa )
print( 'GPAW EasyBuild INFO: noblas =                      ', noblas )
print( 'GPAW EasyBuild INFO: parallel_python_interpreter = ', parallel_python_interpreter )
print( 'GPAW EasyBuild INFO: Ending execution of the customization script' )
"""

default_easyblock = 'Tarball'

components = [
    ( 'GPAW-setups', local_GPAWsetup_version, { 
        # Check version on https://wiki.fysik.dtu.dk/gpaw/setups/setups.html
        'sources':   [{
                        'filename':    SOURCELOWER_TAR_GZ,
                        'source_urls': ['https://wiki.fysik.dtu.dk/gpaw-files/'], 
                        'extract_cmd': 'mkdir -p share/gpaw-setups ; cd share/gpaw-setups ; tar -xf %s --strip-components=1',
                     }]  
    }),
]

# This is a bundle of Python packages
exts_defaultclass = 'PythonPackage'
exts_filter = ("python -c 'import %(ext_name)s'", '')
exts_default_options = {
    'download_dep_fail': False, # To avoid EasyBuild complaining about the optional dependencies of ASE
    'sanity_pip_check':  False, # Otherwise requires the optional dependencies of GPAW.
    'source_urls':       [PYPI_SOURCE],
    'use_pip':           True,
    'use_pip_for_deps':  False,
}

exts_list = [
    ('Cython', local_Cython_version, { # Needed for NumPy according to the NumPy documentation.
        'checksums': ['e57acb89bd55943c8d8bf813763d20b9099cc7165c0f16b707631a7654be9cad'],
    }),
    ('numpy', local_numpy_version, {
        'patches': ['numpy-1.18.2-mkl.patch'],
        'source_tmpl': '%(name)s-%(version)s.zip',
        'checksums': [
            '34e96e9dae65c4839bd80012023aadd6ee2ccb73ce7fdf3074c62f301e63120b',  # numpy-1.18.5.zip
            'ea25ad5c0148c1398d282f0424e642fb9815a1a80f4512659b018e2adc378bcf',  # numpy-1.18.2-mkl.patch
        ],
     }),
    ('scipy', local_scipy_version, {
        'patches': ['scipy-1.4.1-fix-pthread.patch'],
        'checksums': [
            'dee1bbf3a6c8f73b6b218cb28eed8dd13347ea2f87d572ce19b289d6fd3fbc59',  # scipy-1.4.1.tar.gz
            '4e2162a93caddce63a1aa2dfb6c181774a4f6615950e1d60c54bb4308fee3bb3',  # scipy-1.4.1-fix-pthread.patch
        ],
    }),    
    ('ase', local_ASE_20_1_version, {
        'checksums':      ['27c378b983dfacd49398236e7232c28590c218c31bb2205695818552c772bc4b'],
#        'buildopts':      '--no-deps',
#        'installopts':    '--no-deps',
    }),
    ('gpaw', version, {
        'use_pip':        False, # When using pip, siteconfig.py seems to be ignored.
        'prebuildopts':   'cat >siteconfig.py <<EOF\n' + local_gpaw_customize + 'EOF\n' + 
                          'unset CC && unset LDSHARED && CFLAGS="$CFLAGS -qno-openmp-simd" ',
        'buildopts':      ' && echo -e "\\nGPAW EasyBuild INFO: configuration log:\\n" && cat configuration.log && echo -e "\\nEnd of GPAW configuration log\\n"',
        'preinstallopts': 'unset CC && unset LDSHARED && CFLAGS="$CFLAGS -qno-openmp-simd" ',
#        'installopts':    '--no-deps',
    }),
]

# Sanity check changed compared to version 19.9.1!
sanity_check_paths = {
    'files': [],
#    'files': ['bin/gpaw%s' % x for x in ['', '-analyse-basis', '-basis', '-mpisim', '-plot-parallel-timings',
#                                         '-runscript', '-setup', '-upfplot']],
    'dirs':  ['lib/python%(pyshortver)s/site-packages']
}

#sanity_check_commands = ["python -c 'import tkinter' "]

# As the main EasyBlock is "Bundle" PYTHONPATH will not be set automatically
modextrapaths = {
    'PYTHONPATH':      ['lib/python%(pyshortver)s/site-packages'],
}

modextravars = {
    'GPAW_SETUP_PATH': '%(installdir)s/share/gpaw-setups', # We don't use modextrapaths as the path should be  
                                                           # comma-separated rather than colon-separated anyway.
    'OMP_NUM_THREADS': '1',
}

moduleclass = 'chem'
