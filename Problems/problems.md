# Problems during GPAW testing

## GPAW-related problems

### G001 Output of gpaw test

It seems impossible to redirect the output of `gpaw test` to a file. The first and 
the last part are printed to STDOUR/STDERR, but the list of tests disappears as soon
as STDOUT or STDERR is redirected.

Moreover, the tests can leave a lot of trash files on `/tmp` when crashes occur.
 

### G002 **SOLVED** Crashes with 1.5.2 with Intel compiler during testing of manual build using IntelPython3/2020a on Vaughan

  * See script to rebuild the crashing version
  * `gpaw test -j 2` runs fine, but `gpaw test` of `gpaw test -j 1` crashes with a 
    segmentation violation in `fd_ops/laplace.py`
      * The default output does not show in which routine the crash occurs, but it may 
        be related to the next one with MPI given the name of the test.
  * Likewise, when running the MPI version using `srun -n 1 gpaw-python -m gpaw test`
    the code crashed at line 44 of `c/bmgs/fd.c`.

**SOLUTION**

It turns out that the code in that routine uses a `omp simd` pragma but the data is
likely sometimes not correctly alligned. This may explain the crash.

The solution is to add the compiler flag `-qno-openmp-simd` when compiling GPAW.


### G003 Failures during testing in response/iron_sf_ALDA_gridrep.py and response/iron_sf_gssALDA_gridrep.py

  * This failure occurred in a GPAW 1.5.2 instance compiled with the Intel compilers and 
    using IntelPython3 from the 2020 compilers. Some errors in the results are too
    large for the test to pass. The tests fail in serial and MPI mode.
  * Error message `response/iron_sf_ALDA_gridrep.py`: 

        AssertionError:  242.866784926 != 239.04368159184725 (error: |3.823103334152762| > 1)

    The precise number of course may vary a bit depending on compiler options etc.
  * Error message `response/iron_sf_gssALDA_gridrep.py`

        AssertionError:  1.038135552 != 1.037536256 (error: |0.0005992959999998604| > 0.0002)

    The precise number of course may vary a bit depending on compiler options etc.
  * See the build script for how GPAW was compiled.
  * The error persists when adding `-ftz -fp-speculation=safe -fp-model source` to 
    the compiler flags. Even reducing the optimization level for GPAW and libxc to 
    `-O0` the problem persists. Hence the problem may have to do with algorithms in
    Intel Python.

  * In one of our EasyBuild 20.1.x builds, response/iron_sf_ALDA.py us skipped and the problem
    does not occur in response/iron_sf_gssALDA.py


### G004 Failure in pw/expert_diag.py

  * Using the same GPAW 1.5.2 installation as for G003.
  * The test `pw/expert_diag.py` produces the error message

        AssertionError: Eigenvalues have changed since rev. #133324

  * The same error was observed running the sequential version and MPI version.
  * The error persists when adding `-ftz -fp-speculation=safe -fp-model source` to 
    the compiler flags. EVen reducing the optimization level for GPAW and libxc to 
    `-O0` the problem persists. Hence the problem may have to do with algorithms in
    Intel Python.


### G005 Failure in response/iron_sf_ALDA.py

  * Using the same GPAW 1.5.2 installation as for G003/G004
  * The test `response/iron_sf_ALDA.py` produces the error message

        AssertionError:  248.822795658 != 246.6286481607201 (error: |2.194147497279886| > 1)

  * We observed this bug when running the test suite on 8 cores, but not when 
    doing an MPI run on 1 core.


### G006 **SOLVED** Manual build on Vaughan of 1.5.2 with IntelPython3 crashes

  * Running

        gpaw test

    produces a segmentation fault in the test `xc/xc.py`. 
    When running with `-j 2` or greater the result of that test does not appear in
    the output. Some other tests in `test/xc` do work though.
  * The actual location of the crash seems to be in `c/xc/tpss.c` on line 434.
    This is a fairly dirty bit of code... It is not clear to me why one would need
    an allocation as on line 430 as it is not clear why one would need an array there.
    It is also not clear to me if this is a valid C99 case of variable length array 
    or not as it is inside a loop.
  * This block of code has changed in version 19.8.1, using a scalar which is all that
    is needed there. Bringing over the corresponding code block to version 1.5.2
    solves that segmentation violation.
  * But now another segmentation violation occurs on line 238 of `c/xc/revtpss.c`. 
    This is caused by a nearly identical block of code there, also changed in 19.8.1.


**SOLUTION**

  * Patch the above mentioned blocks of code to bring them in line with the code 
    of GPAW 1.19.8


### G0007 Manual build on Vaughan of 1.5.2 with Intel-compiled Python module crashes

  * Running

        gpaw test

    produces a segmentation fault in the test `pw/interpol.py`. See also G006 for other
    observations. So far I failed to determine the location of the crash.

### G0008 assertion errors in pw/realffc and/or pw/lfc.py

  * Observed with gpaw 1.5.2, Python 3.7.9, compiled UEABS-style on Vaughan.
  * `pw/realffc` and `pw/lfc.py` produce assertion errors similar to

        AssertionError:  1.0224602519914552 != 0 (error: |1.0224602519914552| > 1e-14)

  * Observed so far in serial testing, yet to do parallel testing.

## Problems that likely have to do with the setup on the systems tested.


###  S001 Compiling with IntelPython3, Intel MPI and gcc, CentOS 8 system (Vaughan)

  * See the build script.
  * Build using Intel Python 3 from the 2020 compilers, MPI/MKL/... from the 2020
    compilers and the GCC compiler included in the EasyBuild 2020a modules (GCC 9.3.0), 
    we get a LTO error when linking:

        lto1: fatal error: bytecode stream in file ‘/apps/.../libpython3.7m.a’ generated with LTO version 6.0 instead of the expected 8.1

  * Building using Intel Python 3 from the 2020 compilers, MPI/MKL/... from the 2020
    compilers and the gcc from CentOS 8.2 (gcc 8.3.1) (by unloading GCCcore and binutils),
    we get a LTO error when linking:

        lto1: fatal error: bytecode stream in file ‘/apps/.../libpython3.7m.a’ generated with LTO version 6.0 instead of the expected 7.3

  * The error is likely caused because Intel Python was compiled with an older GCC 
    compiler and including LTO information in the libpython.a file which is refused
    by newer compilers. Clearly a weakness in the setup of link-time optimizations
    in the GNU compiler system.
  * TODO: Is there a linker flag to disable LTO and what would the performance of the 
    code then be?


## EasyBuild problems

### E001 **SOLVED** Failure to avoid the installation of optional components of ase

  * Observed with EasyBuild 4.3.0.
  * When installing ase, EasyBuild did pull in the dependencies which according to
    the documentation of ase are optional. Moreover, these dependencies are not
    needed to run the UEABS benchmarks and require lots of other modules/packages
    to be installed on the system.
  * It was possible to avoid that EasyBuild installs them with ase by installing
    ase with `use_pip=True` and `use_pip_for_deps=False` while also setting
    `download_dep_fail=False` and `sanity_pip_check=False` to avoid problems during
    the sanity check. However, as GPAW itself cannot be installed in EasyBuild with `pip`
    (at least, we haven't found yet how to tell `pip` to use the site customizations),
    these dependencies still get pulled in after the installation of gpaw.
  * **Solution** It is actually possible to install gpaw with `pip` in EasyBuild. 
    However, only the install step is executed so the `siteconfig.py` file has to be 
    generated in `preinstallopts` rather than `prebuildopts`. Also, since EasyBuild adds
    some options only after `installopts`, it is not possible to use the latter for 
    additional processing at the end of the install step.
