#!/bin/bash

use_modules=( \
  'UEABS/2.2 GPAW/20.1.0-intel-2020a-Python-3.8.7-FFTW-Cfg1-UEABS-GPAW' \
  'UEABS/2.2 GPAW/20.10.0-intel-2020a-Python-3.9.1-FFTW-Cfg1-UEABS-GPAW' \
  'UEABS/2.2 GPAW/21.1.0-intel-2020a-Python-3.9.1-FFTW-Cfg1-UEABS-GPAW' \
)

module purge
MODULEPATH=$VSC_SCRATCH/UEABS/CalcUA-$VSC_INSTITUTE_CLUSTER-$VSC_ARCH_LOCAL/Modules

for (( c1=0; c1<${#use_modules[*]}; c1++ ))
do

    echo -e "\n\033[31mTrying to load ${use_modules[$c1]}.\033[0m"

    module purge
    module load ${use_modules[$c1]}
    module list

done
