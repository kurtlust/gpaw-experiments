# Overview of results on CalcUA-vaughan-rome

## Table of tests

  * GXXX marks refer to problems reported in the `Problems` subdirectory.

|:-------------------------------------------------------------------|:-----------------|:----------------|:--------|:--------|:----------|
| Module                                                             | Serial tests     | Parallel tests  | Small   | Medium  | Large     |
|:-------------------------------------------------------------------|:-----------------|:----------------|:--------|:--------|:----------|
| GPAW-UEABS/1.5.2-IntelPython3-icc                                  | G003/G004        | G003/G004/G005? | OK      |         |           |
| GPAW-UEABS/19.8.1-IntelPython3-icc                                 | G003/G004        |                 |         |         |           |
| GPAW-UEABS/20.1.0-IntelPython3-icc                                 | OK               |                 | OK      |         |           |
| GPAW-UEABS/20.10.0-IntelPython3-icc                                | OK               | OK              |         |         |           |
| GPAW-UEABS/1.5.2-Python37-icc                                      | Many errors      |                 |         |         |           |
| GPAW-UEABS/19.8.1-Python37-FFTW-icc                                |                  |                 |         |         |           |
| GPAW-UEABS/20.1.0-Python38-FFTW-icc                                |                  |                 |         |         |           |
| GPAW-UEABS/20.10.0-Python39-FFTW-icc                               |                  |                 |         |         |           |
|:-------------------------------------------------------------------|:-----------------|:----------------|:--------|:--------|:----------|
| GPAW/1.5.2-intel-2020a-Python-3.7.9-Cfg1-UEABS-GPAW                |                  |                 | OK      |         |           |
| GPAW/19.8.1-intel-2020a-Python-3.7.9-FFTW-Cfg1-UEABS-GPAW          |                  |                 | OK      |         |           |
| GPAW/19.8.1-intel-2020a-Python-3.7.9-MKLFFTW-Cfg1-UEABS-GPAW       |                  |                 | OK      |         |           |
| GPAW/19.8.1-intel-2020a-Python-3.7.9-NumPyFFT-Cfg1-UEABS-GPAW      |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python-3.8.6-FFTW-Cfg1-UEABS-GPAW          |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python-3.8.6-MKLFFTW-Cfg1-UEABS-GPAW       |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python-3.8.6-NumPyFFT-Cfg1-UEABS-GPAW      |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-FFTW-Cfg5-UEABS-GPAW      |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-MKLFFTW-Cfg5-UEABS-GPAW   |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-NumPyFFT-Cfg5-UEABS-GPAW  |                  |                 | OK      |         |           |
|:-------------------------------------------------------------------|:-----------------|:----------------|:--------|:--------|:----------|
| GPAW-UEABSl/1.5.2-Python37icc-icc                                  | G007             |                 | OK      |         |           |
| GPAW-UEABS/1.5.2-Python37-icc-eblibs                               |                  |                 |         |         |           |
|:-------------------------------------------------------------------|:-----------------|:----------------|:--------|:--------|:----------|
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-FFTW-Cfg3-UEABS-GPAW      |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-MKLFFTW-Cfg3-UEABS-GPAW   |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-FFTW-Cfg4-UEABS-GPAW      |                  |                 | OK      |         |           |
| GPAW/20.1.0-intel-2020a-Python_icc-3.8.6-MKLFFTW-Cfg4-UEABS-GPAW   |                  |                 | OK      |         |           |
|:-------------------------------------------------------------------|:-----------------|:----------------|:--------|:--------|:----------|

## Versions 2020-11-25

  * GPAW-UEABS/1.5.2-IntelPython3-icc
      * `gpaw test -j 64`:  335 total, 306 ran, 29 skipped, 3 fail (G003/G004)
      * `srun  -n 4 gpaw-python -m gpaw test`:
  * GPAW-UEABS/19.8.1-IntelPython3-icc
      * `gpaw test -j 64`:  349 total, 322 ran, 27 skipped, 0 fail
      * `srun  -n 4 gpaw-python -m gpaw test`:
  * GPAW-UEABS/20.1.0-IntelPython3-icc
      * `gpaw test -j 64`:  349 total, 322 ran, 27 skipped, 0 fail
      * `srun -n 4 python -m gpaw test`:
  * GPAW-UEABS/20.10.0-IntelPython3-icc
      * `gpaw test`: OK
      * `gpaw -P 4 test`: OK
  * GPAW-UEABS/1.5.2-Python37-icc
      * `gpaw test -j 64`:  335 total, 306 ran, 29 skipped, 17 fail (G008/G009 and others)


## Old results

  * GPAW-UEABS/1.5.2-Python3icc-icc
      * With `gpaw test -j 16`: 306 ran, 29 skipped, 3 failed
  * GPAW-UEABS/1.5.2-Python3-icc
      * With `gpaw test`:       306 ran, 29 skipped, 3 failed
      * With `gpaw test -j 16`: 306 ran, 29 skipped, 3 failed
