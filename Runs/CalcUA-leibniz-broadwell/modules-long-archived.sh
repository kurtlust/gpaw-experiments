#!/bin/bash
#
# Configure the modules for Leibniz.
#

gpaw_1_5=0
gpaw_19_8=0
gpaw_leibniz=0
gpaw_Cfg1=1
gpaw_Cfg2=0
gpaw_Cfg3=0
gpaw_Cfg4=0
gpaw_Cfg5=1
gpaw_UEABS=1

use_modules=()
# Add the leibniz modules
# Note that this will need an additional directory in the MODULEPATH.
if (( $gpaw_leibniz )); then
    (( $gpaw_19_8 ))  && use_modules+=('calcua/2019b GPAW/19.8.1-intel-2019b-Python-3.7.4')                            # Leibniz modules
    (( $gpaw_19_8 ))  && use_modules+=('calcua/2019b GPAW/19.8.1-intel-2019b-Python-3.7.4-MKLFFTW')                    # Leibniz module
    (( $gpaw_19_8 ))  && use_modules+=('calcua/2019b GPAW/19.8.1-intel-2019b-IntelPython3')                            # Leibniz module
    (( $gpaw_19_8 ))  && use_modules+=('calcua/2019b GPAW/19.8.1-intel-2019b-IntelPython3-MKLFFTW')                    # Leibniz module
fi
# Cfg1
if (( $gpaw_Cfg1 )); then
    (( $gpaw_1_5 ))   && use_modules+=('UEABS/2.2 GPAW/1.5.2-intel-2020a-Python-3.7.9-Cfg1-UEABS-GPAW')                # Cfg1 Archive
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW/19.8.1-intel-2020a-Python-3.7.9-FFTW-Cfg1-UEABS-GPAW')          # Cfg1 Archive
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW/19.8.1-intel-2020a-Python-3.7.9-MKLFFTW-Cfg1-UEABS-GPAW')       # Cfg1 Archive
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW/19.8.1-intel-2020a-Python-3.7.9-NumPyFFT-Cfg1-UEABS-GPAW')      # Cfg1 Archive
fi
# Cfg3
# Cfg4
# Cfg5
if (( $gpaw_Cfg5 )); then
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW/19.8.1-intel-2020a-Python_icc-3.7.9-FFTW-Cfg5-UEABS-GPAW')      # Cfg5
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW/19.8.1-intel-2020a-Python_icc-3.7.9-MKLFFTW-Cfg5-UEABS-GPAW')   # Cfg5
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW/19.8.1-intel-2020a-Python_icc-3.7.9-NumPyFFT-Cfg5-UEABS-GPAW')  # Cfg5
fi
# GPAW-UEABS
if (( $gpaw_UEABS )); then
    (( $gpaw_1_5 ))   && use_modules+=('UEABS/2.2 GPAW-UEABS/1.5.2-IntelPython3-icc')                                  # UEABS IntelPython3-based Archive
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/19.8.1-IntelPython3-icc')                                 # UEABS IntelPython3-based Archive
    (( $gpaw_1_5 ))   && use_modules+=('UEABS/2.2 GPAW-UEABS/1.5.2-Python37icc-icc')                                   # UEABS Python Archive
    (( $gpaw_1_5 ))   && use_modules+=('UEABS/2.2 GPAW-UEABS/1.5.2-Python37-icc-eblibs')                               # UEABS Python Archive
    (( $gpaw_1_5 ))   && use_modules+=('UEABS/2.2 GPAW-UEABS/1.5.2-Python37-icc')                                      # UEABS Python + NumPy Archive
    (( $gpaw_19_8 ))  && use_modules+=('UEABS/2.2 GPAW-UEABS/19.8.1-Python37-FFTW-icc')                                # UEABS Python + FFTW Intel Archive
fi


module purge
MODULEPATH=$VSC_SCRATCH/UEABS/CalcUA-$VSC_INSTITUTE_CLUSTER-$VSC_ARCH_LOCAL/Modules

for (( c1=0; c1<${#use_modules[*]}; c1++ ))
do

    echo -e "\n\033[31mTrying to load ${use_modules[$c1]}.\033[0m"

    module purge
    module load ${use_modules[$c1]}
    module list

done
