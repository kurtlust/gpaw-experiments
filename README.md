# GPAW-experiments repository

Work space to do tests with GPAW and prepare new material before trying to integrate
it in a development branch of the official UEABS web site.

This gives us more freedom to also keep notes that will not make it to the official
repository.

## Directory structure

The main subdirectories in this repository are:
  * [UEABS](UEABS/README.md): Files that come from or might go into the
    official PRACE UEABS repository.
  * [EasyBuild](EasyBuild/README.md): All EasyBuild-related files. This includes
    the EasyBuild configuration file directory (though we generate most of these
    files), a directory with files specific to each system that we tested on, some
    custom EasyBlocks that we needed, a custom module scheme that is simpler in
    structure than those provided by EasyBuild (e.g., no additional module directory
    for each moduleclass in the EasyBuild recipe files), and all EasyBuild recipes
    that we developed. [See that directory tree](EasyBuild/README.md) for more
    information.
  * [Systems](Systems/README.md): All system-specific scripts for systems that we
    tested, e.g.
      * Scripts to get started. Those scripts typically set up the directory
        structure for the software installations, module files to create a suitable
        environment, and build all tested configurations.
      * Scripts for testing the installed modules, e.g., a simple test if the
        modules load.
      * Common parts for job scripts that can be used for all benchmarks.

## Starting points for exploration

  * The directory structure for software installations is [explained in the
    Systems subdirectory README](Systems/README.md).
  * The [EasyBuild subdirectory README file](EasyBuild/README.md) explains that
    part of this repository.


## Some notes

  * The input file for the carbon nanotube benchmark from the 5IP benchmark suite
    (UEABS release 2.1) doesn't work anymore on recent versions of GPAW. There is
    a compatibility check that needs to be removed. (Source of the information:
    Martti Louhivouri, BCO in 5IP).
  * The carbon nanotube and silicon cluster benchmarks are originally from from
    Jussi Enkovaara of CSC who is a DFT expert and has also been actively developing
    GPAW in the past. The copper filament benchmark was derived from the carbon
    nanotube benchmark by Martti Loufivouri (CSC) who is not really a DFT expert.
    The basic idea was to do a benchmark similar to the nanotube one but to
    use more electrons (copper vs. carbon) with the aim to have more things
    to compute and thus, hopefully, better scalability. In hindsight, it
    seems to scale only somewhat better, but it is able to run on much
    larger systems. (Source of the information: Martti Louhivouri, BCO in 5IP).
  * In the experience of Martti, GPAW is  quite picky about the exact number of cores
    to use, e.g. due to how it does domain decomposition (BLACS grid). So,
    often one needs to either change the parallelisation settings ('sl_default')
    for ScaLAPACK or simply skip some core counts. Martti has always used full
    nodes and powers of two for node counts.
  * Martti Louhivouri has [a GitHub with several GPAW benchmarks including those
    used in the UEABS suite](https://github.com/mlouhivu/gpaw-benchmarks/).
  * Modifications to the medium test case (teh copper filament):
      * Mixing coefficients changed from (0.1, 5, 100) to (0.15, 3, 100)
      * Changed the convergence criteria from the defaults to 0.005 for
        energy (which corresponds for the case in a relative error around
        1e-5), 1e-4 for the density which is still the default value, and
        1e-5 for the eigenstates.
